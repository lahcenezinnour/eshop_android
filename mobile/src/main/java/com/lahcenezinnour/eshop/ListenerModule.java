package com.lahcenezinnour.eshop;

import android.content.Context;

import com.lahcenezinnour.eshop.ui.interfaces.OnFragmentFiltersInteractionListener;
import com.lahcenezinnour.eshop.ui.interfaces.OnFragmentLabelInteractionListener;
import com.lahcenezinnour.eshop.ui.interfaces.OnFragmentShopInteractionListener;
import com.lahcenezinnour.eshop.ui.interfaces.OnFragmentSubcategoriesInteractionListener;
import com.lahcenezinnour.eshop.ui.interfaces.OnMenuItemFragmentFiltersInteractionListener;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by lahcene zinnour on 3/2/17.
 */
@Module
public class ListenerModule {

    OnFragmentShopInteractionListener listenerShop;
    OnFragmentLabelInteractionListener listenerLabel;
    OnFragmentFiltersInteractionListener listenerFilters;
    OnMenuItemFragmentFiltersInteractionListener listenerMenuItemFilters;
    OnFragmentSubcategoriesInteractionListener listenerSubcategories;

    ListenerModule(Context context) {

        if (context instanceof OnFragmentShopInteractionListener) {
            listenerShop = (OnFragmentShopInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentShopInteractionListener");
        }

        if (context instanceof OnFragmentLabelInteractionListener) {
            listenerLabel = (OnFragmentLabelInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentLabelInteractionListener");
        }

        if (context instanceof OnFragmentFiltersInteractionListener) {
            listenerFilters = (OnFragmentFiltersInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentFiltersInteractionListener");
        }

        if (context instanceof OnMenuItemFragmentFiltersInteractionListener) {
            listenerMenuItemFilters = (OnMenuItemFragmentFiltersInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnMenuItemFragmentFiltersInteractionListener");
        }

        if (context instanceof OnFragmentSubcategoriesInteractionListener) {
            listenerSubcategories = (OnFragmentSubcategoriesInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentSubcategoriesInteractionListener");
        }

    }

    @Provides
    @Singleton
    OnFragmentLabelInteractionListener provideOnFragmentLabelInteractionListener() {
        return listenerLabel;
    }

    @Provides
    @Singleton
    OnFragmentShopInteractionListener provideOnFragmentShopInteractionListener() {
        return listenerShop;
    }

    @Provides
    @Singleton
    OnFragmentFiltersInteractionListener provideOnFragmentFiltersInteractionListener() {
        return listenerFilters;
    }

    @Provides
    @Singleton
    OnMenuItemFragmentFiltersInteractionListener provideOnMenuItemFragmentFiltersInteractionListener() {
        return listenerMenuItemFilters;
    }

    @Provides
    @Singleton
    OnFragmentSubcategoriesInteractionListener provideOnFragmentSubcategoriesInteractionListener() {
        return listenerSubcategories;
    }
}

package com.lahcenezinnour.eshop.util;

import android.app.Activity;
import android.os.Build;
import android.view.Window;
import android.view.WindowManager;

import javax.inject.Inject;

/**
 * Created by lahcene zinnour on 2/27/17.
 */

public class Utils {

    @Inject
    public Utils() {
    }

    public void changeStatusBarColor(Activity activity, int color){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = activity.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(color);
        }
    }
}

package com.lahcenezinnour.eshop.util;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.lahcenezinnour.eshop.R;

import javax.inject.Inject;

/**
 * Created by lahcene zinnour on 11/14/16.
 */

public class Animations {

    private Context context;

    @Inject
    public Animations(Context context) {
        this.context = context;
    }

    public void translateIn(View view, long duration){
        ObjectAnimator anim7 = ObjectAnimator.ofFloat(view, "scaleX", 1f);
        anim7.setDuration(duration);
        anim7.start();
    }

    public void translateOut(View view, long duration){
        ObjectAnimator anim7 = ObjectAnimator.ofFloat(view, "scaleX", 1f);
        anim7.setDuration(duration);
        anim7.start();
    }

    public void alphaIn(View view){
        ObjectAnimator anim7 = ObjectAnimator.ofFloat(view, "alpha", 1.0f, 0.0f);
        anim7.setDuration(400);
        anim7.start();
    }

    public void alphaOut(View view){
        ObjectAnimator anim7 = ObjectAnimator.ofFloat(view, "alpha", 0.0f, 1.0f);
        anim7.setDuration(400);
        anim7.start();
    }
//--------------------------------------------------------
    public void translateInAnim(View view){
        view.startAnimation(AnimationUtils.loadAnimation(this.context, R.anim.transition));
    }

    public void translateOutAnim(View view, @Nullable Fragment fragment, @Nullable FragmentManager fragmentManager){
        Animation animation = AnimationUtils.loadAnimation(this.context, R.anim.transition2);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation){
                if (fragment != null && fragmentManager != null)
                    fragmentManager.beginTransaction().hide(fragment).commit();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
        view.startAnimation(animation);
    }

    public void translateOutAnim2(View view, @Nullable Fragment fragment, @Nullable FragmentManager fragmentManager){
        Animation animation = AnimationUtils.loadAnimation(this.context, R.anim.transition3);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation){
                if (fragment != null && fragmentManager != null)
                    fragmentManager.beginTransaction().hide(fragment).commit();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
        view.startAnimation(animation);
    }

    public void alphaInAnim(View view){
        view.startAnimation(AnimationUtils.loadAnimation(this.context, R.anim.alpha));
    }

    public void alphaOutAnim(View view){
        view.startAnimation(AnimationUtils.loadAnimation(this.context, R.anim.alpha2));
    }
    //--------------------------------------------------------

    public void translateInHorAnim(View view){
        view.startAnimation(AnimationUtils.loadAnimation(this.context, R.anim.transition4));
    }

    public void translateOutHorAnim(View view, @Nullable Fragment fragment, @Nullable FragmentManager fragmentManager){
        Animation animation = AnimationUtils.loadAnimation(this.context, R.anim.transition5);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation){
                if (fragment != null && fragmentManager != null)
                    fragmentManager.beginTransaction().hide(fragment).commit();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
        view.startAnimation(animation);
    }
}

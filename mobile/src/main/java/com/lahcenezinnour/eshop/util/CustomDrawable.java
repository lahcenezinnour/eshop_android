package com.lahcenezinnour.eshop.util;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.StateListDrawable;
import android.graphics.drawable.shapes.RectShape;
import android.support.v4.content.ContextCompat;
import android.view.View;

import com.lahcenezinnour.eshop.R;


/**
 * Created by lahcene zinnour on 11/20/16.
 */

public class CustomDrawable {

    public LayerDrawable setElevation(Context context, int backgroundColor){

        ShapeDrawable shape1 = new ShapeDrawable(new RectShape());
        shape1.getPaint().setColor(ContextCompat.getColor(context, R.color.elevation_shadow1));
        shape1.setPadding(3,1,3,4);


        ShapeDrawable shape2 = new ShapeDrawable(new RectShape ());
        shape2.getPaint().setColor(ContextCompat.getColor(context, R.color.elevation_shadow2));
        shape2.setPadding(3,1,3,4);

        ShapeDrawable shape3 = new ShapeDrawable(new RectShape ());
        shape3.getPaint().setColor(ContextCompat.getColor(context, R.color.elevation_shadow2));
        shape3.setPadding(4,1,4,5);

        ShapeDrawable shape4 = new ShapeDrawable(new RectShape ());
        shape4.getPaint().setColor(ContextCompat.getColor(context, R.color.elevation_shadow3));
        shape4.setPadding(3,0,3,4);

        ShapeDrawable shape5 = new ShapeDrawable(new RectShape ());
        shape5.getPaint().setColor(ContextCompat.getColor(context, R.color.elevation_shadow1));
        shape5.setPadding(2,0,2,3);

        ShapeDrawable shape6 = new ShapeDrawable(new RectShape ());
        shape6.getPaint().setColor(backgroundColor);


        LayerDrawable layerDrawable = new LayerDrawable(new Drawable[]{shape1, shape2, shape3, shape4, shape5, shape6});

        return layerDrawable;
    }

    public LayerDrawable setElevation(Context context, GradientDrawable drawable){

        ShapeDrawable shape1 = new ShapeDrawable(new RectShape());
        shape1.getPaint().setColor(ContextCompat.getColor(context, R.color.elevation_shadow1));
        shape1.setPadding(3,1,3,4);


        ShapeDrawable shape2 = new ShapeDrawable(new RectShape ());
        shape2.getPaint().setColor(ContextCompat.getColor(context, R.color.elevation_shadow2));
        shape2.setPadding(3,1,3,4);

        ShapeDrawable shape3 = new ShapeDrawable(new RectShape ());
        shape3.getPaint().setColor(ContextCompat.getColor(context, R.color.elevation_shadow2));
        shape3.setPadding(4,1,4,5);

        ShapeDrawable shape4 = new ShapeDrawable(new RectShape ());
        shape4.getPaint().setColor(ContextCompat.getColor(context, R.color.elevation_shadow3));
        shape4.setPadding(3,0,3,4);

        ShapeDrawable shape5 = new ShapeDrawable(new RectShape ());
        shape5.getPaint().setColor(ContextCompat.getColor(context, R.color.elevation_shadow1));
        shape5.setPadding(2,0,2,3);


        LayerDrawable layerDrawable = new LayerDrawable(new Drawable[]{shape1, shape2, shape3, shape4, shape5, drawable});

        return layerDrawable;
    }

    public GradientDrawable setCornerView(int radius, int backgroundColor){
        GradientDrawable drawable = new GradientDrawable();
        drawable.setShape (GradientDrawable.RECTANGLE);
        drawable.setColor(backgroundColor);
        drawable.setCornerRadius(radius);

        return drawable;
    }

    public void setSelector(Context context, View image, int drawablePressed, int drawableFocused, int drawableDefault){
        StateListDrawable states = new StateListDrawable();
        states.addState(new int[] {android.R.attr.state_pressed}, ContextCompat.getDrawable(context, drawablePressed));
        states.addState(new int[] {android.R.attr.state_focused}, ContextCompat.getDrawable(context, drawableFocused));
        states.addState(new int[] {}, ContextCompat.getDrawable(context, drawableDefault));

        image.setBackground(states);
    }

    public void setSelectorCorner(View image, Drawable drawablePressed, Drawable drawableFocused, Drawable drawableDefault){
        StateListDrawable states = new StateListDrawable();
        states.addState(new int[] {android.R.attr.state_pressed}, drawablePressed);
        states.addState(new int[] {android.R.attr.state_focused}, drawableFocused);
        states.addState(new int[] {}, drawableDefault);

        image.setBackground(states);
    }

}

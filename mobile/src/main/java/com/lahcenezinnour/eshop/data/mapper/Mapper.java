package com.lahcenezinnour.eshop.data.mapper;

import android.database.Cursor;

import com.lahcenezinnour.eshop.data.EcommerceContract;
import com.lahcenezinnour.eshop.mvp.model.Productcategories;
import com.lahcenezinnour.eshop.mvp.model.Products;
import com.lahcenezinnour.eshop.mvp.model.Subproductcategories;
import com.lahcenezinnour.eshop.ui.adapters.model.SubCategoryItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lahcene zinnour on 1/2/17.
 */

public class Mapper {

    public static List<Products> cursorToProduct(Cursor cursor) {
        List<Products> productsModel = new ArrayList<>();

        if (cursor == null) {
            return null;
        }
        cursor.moveToFirst();
        try {
            while (cursor.moveToNext()) {
                Products productModel = new Products();

                productModel.setProductID(cursor.getInt(cursor.getColumnIndex(EcommerceContract.ProductEntry._ID)));
                productModel.setProductCartDesc(cursor.getString(cursor.getColumnIndex(EcommerceContract.ProductEntry.COLUMN_PRODUCT_DESC)));
                productModel.setProductImage(cursor.getString(cursor.getColumnIndex(EcommerceContract.ProductEntry.COLUMN_PRODUCT_IMAGE)));
                productModel.setProductLocation(cursor.getString(cursor.getColumnIndex(EcommerceContract.ProductEntry.COLUMN_PRODUCT_LOCATION)));
                productModel.setProductLongDesc(cursor.getString(cursor.getColumnIndex(EcommerceContract.ProductEntry.COLUMN_PRODUCT_LONG_DESC)));
                productModel.setProductName(cursor.getString(cursor.getColumnIndex(EcommerceContract.ProductEntry.COLUMN_PRODUCT_NAME)));
                productModel.setProductPrice(cursor.getFloat(cursor.getColumnIndex(EcommerceContract.ProductEntry.COLUMN_PRODUCT_PRICE)));
                productModel.setProductShortDesc(cursor.getString(cursor.getColumnIndex(EcommerceContract.ProductEntry.COLUMN_PRODUCT_SHORT_DESC)));
                productModel.setProductSKU(cursor.getString(cursor.getColumnIndex(EcommerceContract.ProductEntry.COLUMN_PRODUCT_SKU)));
                productModel.setProductStock(cursor.getFloat(cursor.getColumnIndex(EcommerceContract.ProductEntry.COLUMN_PRODUCT_STOCK)));
                productModel.setProductThumb(cursor.getString(cursor.getColumnIndex(EcommerceContract.ProductEntry.COLUMN_PRODUCT_THUMB)));
                productModel.setProductUnlimited(cursor.getInt(cursor.getColumnIndex(EcommerceContract.ProductEntry.COLUMN_PRODUCT_UNLIMITED)) > 0);
                productModel.setProductUpdateDate(cursor.getLong(cursor.getColumnIndex(EcommerceContract.ProductEntry.COLUMN_PRODUCT_UPDATE_DATE)));
                productModel.setProductWeight(cursor.getFloat(cursor.getColumnIndex(EcommerceContract.ProductEntry.COLUMN_PRODUCT_WEIGHT)));
                productModel.setProductActivityDisplay(cursor.getString(cursor.getColumnIndex(EcommerceContract.ProductEntry.COLUMN_PRODUCT_ACTIVITY_DISPLAY)));
                productModel.setProductActivityName(cursor.getString(cursor.getColumnIndex(EcommerceContract.ProductEntry.COLUMN_PRODUCT_ACTIVITY_NAME)));

                productsModel.add(productModel);
            }
        } finally {
            cursor.close();
        }
        return productsModel;
    }


    public List<Productcategories> cursorToCategory(Cursor cursor) {
        List<Productcategories> categoriesModel = new ArrayList<>();

        if (cursor == null) {
            return null;
        }
        //cursor.moveToFirst();
        try {
            while (cursor.moveToNext()) {
                Productcategories categoryModel = new Productcategories();

                categoryModel.setCategoryID(cursor.getInt(cursor.getColumnIndex(EcommerceContract.CategoryEntry._ID)));
                categoryModel.setCategoryName(cursor.getString(cursor.getColumnIndex(EcommerceContract.CategoryEntry.COLUMN_CATEGORY_NAME)));
                categoryModel.setCategoryGender(cursor.getString(cursor.getColumnIndex(EcommerceContract.CategoryEntry.COLUMN_CATEGORY_GENDER)));

                categoriesModel.add(categoryModel);
            }
        } finally {
            cursor.close();
        }
        return categoriesModel;
    }

    public List<Subproductcategories> cursorToSubcategory(Cursor cursor) {
        List<Subproductcategories> subproductcategoriesModel = new ArrayList<>();

        if (cursor == null) {
            return null;
        }
        //cursor.moveToFirst();
        try {
            while (cursor.moveToNext()) {
                Subproductcategories SubproductcategoryModel = new Subproductcategories();

                SubproductcategoryModel.setSubCategoryID(cursor.getInt(cursor.getColumnIndex(EcommerceContract.SubcategoryEntry._ID)));
                SubproductcategoryModel.setSubCategoryName(cursor.getString(cursor.getColumnIndex(EcommerceContract.SubcategoryEntry.COLUMN_SUBCATEGORY_NAME)));

                subproductcategoriesModel.add(SubproductcategoryModel);
            }
        } finally {
            cursor.close();
        }
        return subproductcategoriesModel;
    }

    public List<SubCategoryItem> subCategoryToSubCategoryItem(List<Subproductcategories> subproductcategories){
        List<SubCategoryItem> subCategoryItems = new ArrayList<>();

        for (int i = 0; i < subproductcategories.size(); i++) {
            SubCategoryItem item = new SubCategoryItem();
            item.setIdSubCategory(subproductcategories.get(i).getSubCategoryID());
            item.setNameSubCategory(subproductcategories.get(i).getSubCategoryName());
            subCategoryItems.add(item);
        }

        return subCategoryItems;
    }
}

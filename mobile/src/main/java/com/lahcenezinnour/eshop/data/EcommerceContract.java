package com.lahcenezinnour.eshop.data;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by lahcene zinnour on 10/1/2016.
 */

public class EcommerceContract {

    public static final String CONTENT_AUTHORITY = "com.lahcenezinnour.eshop.mobile";
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);
    public static final String PATH_PRODUCTS = "products";
    public static final String PATH_CATEGORIES = "categories";
    public static final String PATH_SUBCATEGORIES = "subcategories";
    public static final String PATH_OPTIONGROUPS = "optiongroups";
    public static final String PATH_OPTIONS = "options";
    public static final String PATH_PRODUCTOPTIONS = "productoptions";

    private EcommerceContract() {
    }

    public static final class ProductEntry implements BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_PRODUCTS).build();

        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_PRODUCTS;
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_PRODUCTS;

        public static final String TABLE_NAME = "products";

        public static final String COLUMN_PRODUCT_DESC = "ProductCartDesc";
        public static final String COLUMN_PRODUCT_IMAGE = "ProductImage";
        public static final String COLUMN_PRODUCT_LIVE = "ProductLive";
        public static final String COLUMN_PRODUCT_LOCATION = "ProductLocation";
        public static final String COLUMN_PRODUCT_LONG_DESC = "ProductLongDesc";
        public static final String COLUMN_PRODUCT_NAME = "ProductName";
        public static final String COLUMN_PRODUCT_PRICE = "ProductPrice";
        public static final String COLUMN_PRODUCT_SHORT_DESC = "ProductShortDesc";
        public static final String COLUMN_PRODUCT_SKU = "ProductSKU";
        public static final String COLUMN_PRODUCT_STOCK = "ProductStock";
        public static final String COLUMN_PRODUCT_THUMB = "ProductThumb";
        public static final String COLUMN_PRODUCT_UNLIMITED = "ProductUnlimited";
        public static final String COLUMN_PRODUCT_UPDATE_DATE = "ProductUpdateDate";
        public static final String COLUMN_PRODUCT_WEIGHT = "ProductWeight";
        public static final String COLUMN_PRODUCT_ACTIVITY_DISPLAY = "productActivityDisplay";
        public static final String COLUMN_PRODUCT_ACTIVITY_NAME = "productActivityName";
        public static final String COLUMN_PRODUCT_CATEGORY_ID = "ProductCategoryID";

        public static Uri buildProductUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }

        public static String getProductIdFromUri(Uri uri) {
            return uri.getPathSegments().get(1);
        }

    }

    public static final class CategoryEntry implements BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_CATEGORIES).build();

        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_CATEGORIES;
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_CATEGORIES;

        public static final String TABLE_NAME = "categories";

        public static final String COLUMN_CATEGORY_NAME = "CategoryName";
        public static final String COLUMN_CATEGORY_GENDER = "CategoryGender";

        public static Uri buildCategoryUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }

        public static String getCategoryIdFromUri(Uri uri) {
            return uri.getPathSegments().get(1);
        }

        /*
            Student: This is the buildWeatherLocation function you filled in.
         */
        public static Uri buildCategoryId(String locationSetting) {
            return CONTENT_URI.buildUpon().appendPath(locationSetting).build();
        }

    }

    public static final class SubcategoryEntry implements BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_SUBCATEGORIES).build();

        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_SUBCATEGORIES;
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_SUBCATEGORIES;

        public static final String TABLE_NAME = "subproductcategories";

        public static final String COLUMN_SUBCATEGORY_NAME = "SubCategoryName";
        public static final String COLUMN_SUBCATEGORY_CATEGORY_ID = "CategoryID";

        public static Uri buildCategoryUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }

        public static String getSubCategoryIdFromUri(Uri uri) {
            return uri.getPathSegments().get(1);
        }

        public static Uri buildSubCategoryId(String locationSetting) {
            return CONTENT_URI.buildUpon().appendPath(locationSetting).build();
        }

    }

    public static final class OptiongroupsEntry implements BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_OPTIONGROUPS).build();

        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_OPTIONGROUPS;
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_OPTIONGROUPS;

        public static final String TABLE_NAME = "optiongroups";

        public static final String COLUMN_OPTIONGROUPS_NAME = "OptionGroupName";

        public static Uri buildOptiongroupUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }

        public static String getOptiongroupIdFromUri(Uri uri) {
            return uri.getPathSegments().get(1);
        }

        /*
            Student: This is the buildWeatherLocation function you filled in.
         */
        public static Uri buildOptiongroupId(String locationSetting) {
            return CONTENT_URI.buildUpon().appendPath(locationSetting).build();
        }

    }

    public static final class OptionsEntry implements BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_OPTIONS).build();

        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_OPTIONS;
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_OPTIONS;

        public static final String TABLE_NAME = "options";

        public static final String COLUMN_OPTIONS_NAME = "OptionName";
        public static final String COLUMN_OPTIONS_OPTIONGROUPS_ID = "OptionGroupID";

        public static Uri buildOptionsUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }

        public static String getOptionsIdFromUri(Uri uri) {
            return uri.getPathSegments().get(1);
        }

        /*
            Student: This is the buildWeatherLocation function you filled in.
         */
        public static Uri buildOptionsId(String locationSetting) {
            return CONTENT_URI.buildUpon().appendPath(locationSetting).build();
        }

    }

    public static final class ProductoptionsEntry implements BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_PRODUCTOPTIONS).build();

        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_PRODUCTOPTIONS;
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_PRODUCTOPTIONS;

        public static final String TABLE_NAME = "productoptions";

        public static final String COLUMN_PRODUCTOPTIONS_OPTIONPRICEINCREMENT = "OptionPriceIncrement";
        public static final String COLUMN_PRODUCTOPTIONS_OPTIONGROUPSID = "OptionGroupID";
        public static final String COLUMN_PRODUCTOPTIONS_PRODUCT_ID = "ProductID";
        public static final String COLUMN_PRODUCTOPTIONS_OPTIONS_ID = "OptionID";

        public static Uri buildProductoptionsUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }

        public static String getProductoptionsIdFromUri(Uri uri) {
            return uri.getPathSegments().get(1);
        }

        public static Uri buildProductoptionsId(String locationSetting) {
            return CONTENT_URI.buildUpon().appendPath(locationSetting).build();
        }

    }
}

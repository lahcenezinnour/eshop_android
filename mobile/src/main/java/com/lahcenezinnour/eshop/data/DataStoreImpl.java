package com.lahcenezinnour.eshop.data;

import android.content.ContentResolver;

import com.lahcenezinnour.eshop.util.ISchedulerProvider;
import com.lahcenezinnour.eshop.data.interfaces.IDataStore;
import com.lahcenezinnour.eshop.mvp.model.Products;

import java.util.List;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by lahcene zinnour on 1/6/17.
 */

public class DataStoreImpl implements IDataStore {

    ContentResolver contentResolver;
    private ISchedulerProvider scheduler;

    @Inject
    public DataStoreImpl(ISchedulerProvider scheduler) {
        this.scheduler = scheduler;
    }

    @Override
    public void getProductsByRangeFromDB(int nb, int i) {
       // return contentResolver.query();
    }

    @Override
    public void getProductDetailsByIdFromDB(int id) {

    }

    @Override
    public void saveProductsInDbByRange(Observable<List<Products>> products) {
       /*products.map(productsList -> {
               contentResolver.bulkInsert();

           return productsList;
       }).observeOn(scheduler.mainThread()).*/

    }
}

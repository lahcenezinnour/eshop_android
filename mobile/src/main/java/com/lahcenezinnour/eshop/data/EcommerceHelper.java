package com.lahcenezinnour.eshop.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;

import com.lahcenezinnour.eshop.data.EcommerceContract.*;

/**
 * Created by lahcene zinnour on 10/1/2016.
 */

public class EcommerceHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "ecommerce.db";

    final String SQL_CREATE_PRODUCTS_TABLE = "CREATE TABLE " + ProductEntry.TABLE_NAME + " (" +
            ProductEntry._ID + " INTEGER PRIMARY KEY UNIQUE," +
            ProductEntry.COLUMN_PRODUCT_CATEGORY_ID + " INTEGER NOT NULL," +
            ProductEntry.COLUMN_PRODUCT_SKU + " TEXT NULL, " +
            ProductEntry.COLUMN_PRODUCT_NAME + " TEXT NULL, " +
            ProductEntry.COLUMN_PRODUCT_PRICE + " REAL NULL, " +
            ProductEntry.COLUMN_PRODUCT_WEIGHT + " REAL NULL, " +
            ProductEntry.COLUMN_PRODUCT_DESC + " TEXT NULL, " +
            ProductEntry.COLUMN_PRODUCT_SHORT_DESC + " TEXT NULL, " +
            ProductEntry.COLUMN_PRODUCT_LONG_DESC + " TEXT NULL, " +
            ProductEntry.COLUMN_PRODUCT_THUMB + " TEXT NULL, " +
            ProductEntry.COLUMN_PRODUCT_IMAGE + " TEXT NULL, " +
            ProductEntry.COLUMN_PRODUCT_UPDATE_DATE + " NUMERIC NULL, " +
            ProductEntry.COLUMN_PRODUCT_STOCK + " REAL NULL, " +
            ProductEntry.COLUMN_PRODUCT_LIVE + " INTEGER NULL, " +
            ProductEntry.COLUMN_PRODUCT_UNLIMITED + " INTEGER NULL, " +
            ProductEntry.COLUMN_PRODUCT_LOCATION + " TEXT NULL," +
            ProductEntry.COLUMN_PRODUCT_ACTIVITY_NAME + " TEXT NULL, " +
            ProductEntry.COLUMN_PRODUCT_ACTIVITY_DISPLAY + " TEXT NULL," +
            " FOREIGN KEY (" + ProductEntry.COLUMN_PRODUCT_CATEGORY_ID + ") REFERENCES " +
            CategoryEntry.TABLE_NAME + " (" + CategoryEntry._ID + "));";


    final String SQL_CREATE_CATEGORIES_TABLE = "CREATE TABLE " + CategoryEntry.TABLE_NAME + " (" +
            CategoryEntry._ID + " INTEGER PRIMARY KEY UNIQUE," +
            CategoryEntry.COLUMN_CATEGORY_NAME + " TEXT NULL, " +
            CategoryEntry.COLUMN_CATEGORY_GENDER + " TEXT NULL );";

    final String SQL_CREATE_SUBCATEGORIES_TABLE = "CREATE TABLE " + SubcategoryEntry.TABLE_NAME + " (" +
            SubcategoryEntry._ID + " INTEGER PRIMARY KEY UNIQUE," +
            SubcategoryEntry.COLUMN_SUBCATEGORY_CATEGORY_ID + " INTEGER NOT NULL," +
            SubcategoryEntry.COLUMN_SUBCATEGORY_NAME + " TEXT NULL, " +
            " FOREIGN KEY (" + SubcategoryEntry.COLUMN_SUBCATEGORY_CATEGORY_ID + ") REFERENCES " +
            CategoryEntry.TABLE_NAME + " (" + CategoryEntry._ID + "));";

    final String SQL_CREATE_OPTIONGROUPS_TABLE = "CREATE TABLE " + OptiongroupsEntry.TABLE_NAME + " (" +
            OptiongroupsEntry._ID + " INTEGER PRIMARY KEY UNIQUE," +
            OptiongroupsEntry.COLUMN_OPTIONGROUPS_NAME + " TEXT NULL" +
            " );";

    final String SQL_CREATE_OPTIONS_TABLE = "CREATE TABLE " + OptionsEntry.TABLE_NAME + " (" +
            OptionsEntry._ID + " INTEGER PRIMARY KEY UNIQUE," +
            OptionsEntry.COLUMN_OPTIONS_OPTIONGROUPS_ID + " INTEGER NOT NULL," +
            OptionsEntry.COLUMN_OPTIONS_NAME + " TEXT NULL, " +
            " FOREIGN KEY (" + OptionsEntry.COLUMN_OPTIONS_OPTIONGROUPS_ID + ") REFERENCES " +
            OptiongroupsEntry.TABLE_NAME + " (" + OptiongroupsEntry._ID + "));";

    final String SQL_CREATE_PRODUCTOPTIONS_TABLE = "CREATE TABLE " + ProductoptionsEntry.TABLE_NAME + " (" +
            ProductoptionsEntry._ID + " INTEGER PRIMARY KEY UNIQUE," +
            ProductoptionsEntry.COLUMN_PRODUCTOPTIONS_PRODUCT_ID + " INTEGER NOT NULL, " +
            ProductoptionsEntry.COLUMN_PRODUCTOPTIONS_OPTIONS_ID + " INTEGER NOT NULL, " +
            ProductoptionsEntry.COLUMN_PRODUCTOPTIONS_OPTIONPRICEINCREMENT + " REAL NULL," +
            ProductoptionsEntry.COLUMN_PRODUCTOPTIONS_OPTIONGROUPSID + " INTEGER NOT NULL, " +
            " FOREIGN KEY (" + ProductoptionsEntry.COLUMN_PRODUCTOPTIONS_PRODUCT_ID + ") REFERENCES " +
            ProductEntry.TABLE_NAME + " (" + ProductEntry._ID + ")," +
            " FOREIGN KEY (" + ProductoptionsEntry.COLUMN_PRODUCTOPTIONS_OPTIONS_ID + ") REFERENCES " +
            OptionsEntry.TABLE_NAME + " (" + OptionsEntry._ID + "));";

    public EcommerceHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        //this.insertData();
        System.out.println("-------------------- EcommerceHelper");
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        System.out.println("-------------------- create");
        sqLiteDatabase.execSQL(SQL_CREATE_PRODUCTS_TABLE);
        sqLiteDatabase.execSQL(SQL_CREATE_CATEGORIES_TABLE);
        sqLiteDatabase.execSQL("INSERT INTO "+ CategoryEntry.TABLE_NAME +
                "("+ CategoryEntry.COLUMN_CATEGORY_NAME +", "+ CategoryEntry.COLUMN_CATEGORY_GENDER +") VALUES" +
                "('Sweaters & Cardigans', 'MEN'),('Coats & Outerwear', 'MEN'),('Casual shirts', 'MEN'),\n" +
                "('T-Shirts, Tanks & Polos', 'MEN'),('Sweatshirts & Hoodies', 'MEN'),('Denim', 'MEN'),('Pants', 'MEN'),\n" +
                "('Shorts & Capris', 'MEN'),('Suits', 'MEN'),('Jackets', 'MEN'),('Dress shirts', 'MEN'),\n" +
                "('Ties & Bow ties', 'MEN'),('Swimwear', 'MEN'), " +
                "('Coats', 'WOMEN'),('Sweaters & Cardigans', 'WOMEN'),('T-Shirts', 'WOMEN'),('Dresses', 'WOMEN'),\n" +
                "('Blouses & Shirts', 'WOMEN'),('Sweatshirts & hoodies', 'WOMEN'),('Jackets', 'WOMEN'),('Pants', 'WOMEN'),\n" +
                "('Jeans', 'WOMEN'),('Skirts', 'WOMEN'),('Shorts', 'WOMEN'),('Beach & Swimwear', 'WOMEN')");
        sqLiteDatabase.execSQL(SQL_CREATE_SUBCATEGORIES_TABLE);
        sqLiteDatabase.execSQL("INSERT INTO "+ SubcategoryEntry.TABLE_NAME +
                "("+ SubcategoryEntry.COLUMN_SUBCATEGORY_NAME +", "+ SubcategoryEntry.COLUMN_SUBCATEGORY_CATEGORY_ID +") VALUES" +
                "('Crew necks', '1'),('V-necks', '1'),('Turtlenecks & Mock necks', '1'),('Shawls & Cardigans', '1'),\n" +
                "('Hooded', '1'),\n" +
                "('Lambswool', '1'),('Merino Wool', '1'),('Cotton', '1'),('Cashmere', '1'),('Fine Cotton Knit', '1'),\n" +
                "('Blends', '1'),\n" +
                /*"\n" +
                "-- ----------------------------- Coats & Outerwear\n" +*/
                "('Curated Collections', '2'),('Parkas & Down', '2'),('Jackets & Vests', '2'),('Overcoats', '2'),\n" +
                "('Leather & Suede', '2'),('Raincoats & Windbreakers', '2'),('Technical sports', '2'),\n" +
                /*"\n" +
                "-- ----------------------------- Casual shirts\n" +*/
                "('Short sleeves', '3'),('Long sleeves', '3'),('Pure Linen', '3'),('Checks', '3'),('Patterns', '3'),('Solid', '3'),\n" +
                /*"\n" +
                "-- ----------------------------- T-Shirts, Tanks & Polos\n" +*/
                "('Short sleeves & 3/4 sleeves', '4'),('Polos', '4'),('Prints', '4'),('Logo wear', '4'),('Buttoned necks', '4'),\n" +
                "('Long sleeves', '4'),\n" +
                /*"\n" +
                "-- ----------------------------- Sweatshirts & Hoodies\n" +
                "\n" +
                "-- ----------------------------- Denim\n" +*/
                "('Super skinny & Skinny fit', '6'),('Slim fit', '6'),('Straight fit', '6'),('Premium Denim', '6'),\n" +
                "('New Proportions', '6'),\n" +
                /*"\n" +
                "-- ----------------------------- Pants\n" +*/
                "('Tailored', '7'),('Joggers', '7'),('Super skinny & Skinny fit', '7'),('Slim fit', '7'),('Straight fit', '7'),\n" +
                "('New Proportions', '7'),\n" +
                /*"\n" +
                "-- ----------------------------- Shorts & Capris\n" +*/
                "('Bermudas', '8'),('Long bermudas', '8'),('Capris', '8'),\n" +
                /*"\n" +
                "-- ----------------------------- Suits\n" +*/
                "('Slim Fit', '9'),('Semi-slim Fit', '9'),('Regular Fit', '9'),\n" +
                /*"\n" +
                "-- ----------------------------- Jackets\n" +*/
                "('Slim Fit', '10'),('Semi-slim Fit', '10'),('Regular Fit', '10'),\n" +
                /*"\n" +
                "-- ----------------------------- Dress shirts\n" +*/
                "('Tailored fit', '11'),('Semi-tailored fit', '11'),('Regular fit', '11'),('Easy Care', '11'),\n" +
                /*"\n" +
                "-- ----------------------------- Ties & Bow ties\n" +*/
                "('Regular Ties', '12'),('Skinny Ties', '12'),('Bow Ties', '12'),('Pocket Squares & Scarves', '12'),\n" +
                /*"\n" +
                "-- ----------------------------- Swimwear\n" +
                "\n" +
                "-- ----------------------------- WOMEN\n" +
                "-- ----------------------------- Coats\n" +*/
                "('Anoraks and Parkas', '14'),('Quilted and Down', '14'),('Wool', '14'),('Trenches and Raincoats', '14'),\n" +
                "('Jackets and Vests', '14'),('Leather and Suede', '14'),('Sports Performance', '14'),\n" +
                /*"\n" +
                "-- ----------------------------- Sweaters & Cardigans\n" +*/
                "('Sweaters', '15'),('Cardigans', '15'),('Tunics', '15'),('Turtlenecks & Mock necks', '15'),\n" +
                "('Capes & Ponchos', '15'),\n" +
                /*"\n" +
                "-- ----------------------------- T-Shirts\n" +*/
                "('Tank Tops', '16'),('Short Sleeves', '16'),('Long Sleeves', '16'),('Crop Tops', '16'),('Tunics', '16'),\n" +
                /*"\n" +
                "-- ----------------------------- Dresses\n" +*/
                "('Mini', '17'),('Knee', '17'),('Midi', '17'),('Maxi', '17'),('Dressy', '17'),('Career', '17'),('Casual', '17'),\n" +
                "('Casual', '17'),('Little black dress', '17'),('Jumpsuits', '17'),\n" +
                /*"\n" +
                "-- ----------------------------- Blouses & Shirts\n" +*/
                "('Chic', '18'),('Fluid', '18'),('Classic', '18'),('Casual', '18'),('Crop Tops', '18'),('Tunics', '18'),\n" +
                /*"\n" +
                "-- ----------------------------- Sweatshirts & hoodies\n" +
                "\n" +
                "-- ----------------------------- Jackets\n" +
                "\n" +
                "-- ----------------------------- Pants\n" +*/
                "('Pants', '21'),('Leggings', '21'),('Fluid', '21'),('Tailored', '21'),('Casual', '21'),\n" +
                /*"\n" +
                "-- ----------------------------- Jeans\n" +*/
                "('Boyfriend', '22'),('Skinny leg', '22'),('Straight leg', '22'),('Boot cut leg', '22'),('Premium denim', '22'),\n" +
                "('Overalls', '22'),\n" +
                /*"\n" +
                "-- ----------------------------- Skirts\n" +*/
                "('Mini', '23'),('Knee', '23'),('Midi', '23'),('Maxi', '23'),\n" +
                /*"\n" +
                "-- ----------------------------- Shorts\n" +
                "\n" +*/
                /*"-- ----------------------------- Beach & Swimwear\n" +*/
                "('Tops', '25'),('Bottoms', '25'),('One-Pieces & Tankinis', '25'),('Rashguards', '25'),('D Cup', '25'),\n" +
                "('Collections', '25'),('Beach Cover-ups & Accessories', '25')");
        sqLiteDatabase.execSQL(SQL_CREATE_OPTIONGROUPS_TABLE);
        sqLiteDatabase.execSQL("INSERT INTO "+ OptiongroupsEntry.TABLE_NAME +
                "("+ OptiongroupsEntry._ID +", "+ OptiongroupsEntry.COLUMN_OPTIONGROUPS_NAME +") VALUES" +
                "(1, 'color'),(2, 'size'),(3, 'brand'), (4, 'gender')");
        sqLiteDatabase.execSQL(SQL_CREATE_OPTIONS_TABLE);
        sqLiteDatabase.execSQL("INSERT INTO "+ OptionsEntry.TABLE_NAME +
                "("+ OptionsEntry._ID +", "+ OptionsEntry.COLUMN_OPTIONS_OPTIONGROUPS_ID +", "+ OptionsEntry.COLUMN_OPTIONS_NAME+") VALUES" +
                "(1, 1, 'Assorted'),(2, 1, 'Beige'),(3, 1, 'Black'),(4, 1, 'Blue'),(5, 1, 'Brown'),(6, 1, 'Crimson'),\n" +
                "(7, 1, 'Green'),(8, 1, 'Grey'),(9, 1, 'Orange'),(10, 1, 'Pink'),(11, 1, 'Red'),(12, 1, 'White'),\n" +
                "(13, 1, 'Yellow'),(14, 2, 'one size'),(15, 2, 'x-small'),(16, 2, 'small'),(17, 2, 'sml-med'),\n" +
                "(18, 2, 'medium'),(19, 2, 'large'),(20, 2, 'large-xl'),(21, 2, 'x-large'),(22, 2, 'xx-large'),\n" +
                "(23, 3, 'Le 31'),(24, 3, 'Djab'),(25, 3, 'BOSS Orange'),(26, 3, 'Blend'),(27, 3, 'Element'),\n" +
                "(28, 3, 'Fairplay'),(29, 3, 'Fileuse d''arvor'),(30, 3, 'G-Star Raw'),(31, 3, 'Hurley'),\n" +
                "(32, 3, 'Imperial'),(33, 3, 'Jack & Jones'),(34, 3, 'John Varvatos'),(35, 3, 'Lacoste'),(36, 3, 'Michael Kors'),\n" +
                "(37, 3, 'Obey'),(38, 3, 'Only & Sons'),(39, 3, 'Point Zero'),(40, 3, 'Private Member'),(41, 3, 'Projek Raw'),\n" +
                "(42, 3, 'Rumors'),(43, 3, 'Selected'),(44, 3, 'Tiger of Sweden'),(45, 3, 'Vans'),(46, 3, 'Vitaly'),\n" +
                "(47, 3, 'Volcom'),(48, 4, 'Man'),(49, 4, 'Woman')");
        sqLiteDatabase.execSQL(SQL_CREATE_PRODUCTOPTIONS_TABLE);
        sqLiteDatabase.execSQL("INSERT INTO "+ ProductoptionsEntry.TABLE_NAME +
                "("+ ProductoptionsEntry._ID +", "+ ProductoptionsEntry.COLUMN_PRODUCTOPTIONS_PRODUCT_ID +"," +
                ProductoptionsEntry.COLUMN_PRODUCTOPTIONS_OPTIONS_ID +","+ ProductoptionsEntry.COLUMN_PRODUCTOPTIONS_OPTIONPRICEINCREMENT+"," +
                ProductoptionsEntry.COLUMN_PRODUCTOPTIONS_OPTIONGROUPSID+") VALUES" +
                "(1, 1, 1, 0, 1),(2, 1, 2, 0, 1),(3, 1, 3, 0, 1),(4, 1, 4, 0, 2),(5, 1, 5, 0, 2),(6, 1, 6, 0, 2),\n" +
                "(7, 1, 7, 2, 2),(8, 1, 8, 2, 2)");

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {

        System.out.println("-------------------- upgrade");
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + ProductEntry.TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + CategoryEntry.TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + SubcategoryEntry.TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + OptiongroupsEntry.TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + OptionsEntry.TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + ProductoptionsEntry.TABLE_NAME);
        onCreate(sqLiteDatabase);
    }

    public void insertData(){
        SQLiteDatabase db = this.getWritableDatabase();
        SQLiteStatement stmt = db.compileStatement("INSERT INTO "+ CategoryEntry.TABLE_NAME +
                "("+ CategoryEntry.COLUMN_CATEGORY_NAME +", "+ CategoryEntry.COLUMN_CATEGORY_GENDER +") VALUES" +
                "('Sweaters & Cardigans', 'MEN'),('Coats & Outerwear', 'MEN'),('Casual shirts', 'MEN'),\n" +
                "('T-Shirts, Tanks & Polos', 'MEN'),('Sweatshirts & Hoodies', 'MEN'),('Denim', 'MEN'),('Pants', 'MEN'),\n" +
                "('Shorts & Capris', 'MEN'),('Suits', 'MEN'),('Jackets', 'MEN'),('Dress shirts', 'MEN'),\n" +
                "('Ties & Bow ties', 'MEN'),('Swimwear', 'MEN'), " +
                "('Coats', 'WOMEN'),('Sweaters & Cardigans', 'WOMEN'),('T-Shirts', 'WOMEN'),('Dresses', 'WOMEN'),\n" +
                "('Blouses & Shirts', 'WOMEN'),('Sweatshirts & hoodies', 'WOMEN'),('Jackets', 'WOMEN'),('Pants', 'WOMEN'),\n" +
                "('Jeans', 'WOMEN'),('Skirts', 'WOMEN'),('Shorts', 'WOMEN'),('Beach & Swimwear', 'WOMEN')");

        stmt.execute();
        stmt.close();
        db.close();
    }
}

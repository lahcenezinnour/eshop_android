package com.lahcenezinnour.eshop.data.interfaces;

import com.lahcenezinnour.eshop.mvp.model.Products;

import java.util.List;

import rx.Observable;

/**
 * Created by lahcene zinnour on 1/6/17.
 */

public interface IDataStore {
    void getProductsByRangeFromDB(int nb, int i);

    void getProductDetailsByIdFromDB(int id);

    void saveProductsInDbByRange(Observable<List<Products>> products);
}

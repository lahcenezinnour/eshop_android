package com.lahcenezinnour.eshop.data;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.Nullable;

/**
 * Created by lahcene zinnour on 10/1/2016.
 */

public class EcommerceProvider extends ContentProvider {


    private EcommerceHelper mOpenHelper;
    private static final int PRODUCTS = 100;
    private static final int PRODUCT = 101;
    private static final int CATEGORIES = 200;
    private static final int CATEGORY = 201;
    private static final int SUBCATEGORIES = 300;
    private static final int SUBCATEGORY = 301;
    private static final int OPTIONGROPS = 400;
    private static final int OPTIONGOUP = 401;
    private static final int OPTIONS = 500;
    private static final int OPTION = 501;

    private static final UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    static {
        uriMatcher.addURI(EcommerceContract.CONTENT_AUTHORITY, EcommerceContract.PATH_PRODUCTS, PRODUCTS);
        uriMatcher.addURI(EcommerceContract.CONTENT_AUTHORITY, EcommerceContract.PATH_PRODUCTS + "/#", PRODUCT);

        uriMatcher.addURI(EcommerceContract.CONTENT_AUTHORITY, EcommerceContract.PATH_CATEGORIES, CATEGORIES);
        uriMatcher.addURI(EcommerceContract.CONTENT_AUTHORITY, EcommerceContract.PATH_CATEGORIES + "/#", CATEGORY);

        uriMatcher.addURI(EcommerceContract.CONTENT_AUTHORITY, EcommerceContract.PATH_SUBCATEGORIES, SUBCATEGORIES);
        uriMatcher.addURI(EcommerceContract.CONTENT_AUTHORITY, EcommerceContract.PATH_SUBCATEGORIES + "/#", SUBCATEGORY);

        uriMatcher.addURI(EcommerceContract.CONTENT_AUTHORITY, EcommerceContract.PATH_OPTIONGROUPS, OPTIONGROPS);
        uriMatcher.addURI(EcommerceContract.CONTENT_AUTHORITY, EcommerceContract.PATH_OPTIONGROUPS + "/#", OPTIONGOUP);

        uriMatcher.addURI(EcommerceContract.CONTENT_AUTHORITY, EcommerceContract.PATH_OPTIONS, OPTIONS);
        uriMatcher.addURI(EcommerceContract.CONTENT_AUTHORITY, EcommerceContract.PATH_OPTIONS + "/#", OPTION);

    }
    private static final SQLiteQueryBuilder sQueryBuilder = new SQLiteQueryBuilder();
    private static final SQLiteQueryBuilder sProductByCategoryQueryBuilder;

    static {
        sProductByCategoryQueryBuilder = new SQLiteQueryBuilder();

        //This is an inner join which looks like
        //weather INNER JOIN location ON weather.location_id = location._id
        sProductByCategoryQueryBuilder.setTables(
                EcommerceContract.ProductEntry.TABLE_NAME + " INNER JOIN " +
                        EcommerceContract.CategoryEntry.TABLE_NAME +
                        " ON " + EcommerceContract.ProductEntry.TABLE_NAME +
                        "." + EcommerceContract.ProductEntry.COLUMN_PRODUCT_CATEGORY_ID +
                        " = " + EcommerceContract.CategoryEntry.TABLE_NAME +
                        "." + EcommerceContract.CategoryEntry._ID);
    }

    private static final SQLiteQueryBuilder sSubcategoryByCategoryQueryBuilder;

    static {
        sSubcategoryByCategoryQueryBuilder = new SQLiteQueryBuilder();

        //This is an inner join which looks like
        //weather INNER JOIN location ON weather.location_id = location._id
        sSubcategoryByCategoryQueryBuilder.setTables(
                EcommerceContract.SubcategoryEntry.TABLE_NAME + " INNER JOIN " +
                        EcommerceContract.CategoryEntry.TABLE_NAME +
                        " ON " + EcommerceContract.SubcategoryEntry.TABLE_NAME +
                        "." + EcommerceContract.SubcategoryEntry.COLUMN_SUBCATEGORY_CATEGORY_ID +
                        " = " + EcommerceContract.CategoryEntry.TABLE_NAME +
                        "." + EcommerceContract.CategoryEntry._ID);
    }


    private Cursor getProductById(Uri uri, String[] projection, String sortOrder) {
        System.out.println("+++ EcommercProvider_getProductById");
        String productId = EcommerceContract.ProductEntry.getProductIdFromUri(uri);
        String[] selectionArgs;
        String selection;

        if (Integer.parseInt(productId) == 0) {
            selection = null;
            selectionArgs = null;
        } else {
            selectionArgs = new String[]{productId};
            selection = productByIdSelection;
        }

        return mOpenHelper.getReadableDatabase().query(
                EcommerceContract.ProductEntry.TABLE_NAME,
                projection,
                selection,
                selectionArgs,
                null,
                null,
                sortOrder
        );
    }

    private Cursor getCategoryById(Uri uri, String[] projection, String sortOrder) {
        System.out.println("+++ EcommercProvider_getCategoryById");
        String categoryId = EcommerceContract.CategoryEntry.getCategoryIdFromUri(uri);
        String[] selectionArgs;
        String selection;

        if (Integer.parseInt(categoryId) == 0) {
            selection = null;
            selectionArgs = null;
        } else {
            selectionArgs = new String[]{categoryId};
            selection = categoryByIdSelection;
        }

        return mOpenHelper.getReadableDatabase().query(
                EcommerceContract.CategoryEntry.TABLE_NAME,
                projection,
                selection,
                selectionArgs,
                null,
                null,
                sortOrder
        );
    }

    private Cursor getSubcategoryById(Uri uri, String[] projection, String sortOrder) {
        System.out.println("+++ EcommercProvider_getSubcategoryById");
        String subcategoryId = EcommerceContract.SubcategoryEntry.getSubCategoryIdFromUri(uri);
        String[] selectionArgs;
        String selection;

        if (Integer.parseInt(subcategoryId) == 0) {
            selection = null;
            selectionArgs = null;
        } else {
            selectionArgs = new String[]{subcategoryId};
            selection = subcategoryByIdSelection;
        }

        return mOpenHelper.getReadableDatabase().query(
                EcommerceContract.SubcategoryEntry.TABLE_NAME,
                projection,
                selection,
                selectionArgs,
                null,
                null,
                sortOrder
        );
    }

    private Cursor getOptiongroupById(Uri uri, String[] projection, String sortOrder) {
        System.out.println("+++ EcommercProvider_getOptiongroupById");
        String optiongroupId = EcommerceContract.OptiongroupsEntry.getOptiongroupIdFromUri(uri);
        String[] selectionArgs;
        String selection;

        if (Integer.parseInt(optiongroupId) == 0) {
            selection = null;
            selectionArgs = null;
        } else {
            selectionArgs = new String[]{optiongroupId};
            selection = optiongroupByIdSelection;
        }

        return mOpenHelper.getReadableDatabase().query(
                EcommerceContract.OptiongroupsEntry.TABLE_NAME,
                projection,
                selection,
                selectionArgs,
                null,
                null,
                sortOrder
        );
    }

    private Cursor getOptionById(Uri uri, String[] projection, String sortOrder) {
        System.out.println("+++ EcommercProvider_getOptionById");
        String optionId = EcommerceContract.OptionsEntry.getOptionsIdFromUri(uri);
        String[] selectionArgs;
        String selection;

        if (Integer.parseInt(optionId) == 0) {
            selection = null;
            selectionArgs = null;
        } else {
            selectionArgs = new String[]{optionId};
            selection = optionByIdSelection;
        }

        return mOpenHelper.getReadableDatabase().query(
                EcommerceContract.OptionsEntry.TABLE_NAME,
                projection,
                selection,
                selectionArgs,
                null,
                null,
                sortOrder
        );
    }

    private int deletProductById(Uri uri, SQLiteDatabase db) {
        System.out.println("+++ EcommercProvider_deletProductById");
        String productId = EcommerceContract.ProductEntry.getProductIdFromUri(uri);
        String[] selectionArgs;
        String selection;

        if (Integer.parseInt(productId) == 0) {
            selection = null;
            selectionArgs = null;
        } else {
            selectionArgs = new String[]{productId};
            selection = productByIdSelection;
        }

        return db.delete(EcommerceContract.ProductEntry.TABLE_NAME, selection, selectionArgs);
    }

    private int deletCategoryById(Uri uri, SQLiteDatabase db) {
        System.out.println("+++ EcommercProvider_deletCategoryById");
        String categoryId = EcommerceContract.CategoryEntry.getCategoryIdFromUri(uri);
        String[] selectionArgs;
        String selection;

        if (Integer.parseInt(categoryId) == 0) {
            selection = null;
            selectionArgs = null;
        } else {
            selectionArgs = new String[]{categoryId};
            selection = categoryByIdSelection;
        }

        return db.delete(EcommerceContract.CategoryEntry.TABLE_NAME, selection, selectionArgs);
    }

    private int deletSubcategoryById(Uri uri, SQLiteDatabase db) {
        System.out.println("+++ EcommercProvider_deletSubcategoryById");
        String subcategoryId = EcommerceContract.SubcategoryEntry.getSubCategoryIdFromUri(uri);
        String[] selectionArgs;
        String selection;

        if (Integer.parseInt(subcategoryId) == 0) {
            selection = null;
            selectionArgs = null;
        } else {
            selectionArgs = new String[]{subcategoryId};
            selection = subcategoryByIdSelection;
        }

        return db.delete(EcommerceContract.SubcategoryEntry.TABLE_NAME, selection, selectionArgs);
    }

    private int deletOptiongroupById(Uri uri, SQLiteDatabase db) {
        System.out.println("+++ EcommercProvider_deletOptiongroupById");
        String optiongroupId = EcommerceContract.OptiongroupsEntry.getOptiongroupIdFromUri(uri);
        String[] selectionArgs;
        String selection;

        if (Integer.parseInt(optiongroupId) == 0) {
            selection = null;
            selectionArgs = null;
        } else {
            selectionArgs = new String[]{optiongroupId};
            selection = optiongroupByIdSelection;
        }

        return db.delete(EcommerceContract.OptiongroupsEntry.TABLE_NAME, selection, selectionArgs);
    }

    private int deletOptionById(Uri uri, SQLiteDatabase db) {
        System.out.println("+++ EcommercProvider_deletOptionById");
        String optionId = EcommerceContract.OptionsEntry.getOptionsIdFromUri(uri);
        String[] selectionArgs;
        String selection;

        if (Integer.parseInt(optionId) == 0) {
            selection = null;
            selectionArgs = null;
        } else {
            selectionArgs = new String[]{optionId};
            selection = optionByIdSelection;
        }

        return db.delete(EcommerceContract.OptionsEntry.TABLE_NAME, selection, selectionArgs);
    }

    private int updateProductById(Uri uri, SQLiteDatabase db, ContentValues values) {
        System.out.println("+++ EcommercProvider_updateProductById");
        String productId = EcommerceContract.ProductEntry.getProductIdFromUri(uri);
        String[] selectionArgs;
        String selection;

        if (Integer.parseInt(productId) == 0) {
            selection = null;
            selectionArgs = null;
        } else {
            selectionArgs = new String[]{productId};
            selection = productByIdSelection;
        }

        return db.update(EcommerceContract.ProductEntry.TABLE_NAME, values, selection, selectionArgs);
    }

    private int updateCategoryById(Uri uri, SQLiteDatabase db, ContentValues values) {
        System.out.println("+++ EcommercProvider_updateCategoryById");
        String categoryId = EcommerceContract.CategoryEntry.getCategoryIdFromUri(uri);
        String[] selectionArgs;
        String selection;

        if (Integer.parseInt(categoryId) == 0) {
            selection = null;
            selectionArgs = null;
        } else {
            selectionArgs = new String[]{categoryId};
            selection = categoryByIdSelection;
        }

        return db.update(EcommerceContract.CategoryEntry.TABLE_NAME, values, selection, selectionArgs);
    }

    private int updateSubcategoryById(Uri uri, SQLiteDatabase db, ContentValues values) {
        System.out.println("+++ EcommercProvider_updateSubcategoryById");
        String subcategoryId = EcommerceContract.SubcategoryEntry.getSubCategoryIdFromUri(uri);
        String[] selectionArgs;
        String selection;

        if (Integer.parseInt(subcategoryId) == 0) {
            selection = null;
            selectionArgs = null;
        } else {
            selectionArgs = new String[]{subcategoryId};
            selection = subcategoryByIdSelection;
        }

        return db.update(EcommerceContract.SubcategoryEntry.TABLE_NAME, values, selection, selectionArgs);
    }

    private int updateOptiongroupById(Uri uri, SQLiteDatabase db, ContentValues values) {
        System.out.println("+++ EcommercProvider_updateOptiongroupById");
        String optiongroupId = EcommerceContract.OptiongroupsEntry.getOptiongroupIdFromUri(uri);
        String[] selectionArgs;
        String selection;

        if (Integer.parseInt(optiongroupId) == 0) {
            selection = null;
            selectionArgs = null;
        } else {
            selectionArgs = new String[]{optiongroupId};
            selection = optiongroupByIdSelection;
        }

        return db.update(EcommerceContract.OptiongroupsEntry.TABLE_NAME, values, selection, selectionArgs);
    }

    private int updateOptionById(Uri uri, SQLiteDatabase db, ContentValues values) {
        System.out.println("+++ EcommercProvider_updateOptionById");
        String optionId = EcommerceContract.OptionsEntry.getOptionsIdFromUri(uri);
        String[] selectionArgs;
        String selection;

        if (Integer.parseInt(optionId) == 0) {
            selection = null;
            selectionArgs = null;
        } else {
            selectionArgs = new String[]{optionId};
            selection = optionByIdSelection;
        }

        return db.update(EcommerceContract.OptionsEntry.TABLE_NAME, values, selection, selectionArgs);
    }

    private static final String productByIdSelection =
            EcommerceContract.ProductEntry.TABLE_NAME +
                    "." + EcommerceContract.ProductEntry._ID + " = ? ";

    private static final String categoryByIdSelection =
            EcommerceContract.CategoryEntry.TABLE_NAME +
                    "." + EcommerceContract.CategoryEntry._ID + " = ? ";

    private static final String subcategoryByIdSelection =
            EcommerceContract.SubcategoryEntry.TABLE_NAME +
                    "." + EcommerceContract.SubcategoryEntry._ID + " = ? ";

    private static final String optiongroupByIdSelection =
            EcommerceContract.OptiongroupsEntry.TABLE_NAME +
                    "." + EcommerceContract.OptiongroupsEntry._ID + " = ? ";

    private static final String optionByIdSelection =
            EcommerceContract.OptionsEntry.TABLE_NAME +
                    "." + EcommerceContract.OptionsEntry._ID + " = ? ";

    @Override
    public boolean onCreate() {
        System.out.println("-------------------- content provider");
        mOpenHelper = new EcommerceHelper(getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        System.out.println("+++ EcommercProvider_query");
        Cursor retCursor;
        switch (uriMatcher.match(uri)) {
            // "weather/*/*"
            case PRODUCTS: {
                retCursor = mOpenHelper.getReadableDatabase().query(
                        EcommerceContract.ProductEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            }
            case PRODUCT: {
                retCursor = getProductById(uri, projection, sortOrder);
                break;
            }

            case CATEGORIES: {
                retCursor = mOpenHelper.getReadableDatabase().query(
                        EcommerceContract.CategoryEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            }
            case CATEGORY: {
                retCursor = getCategoryById(uri, projection, sortOrder);
                break;
            }

            case SUBCATEGORIES: {
                retCursor = mOpenHelper.getReadableDatabase().query(
                        EcommerceContract.SubcategoryEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            }
            case SUBCATEGORY: {
                retCursor = getSubcategoryById(uri, projection, sortOrder);
                break;
            }

            case OPTIONGROPS: {
                retCursor = mOpenHelper.getReadableDatabase().query(
                        EcommerceContract.OptiongroupsEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            }
            case OPTIONGOUP: {
                retCursor = getOptiongroupById(uri, projection, sortOrder);
                break;
            }

            case OPTIONS: {
                retCursor = mOpenHelper.getReadableDatabase().query(
                        EcommerceContract.OptionsEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            }
            case OPTION: {
                retCursor = getOptionById(uri, projection, sortOrder);
                break;
            }


            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        retCursor.setNotificationUri(getContext().getContentResolver(), uri);
        return retCursor;
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        System.out.println("+++ EcommercProvider_getType");
        final int match = uriMatcher.match(uri);

        switch (match) {

            case PRODUCTS:
                return EcommerceContract.ProductEntry.CONTENT_TYPE;
            case PRODUCT:
                return EcommerceContract.ProductEntry.CONTENT_ITEM_TYPE;
            case CATEGORIES:
                return EcommerceContract.CategoryEntry.CONTENT_TYPE;
            case CATEGORY:
                return EcommerceContract.CategoryEntry.CONTENT_ITEM_TYPE;
            case SUBCATEGORIES:
                return EcommerceContract.SubcategoryEntry.CONTENT_TYPE;
            case SUBCATEGORY:
                return EcommerceContract.SubcategoryEntry.CONTENT_ITEM_TYPE;
            case OPTIONGROPS:
                return EcommerceContract.OptiongroupsEntry.CONTENT_TYPE;
            case OPTIONGOUP:
                return EcommerceContract.OptiongroupsEntry.CONTENT_ITEM_TYPE;
            case OPTIONS:
                return EcommerceContract.OptionsEntry.CONTENT_TYPE;
            case OPTION:
                return EcommerceContract.OptionsEntry.CONTENT_ITEM_TYPE;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        System.out.println("+++ EcommercProvider_insert");
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final int match = uriMatcher.match(uri);
        Uri returnUri;

        switch (match) {
            case PRODUCT: {
                long _id = db.insert(EcommerceContract.ProductEntry.TABLE_NAME, null, values);
                if ( _id > 0 )
                    returnUri = EcommerceContract.ProductEntry.buildProductUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            case CATEGORY: {
                long _id = db.insert(EcommerceContract.CategoryEntry.TABLE_NAME, null, values);
                if ( _id > 0 )
                    returnUri = EcommerceContract.ProductEntry.buildProductUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            case SUBCATEGORY: {
                long _id = db.insert(EcommerceContract.CategoryEntry.TABLE_NAME, null, values);
                if ( _id > 0 )
                    returnUri = EcommerceContract.ProductEntry.buildProductUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            case OPTIONGOUP: {
                long _id = db.insert(EcommerceContract.CategoryEntry.TABLE_NAME, null, values);
                if ( _id > 0 )
                    returnUri = EcommerceContract.ProductEntry.buildProductUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            case OPTION: {
                long _id = db.insert(EcommerceContract.CategoryEntry.TABLE_NAME, null, values);
                if ( _id > 0 )
                    returnUri = EcommerceContract.ProductEntry.buildProductUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return returnUri;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        System.out.println("+++ EcommercProvider_delete");
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final int match = uriMatcher.match(uri);
        int rowsDeleted;
        // this makes delete all rows return the number of rows deleted
        if ( null == selection ) selection = "1";
        switch (match) {
            case PRODUCTS:
                rowsDeleted = db.delete(
                        EcommerceContract.ProductEntry.TABLE_NAME, selection, selectionArgs);
                break;
            case PRODUCT:
                rowsDeleted = deletProductById(uri, db);
                break;

            case CATEGORIES:
                rowsDeleted = db.delete(
                        EcommerceContract.CategoryEntry.TABLE_NAME, selection, selectionArgs);
                break;
            case CATEGORY:
                rowsDeleted = deletCategoryById(uri, db);
                break;

            case SUBCATEGORIES:
                rowsDeleted = db.delete(
                        EcommerceContract.SubcategoryEntry.TABLE_NAME, selection, selectionArgs);
                break;
            case SUBCATEGORY:
                rowsDeleted = deletSubcategoryById(uri, db);
                break;

            case OPTIONGROPS:
                rowsDeleted = db.delete(
                        EcommerceContract.OptiongroupsEntry.TABLE_NAME, selection, selectionArgs);
                break;
            case OPTIONGOUP:
                rowsDeleted = deletOptiongroupById(uri, db);
                break;

            case OPTIONS:
                rowsDeleted = db.delete(
                        EcommerceContract.OptionsEntry.TABLE_NAME, selection, selectionArgs);
                break;
            case OPTION:
                rowsDeleted = deletOptionById(uri, db);
                break;

            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        // Because a null deletes all rows
        if (rowsDeleted != 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return rowsDeleted;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        System.out.println("+++ EcommercProvider_update");
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final int match = uriMatcher.match(uri);
        int rowsUpdated;

        switch (match) {
            case PRODUCTS:
                rowsUpdated = db.update(EcommerceContract.ProductEntry.TABLE_NAME, values, selection, selectionArgs);
                break;
            case PRODUCT:
                rowsUpdated = updateProductById(uri, db, values);
                break;

            case CATEGORIES:
                rowsUpdated = db.update(EcommerceContract.CategoryEntry.TABLE_NAME, values, selection, selectionArgs);
                break;
            case CATEGORY:
                rowsUpdated = updateCategoryById(uri, db, values);
                break;

            case SUBCATEGORIES:
                rowsUpdated = db.update(EcommerceContract.SubcategoryEntry.TABLE_NAME, values, selection, selectionArgs);
                break;
            case SUBCATEGORY:
                rowsUpdated = updateSubcategoryById(uri, db, values);
                break;

            case OPTIONGROPS:
                rowsUpdated = db.update(EcommerceContract.OptiongroupsEntry.TABLE_NAME, values, selection, selectionArgs);
                break;
            case OPTIONGOUP:
                rowsUpdated = updateOptiongroupById(uri, db, values);
                break;

            case OPTIONS:
                rowsUpdated = db.update(EcommerceContract.OptionsEntry.TABLE_NAME, values, selection, selectionArgs);
                break;
            case OPTION:
                rowsUpdated = updateOptionById(uri, db, values);
                break;

            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        if (rowsUpdated != 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return rowsUpdated;
    }

    @Override
    public int bulkInsert(Uri uri, ContentValues[] values) {
        System.out.println("+++ EcommercProvider_bulkInsert");
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final int match = uriMatcher.match(uri);

        switch (match) {
            case PRODUCTS: {
                db.beginTransaction();
                int returnCount = 0;
                try {
                    for (ContentValues value : values) {
                        long _id = db.insert(EcommerceContract.ProductEntry.TABLE_NAME, null, value);
                        if (_id != -1) {
                            returnCount++;
                        }
                    }
                    db.setTransactionSuccessful();
                } finally {
                    db.endTransaction();
                }
                getContext().getContentResolver().notifyChange(uri, null);
                return returnCount;
            }
            case CATEGORIES: {
                db.beginTransaction();
                int returnCount = 0;
                try {
                    for (ContentValues value : values) {
                        long _id = db.insert(EcommerceContract.CategoryEntry.TABLE_NAME, null, value);
                        if (_id != -1) {
                            returnCount++;
                        }
                    }
                    db.setTransactionSuccessful();
                } finally {
                    db.endTransaction();
                }
                getContext().getContentResolver().notifyChange(uri, null);
                return returnCount;
            }
            case SUBCATEGORIES: {
                db.beginTransaction();
                int returnCount = 0;
                try {
                    for (ContentValues value : values) {
                        long _id = db.insert(EcommerceContract.SubcategoryEntry.TABLE_NAME, null, value);
                        if (_id != -1) {
                            returnCount++;
                        }
                    }
                    db.setTransactionSuccessful();
                } finally {
                    db.endTransaction();
                }
                getContext().getContentResolver().notifyChange(uri, null);
                return returnCount;
            }
            case OPTIONGROPS: {
                db.beginTransaction();
                int returnCount = 0;
                try {
                    for (ContentValues value : values) {
                        long _id = db.insert(EcommerceContract.OptiongroupsEntry.TABLE_NAME, null, value);
                        if (_id != -1) {
                            returnCount++;
                        }
                    }
                    db.setTransactionSuccessful();
                } finally {
                    db.endTransaction();
                }
                getContext().getContentResolver().notifyChange(uri, null);
                return returnCount;
            }
            case OPTIONS: {
                db.beginTransaction();
                int returnCount = 0;
                try {
                    for (ContentValues value : values) {
                        long _id = db.insert(EcommerceContract.OptionsEntry.TABLE_NAME, null, value);
                        if (_id != -1) {
                            returnCount++;
                        }
                    }
                    db.setTransactionSuccessful();
                } finally {
                    db.endTransaction();
                }
                getContext().getContentResolver().notifyChange(uri, null);
                return returnCount;
            }
            default:
                return super.bulkInsert(uri, values);
        }
    }
}

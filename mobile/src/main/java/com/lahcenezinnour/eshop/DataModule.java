package com.lahcenezinnour.eshop;

import com.lahcenezinnour.eshop.interactor.InteractorDbImpl;
import com.lahcenezinnour.eshop.interactor.interfaces.IInteractorDb;

import dagger.Module;
import dagger.Provides;

/**
 * Created by lahcene zinnour on 2/28/17.
 */
@Module
public class DataModule {

    @Provides
    public IInteractorDb provideInteractorDb(InteractorDbImpl interactorDb) {
        return interactorDb;
    }
}

package com.lahcenezinnour.eshop.interactor;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.lahcenezinnour.eshop.data.EcommerceContract;
import com.lahcenezinnour.eshop.data.mapper.Mapper;
import com.lahcenezinnour.eshop.interactor.interfaces.IInteractorDb;
import com.lahcenezinnour.eshop.mvp.model.Productcategories;
import com.lahcenezinnour.eshop.mvp.model.Products;
import com.lahcenezinnour.eshop.mvp.model.Subproductcategories;

import java.util.List;
import java.util.Vector;

import javax.inject.Inject;

/**
 * Created by lahcene zinnour on 1/3/17.
 */

public class InteractorDbImpl implements IInteractorDb {

    private Context context;


    @Inject
    public InteractorDbImpl(Context context){
        this.context = context;
    }

    @Override
    public List<Products> getProductsByRangeFromDb(int nb, int i) {
        Cursor cursor = context.getContentResolver().query(EcommerceContract.ProductEntry.CONTENT_URI,
                    null,
                    null,
                    null,
                    null);

        return new Mapper().cursorToProduct(cursor);
    }

    @Override
    public int insertProductsByRangeInDb(List<Products> products) {
        Vector<ContentValues> productsVector = new Vector<>(products.size());


        for (int i = 0; i < products.size(); i++) {
            System.out.println("i = "+i);
            ContentValues productsValue = new ContentValues();
            productsValue.put(EcommerceContract.ProductEntry._ID, products.get(i).getProductID());
            productsValue.put(EcommerceContract.ProductEntry.COLUMN_PRODUCT_DESC, products.get(i).getProductCartDesc());
            productsValue.put(EcommerceContract.ProductEntry.COLUMN_PRODUCT_IMAGE, products.get(i).getProductImage());
            productsValue.put(EcommerceContract.ProductEntry.COLUMN_PRODUCT_LIVE, products.get(i).getProductLive());
            productsValue.put(EcommerceContract.ProductEntry.COLUMN_PRODUCT_LOCATION, products.get(i).getProductLocation());
            productsValue.put(EcommerceContract.ProductEntry.COLUMN_PRODUCT_LONG_DESC, products.get(i).getProductLongDesc());
            productsValue.put(EcommerceContract.ProductEntry.COLUMN_PRODUCT_NAME, products.get(i).getProductName());
            productsValue.put(EcommerceContract.ProductEntry.COLUMN_PRODUCT_PRICE, products.get(i).getProductPrice());
            productsValue.put(EcommerceContract.ProductEntry.COLUMN_PRODUCT_SHORT_DESC, products.get(i).getProductShortDesc());
            productsValue.put(EcommerceContract.ProductEntry.COLUMN_PRODUCT_SKU, products.get(i).getProductSKU());
            productsValue.put(EcommerceContract.ProductEntry.COLUMN_PRODUCT_STOCK, products.get(i).getProductStock());
            productsValue.put(EcommerceContract.ProductEntry.COLUMN_PRODUCT_THUMB, products.get(i).getProductThumb());
            productsValue.put(EcommerceContract.ProductEntry.COLUMN_PRODUCT_UNLIMITED, products.get(i).getProductUnlimited());
            productsValue.put(EcommerceContract.ProductEntry.COLUMN_PRODUCT_UPDATE_DATE, products.get(i).getProductUpdateDate());
            productsValue.put(EcommerceContract.ProductEntry.COLUMN_PRODUCT_WEIGHT, products.get(i).getProductWeight());
            productsValue.put(EcommerceContract.ProductEntry.COLUMN_PRODUCT_ACTIVITY_DISPLAY, products.get(i).getProductActivityDisplay());
            productsValue.put(EcommerceContract.ProductEntry.COLUMN_PRODUCT_ACTIVITY_NAME, products.get(i).getProductActivityName());
            productsValue.put(EcommerceContract.ProductEntry.COLUMN_PRODUCT_CATEGORY_ID, products.get(i).getCategory().getCategoryID());
            productsVector.add(productsValue);
        }

        if (productsVector.size() > 0) {
            ContentValues[] cvArray = new ContentValues[productsVector.size()];
            productsVector.toArray(cvArray);
            return context.getContentResolver().bulkInsert(EcommerceContract.ProductEntry.CONTENT_URI, cvArray);
        }

        return 0;
    }

    @Override
    public List<Productcategories> getAllCategoriesFromDb() {

        Cursor cursor = context.getContentResolver().query(EcommerceContract.CategoryEntry.CONTENT_URI,
                null,
                null,
                null,
                null);

        return new Mapper().cursorToCategory(cursor);
    }

    @Override
    public List<Productcategories> getCategoriesByGenderFromDb(String gender) {
        Cursor cursor = context.getContentResolver().query(EcommerceContract.CategoryEntry.CONTENT_URI,
                null,
                EcommerceContract.CategoryEntry.COLUMN_CATEGORY_GENDER +" =?",
                new String[]{gender},
                null);
        //System.out.println("+++++++++++ Cursor = "+ DatabaseUtils.dumpCursorToString(cursor));
        return new Mapper().cursorToCategory(cursor);
    }

    @Override
    public List<Subproductcategories> getSubcategoriesByCategoryFromDb(int idCategory) {
        System.out.println("id = "  + idCategory);
        Cursor cursor = context.getContentResolver().query(EcommerceContract.SubcategoryEntry.CONTENT_URI,
                null,
                EcommerceContract.SubcategoryEntry.COLUMN_SUBCATEGORY_CATEGORY_ID +" =?",
                new String[]{String.valueOf(idCategory)},
                null);
        //System.out.println("+++++++++++ Cursor = "+ DatabaseUtils.dumpCursorToString(cursor));
        return new Mapper().cursorToSubcategory(cursor);
    }

    @Override
    public List<Products> getProductsByCategoryFromDb(int id, int nb, int i) {
        return null;
    }


}

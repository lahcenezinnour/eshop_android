package com.lahcenezinnour.eshop;


import com.lahcenezinnour.eshop.domain.ApiModule;
import com.lahcenezinnour.eshop.domain.ClientModule;
import com.lahcenezinnour.eshop.injector.component.AdviceSubComponent;
import com.lahcenezinnour.eshop.injector.component.FiltersSubComponent;
import com.lahcenezinnour.eshop.injector.component.MainSubComponent;
import com.lahcenezinnour.eshop.injector.component.ProductsSubComponent;
import com.lahcenezinnour.eshop.injector.component.ShopSubComponent;
import com.lahcenezinnour.eshop.injector.component.SubcategoriesSubComponent;
import com.lahcenezinnour.eshop.injector.module.AdviceModule;
import com.lahcenezinnour.eshop.injector.module.FiltersModule;
import com.lahcenezinnour.eshop.injector.module.MainModule;
import com.lahcenezinnour.eshop.injector.module.ProductsModule;
import com.lahcenezinnour.eshop.injector.module.ShopModule;
import com.lahcenezinnour.eshop.injector.module.SubcategoriesModule;
import com.lahcenezinnour.eshop.ui.activity.AdviceActivity;
import com.lahcenezinnour.eshop.ui.activity.MainActivity;
import com.lahcenezinnour.eshop.ui.activity.ProductsActivity;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by lahcene zinnour on 20/10/2016.
 */

@Singleton
@Component(modules = {
        AndroidModule.class,
        ApplicationModule.class,
        ApiModule.class,
        ClientModule.class,
        DataModule.class,

})
public interface ApplicationComponent {

    void injectMainActivity(MainActivity activity);

    void injectAdviceActivity(AdviceActivity activity);

    void injectProductsActivity(ProductsActivity activity);

    MainSubComponent plus_MainFragment(MainModule module);

    AdviceSubComponent plus_AdviceFragment(AdviceModule module);

    ProductsSubComponent plus_ProductsFragment(ProductsModule module);

    FiltersSubComponent plus_FiltersFragment(FiltersModule module);

    ShopSubComponent plus_ShopFragment(ShopModule module);

    SubcategoriesSubComponent plus_SubcategoriesFragment(SubcategoriesModule module);
}
package com.lahcenezinnour.eshop.injector.component;

import com.lahcenezinnour.eshop.base.BaseFragment;
import com.lahcenezinnour.eshop.injector.module.BaseModule;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;

/**
 * Created by lahcene zinnour on 7/15/17.
 */
@Subcomponent(modules = BaseModule.class)
public interface BaseFragmentSubComponent extends AndroidInjector<BaseFragment> {

    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<BaseFragment> {
        public abstract Builder baseModule(BaseModule module);

        @Override
        public void seedInstance(BaseFragment instance) {
            baseModule(new BaseModule(instance,instance.getActivity()));
        }
    }
}

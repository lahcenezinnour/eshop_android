package com.lahcenezinnour.eshop.injector.component;

import com.lahcenezinnour.eshop.injector.module.ProductsModule;
import com.lahcenezinnour.eshop.ui.fragment.ProductsFragment;

import dagger.Subcomponent;

/**
 * Created by lahcene zinnour on 1/17/17.
 */
@Subcomponent(modules = {ProductsModule.class})
public interface ProductsSubComponent {
    void injectProductsFragment(ProductsFragment fragment);
}

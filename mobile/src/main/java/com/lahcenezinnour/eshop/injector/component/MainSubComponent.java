package com.lahcenezinnour.eshop.injector.component;

import com.lahcenezinnour.eshop.injector.module.MainModule;
import com.lahcenezinnour.eshop.ui.fragment.MainFragment;

import dagger.Subcomponent;

/**
 * Created by lahcene zinnour on 1/17/17.
 */
@Subcomponent(modules = {MainModule.class})
public interface MainSubComponent {
    void injectMainFragment(MainFragment fragment);
}

package com.lahcenezinnour.eshop.injector.module;

import android.content.Context;

import com.lahcenezinnour.eshop.ui.fragment.SubcategoriesFragment;
import com.lahcenezinnour.eshop.ui.interfaces.OnShadowFragmentSubcategoriesInteractionListener;

import dagger.Module;
import dagger.Provides;

/**
 * Created by lahcene zinnour on 1/17/17.
 */
@Module
public class SubcategoriesModule extends SubcategoriesModuleCore {

    OnShadowFragmentSubcategoriesInteractionListener listenerSubcategories;

    public SubcategoriesModule(SubcategoriesFragment fragment, Context context) {
        super(fragment);

        if (context instanceof OnShadowFragmentSubcategoriesInteractionListener) {
            listenerSubcategories = (OnShadowFragmentSubcategoriesInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnShadowFragmentSubcategoriesInteractionListener");
        }

    }

    @Provides
    OnShadowFragmentSubcategoriesInteractionListener provideOnShadowFragmentSubcategoriesInteractionListener() {
        return listenerSubcategories;
    }



}

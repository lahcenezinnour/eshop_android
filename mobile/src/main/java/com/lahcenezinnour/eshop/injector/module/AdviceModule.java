package com.lahcenezinnour.eshop.injector.module;

import android.content.Context;

import com.lahcenezinnour.eshop.mvp.presenter.AdvicePresenterImpl;
import com.lahcenezinnour.eshop.mvp.presenter.interfaces.IAdvicePresenter;
import com.lahcenezinnour.eshop.ui.fragment.AdviceFragment;
import com.lahcenezinnour.eshop.ui.interfaces.OnFragmentShopInteractionListener;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

/**
 * Created by lahcene zinnour on 2/21/17.
 */
@Module
public class AdviceModule extends AdviceModuleCore {

    OnFragmentShopInteractionListener listenerShop;

    public AdviceModule(AdviceFragment fragment, Context context) {
        super(fragment);

        if (context instanceof OnFragmentShopInteractionListener) {
            listenerShop = (OnFragmentShopInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentShopInteractionListener");
        }
    }

    @Provides
    @Named("Advice")
    OnFragmentShopInteractionListener provideOnFragmentShopInteractionListener() {
        return listenerShop;
    }

    @Provides
    public IAdvicePresenter providePresenter(AdvicePresenterImpl presenter) {
        presenter.bind(view);
        return presenter;
    }
}

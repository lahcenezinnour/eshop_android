package com.lahcenezinnour.eshop.injector.module;

import android.content.Context;

import com.lahcenezinnour.eshop.mvp.presenter.ProductsPresenterImpl;
import com.lahcenezinnour.eshop.mvp.presenter.interfaces.IProductsPresenter;
import com.lahcenezinnour.eshop.ui.fragment.ProductsFragment;
import com.lahcenezinnour.eshop.ui.interfaces.OnFragmentFiltersInteractionListener;
import com.lahcenezinnour.eshop.ui.interfaces.OnFragmentShopInteractionListener;
import com.lahcenezinnour.eshop.ui.interfaces.OnFragmentSubcategoriesInteractionListener;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

/**
 * Created by lahcene zinnour on 1/17/17.
 */
@Module
public class ProductsModule extends ProductsModuleCore{

    OnFragmentShopInteractionListener listenerShop;
    OnFragmentFiltersInteractionListener listenerFilters;
    OnFragmentSubcategoriesInteractionListener listenerFSubcategories;

    public ProductsModule(ProductsFragment fragment, Context context) {
        super(fragment);

        if (context instanceof OnFragmentShopInteractionListener) {
            listenerShop = (OnFragmentShopInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentShopInteractionListener");
        }

        if (context instanceof OnFragmentFiltersInteractionListener) {
            listenerFilters = (OnFragmentFiltersInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentFiltersInteractionListener");
        }

        if (context instanceof OnFragmentSubcategoriesInteractionListener) {
            listenerFSubcategories = (OnFragmentSubcategoriesInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentSubcategoriesInteractionListener");
        }
    }


    @Provides
    @Named("products")
    OnFragmentShopInteractionListener provideOnFragmentShopInteractionListener() {
        return listenerShop;
    }

    @Provides
    OnFragmentFiltersInteractionListener provideOnFragmentFiltersInteractionListener() {
        return listenerFilters;
    }

    @Provides
    OnFragmentSubcategoriesInteractionListener provideOnFragmentSubcategoriesInteractionListener() {
        return listenerFSubcategories;
    }

    @Provides
    public IProductsPresenter providePresenter(ProductsPresenterImpl presenter) {
        presenter.bind(view);
        return presenter;
    }

}

package com.lahcenezinnour.eshop.injector.module;

import android.app.Activity;

import com.lahcenezinnour.eshop.base.BaseActivity;
import com.lahcenezinnour.eshop.injector.component.BaseActivitySubComponent;

import dagger.Binds;
import dagger.Module;
import dagger.android.ActivityKey;
import dagger.android.AndroidInjector;
import dagger.multibindings.IntoMap;

/**
 * Created by lahcene zinnour on 7/16/17.
 */

@Module(subcomponents = BaseActivitySubComponent.class)
public abstract class ActivityBuilder {

    @Binds
    @IntoMap
    @ActivityKey(BaseActivity.class)
    abstract AndroidInjector.Factory<? extends Activity> bindBaseActivity(BaseActivitySubComponent.Builder builder);


}
package com.lahcenezinnour.eshop.injector.module;

import android.content.Context;

import com.lahcenezinnour.eshop.ui.fragment.ShopFragment;
import com.lahcenezinnour.eshop.ui.interfaces.OnShadowFragmentShopInteractionListener;

import dagger.Module;
import dagger.Provides;

/**
 * Created by lahcene zinnour on 1/17/17.
 */
@Module
public class ShopModule extends ShopModuleCore{

    OnShadowFragmentShopInteractionListener listenerShadowShop;

    public ShopModule(ShopFragment fragment, Context context) {
        super(fragment);

        if (context instanceof OnShadowFragmentShopInteractionListener) {
            listenerShadowShop = (OnShadowFragmentShopInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnShadowFragmentShopInteractionListener");
        }

    }

    @Provides
    OnShadowFragmentShopInteractionListener provideOnShadowFragmentShopInteractionListener() {
        return listenerShadowShop;
    }

}

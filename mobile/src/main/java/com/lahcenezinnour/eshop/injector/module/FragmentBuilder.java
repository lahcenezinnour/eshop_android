package com.lahcenezinnour.eshop.injector.module;

import android.support.v4.app.Fragment;

import com.lahcenezinnour.eshop.base.BaseFragment;
import com.lahcenezinnour.eshop.injector.component.BaseFragmentSubComponent;

import dagger.Binds;
import dagger.Module;
import dagger.android.AndroidInjector;
import dagger.android.support.FragmentKey;
import dagger.multibindings.IntoMap;

/**
 * Created by lahcene zinnour on 7/16/17.
 */

@Module(subcomponents = {BaseFragmentSubComponent.class})
public abstract class FragmentBuilder {

    @Binds
    @IntoMap
    @FragmentKey(BaseFragment.class)
    abstract AndroidInjector.Factory<? extends Fragment> bindBaseFragment(BaseFragmentSubComponent.Builder builder);


}
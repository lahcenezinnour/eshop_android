package com.lahcenezinnour.eshop.injector.component;

import com.lahcenezinnour.eshop.injector.module.FiltersModule;
import com.lahcenezinnour.eshop.ui.fragment.FiltersFragment;

import dagger.Subcomponent;

/**
 * Created by lahcene zinnour on 1/17/17.
 */
@Subcomponent(modules = {FiltersModule.class})
public interface FiltersSubComponent {
    void injectFiltersFragment(FiltersFragment fragment);
}

package com.lahcenezinnour.eshop.injector.module;

import android.content.Context;

import com.lahcenezinnour.eshop.mvp.presenter.FiltersPresenterImpl;
import com.lahcenezinnour.eshop.mvp.presenter.interfaces.IFiltersPresenter;
import com.lahcenezinnour.eshop.ui.fragment.FiltersFragment;
import com.lahcenezinnour.eshop.ui.interfaces.OnMenuItemFragmentFiltersInteractionListener;
import com.lahcenezinnour.eshop.ui.interfaces.OnShadowFragmentFiltersInteractionListener;

import dagger.Module;
import dagger.Provides;

/**
 * Created by lahcene zinnour on 1/17/17.
 */
@Module
public class FiltersModule extends FiltersModuleCore{

    OnMenuItemFragmentFiltersInteractionListener listenerMenuItemFilters;
    OnShadowFragmentFiltersInteractionListener listenerShadowFilters;

    public FiltersModule(FiltersFragment fragment, Context context) {
        super(fragment);

        if (context instanceof OnMenuItemFragmentFiltersInteractionListener) {
            listenerMenuItemFilters = (OnMenuItemFragmentFiltersInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnMenuItemFragmentFiltersInteractionListener");
        }

        if (context instanceof OnShadowFragmentFiltersInteractionListener) {
            listenerShadowFilters = (OnShadowFragmentFiltersInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnShadowFragmentFiltersInteractionListener");
        }

    }

    @Provides
    OnMenuItemFragmentFiltersInteractionListener provideOnMenuItemFragmentFiltersInteractionListener() {
        return listenerMenuItemFilters;
    }

    @Provides
    OnShadowFragmentFiltersInteractionListener provideOnShadowFragmentFiltersInteractionListener() {
        return listenerShadowFilters;
    }

    @Provides
    public IFiltersPresenter providePresenter(FiltersPresenterImpl presenter) {
        presenter.bind(view);
        return presenter;
    }

}

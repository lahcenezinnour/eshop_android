package com.lahcenezinnour.eshop.injector.component;

import com.lahcenezinnour.eshop.injector.module.ShopModule;
import com.lahcenezinnour.eshop.ui.fragment.ShopFragment;

import dagger.Subcomponent;

/**
 * Created by lahcene zinnour on 1/17/17.
 */
@Subcomponent(modules = {ShopModule.class})
public interface ShopSubComponent {
    void injectShopFragment(ShopFragment fragment);
}

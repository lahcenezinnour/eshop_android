package com.lahcenezinnour.eshop.injector.component;

import com.lahcenezinnour.eshop.injector.module.AdviceModule;
import com.lahcenezinnour.eshop.ui.fragment.AdviceFragment;

import dagger.Subcomponent;

/**
 * Created by lahcene zinnour on 2/21/17.
 */
@Subcomponent(modules = {AdviceModule.class})
public interface AdviceSubComponent {
    void injectAdviceFragment(AdviceFragment fragment);
}

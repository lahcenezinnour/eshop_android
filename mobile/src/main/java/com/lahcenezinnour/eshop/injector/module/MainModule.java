package com.lahcenezinnour.eshop.injector.module;

import android.content.Context;

import com.lahcenezinnour.eshop.injector.module.MainModuleCore;
import com.lahcenezinnour.eshop.mvp.presenter.interfaces.IMainPresenter;
import com.lahcenezinnour.eshop.mvp.presenter.MainPresenterImpl;
import com.lahcenezinnour.eshop.ui.fragment.MainFragment;
import com.lahcenezinnour.eshop.ui.interfaces.OnFragmentLabelInteractionListener;
import com.lahcenezinnour.eshop.ui.interfaces.OnFragmentShopInteractionListener;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

/**
 * Created by lahcene zinnour on 1/17/17.
 */
@Module
public class MainModule extends MainModuleCore {

    OnFragmentShopInteractionListener listenerShop;
    OnFragmentLabelInteractionListener listenerLabel;

    public MainModule(MainFragment fragment, Context context) {
        super(fragment);

        if (context instanceof OnFragmentShopInteractionListener) {
            listenerShop = (OnFragmentShopInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentShopInteractionListener");
        }

        if (context instanceof OnFragmentLabelInteractionListener) {
            listenerLabel = (OnFragmentLabelInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentLabelInteractionListener");
        }

    }

    @Provides
    @Named("main")
    OnFragmentLabelInteractionListener provideOnFragmentLabelInteractionListener() {
        return listenerLabel;
    }

    @Provides
    @Named("main")
    OnFragmentShopInteractionListener provideOnFragmentShopInteractionListener() {
        return listenerShop;
    }

    @Provides
    public IMainPresenter providePresenter(MainPresenterImpl presenter) {
        presenter.bind(view);
        return presenter;
    }

}

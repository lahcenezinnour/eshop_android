package com.lahcenezinnour.eshop.injector.component;

import com.lahcenezinnour.eshop.base.BaseActivity;
import com.lahcenezinnour.eshop.base.BaseFragment;
import com.lahcenezinnour.eshop.base.BaseView;
import com.lahcenezinnour.eshop.injector.module.BaseModule;
import com.lahcenezinnour.eshop.ui.activity.AdviceActivity;
import com.lahcenezinnour.eshop.ui.fragment.MainFragment;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;

/**
 * Created by lahcene zinnour on 7/15/17.
 */
@Subcomponent(modules = BaseModule.class)
public interface BaseActivitySubComponent extends AndroidInjector<BaseActivity> {

    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<BaseActivity> {
        public abstract BaseActivitySubComponent.Builder baseModule(BaseModule module);

        @Override
        public void seedInstance(BaseActivity instance) {
            baseModule(new BaseModule((BaseView) instance));
        }
    }
}

package com.lahcenezinnour.eshop.injector.module;

import android.content.Context;

import com.lahcenezinnour.eshop.base.BaseFragment;
import com.lahcenezinnour.eshop.base.BasePresenter;
import com.lahcenezinnour.eshop.base.BaseView;
import com.lahcenezinnour.eshop.ui.interfaces.OnFragmentLabelInteractionListener;
import com.lahcenezinnour.eshop.ui.interfaces.OnFragmentShopInteractionListener;
import com.lahcenezinnour.eshop.ui.interfaces.OnFragmentSubcategoriesInteractionListener;
import com.lahcenezinnour.eshop.ui.interfaces.OnMenuItemFragmentFiltersInteractionListener;
import com.lahcenezinnour.eshop.ui.interfaces.OnShadowFragmentFiltersInteractionListener;
import com.lahcenezinnour.eshop.ui.interfaces.OnShadowFragmentShopInteractionListener;
import com.lahcenezinnour.eshop.ui.interfaces.OnShadowFragmentSubcategoriesInteractionListener;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

/**
 * Created by lahcene zinnour on 7/16/17.
 */
@Module
public class BaseModule extends BaseModuleCore {

    OnFragmentShopInteractionListener listenerShop;
    OnMenuItemFragmentFiltersInteractionListener listenerMenuItemFilters;
    OnShadowFragmentFiltersInteractionListener listenerShadowFilters;
    OnFragmentLabelInteractionListener listenerLabel;
    OnFragmentSubcategoriesInteractionListener listenerFSubcategories;
    OnShadowFragmentShopInteractionListener listenerShadowShop;
    OnShadowFragmentSubcategoriesInteractionListener listenerSubcategories;

    public BaseModule(BaseView view) {
        super(view);
    }

    public BaseModule(BaseFragment fragment, Context context) {
        super(fragment);

        if (context instanceof OnFragmentShopInteractionListener) {
            listenerShop = (OnFragmentShopInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentShopInteractionListener");
        }
        if (context instanceof OnMenuItemFragmentFiltersInteractionListener) {
            listenerMenuItemFilters = (OnMenuItemFragmentFiltersInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnMenuItemFragmentFiltersInteractionListener");
        }

        if (context instanceof OnShadowFragmentFiltersInteractionListener) {
            listenerShadowFilters = (OnShadowFragmentFiltersInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnShadowFragmentFiltersInteractionListener");
        }
        if (context instanceof OnFragmentLabelInteractionListener) {
            listenerLabel = (OnFragmentLabelInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentLabelInteractionListener");
        }
        if (context instanceof OnFragmentSubcategoriesInteractionListener) {
            listenerFSubcategories = (OnFragmentSubcategoriesInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentSubcategoriesInteractionListener");
        }
        if (context instanceof OnShadowFragmentShopInteractionListener) {
            listenerShadowShop = (OnShadowFragmentShopInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnShadowFragmentShopInteractionListener");
        }
        if (context instanceof OnShadowFragmentSubcategoriesInteractionListener) {
            listenerSubcategories = (OnShadowFragmentSubcategoriesInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnShadowFragmentSubcategoriesInteractionListener");
        }

    }


    @Provides
    @Named("Advice")
    OnFragmentShopInteractionListener provideOnFragmentShopInteractionListener() {
        return listenerShop;
    }

    @Provides
    OnMenuItemFragmentFiltersInteractionListener provideOnMenuItemFragmentFiltersInteractionListener() {
        return listenerMenuItemFilters;
    }

    @Provides
    OnShadowFragmentFiltersInteractionListener provideOnShadowFragmentFiltersInteractionListener() {
        return listenerShadowFilters;
    }

    @Provides
    @Named("main")
    OnFragmentLabelInteractionListener provideOnFragmentLabelInteractionListener() {
        return listenerLabel;
    }
    @Provides
    OnFragmentSubcategoriesInteractionListener provideOnFragmentSubcategoriesInteractionListener() {
        return listenerFSubcategories;
    }
    @Provides
    OnShadowFragmentShopInteractionListener provideOnShadowFragmentShopInteractionListener() {
        return listenerShadowShop;
    }
    @Provides
    OnShadowFragmentSubcategoriesInteractionListener provideOnShadowFragmentSubcategoriesInteractionListener() {
        return listenerSubcategories;
    }


    @Provides
    public BasePresenter<BaseView> providePresenter(BasePresenter<BaseView> presenter) {
        presenter.bind(view);
        return presenter;
    }
}

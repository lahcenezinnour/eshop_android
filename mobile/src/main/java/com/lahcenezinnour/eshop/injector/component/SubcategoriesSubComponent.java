package com.lahcenezinnour.eshop.injector.component;

import com.lahcenezinnour.eshop.injector.module.SubcategoriesModule;
import com.lahcenezinnour.eshop.ui.fragment.SubcategoriesFragment;

import dagger.Subcomponent;

/**
 * Created by lahcene zinnour on 1/17/17.
 */
@Subcomponent(modules = {SubcategoriesModule.class})
public interface SubcategoriesSubComponent {
    void injectSubcategoriesFragment(SubcategoriesFragment fragment);
}

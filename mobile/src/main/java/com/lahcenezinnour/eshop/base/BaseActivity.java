package com.lahcenezinnour.eshop.base;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.lahcenezinnour.eshop.ApplicationComponent;
import com.lahcenezinnour.eshop.EshopApplication;

/**
 * Created by lahcene zinnour on 20/10/2016.
 */

public abstract class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        injectDependencies(EshopApplication.getComponent());

        // can be used for general purpose in all Activities of Application
    }

    protected abstract void injectDependencies(ApplicationComponent component);

    @Override
    public void finish() {
        super.finish();

        releaseSubComponents(EshopApplication.get(this));
    }

    protected abstract void releaseSubComponents(EshopApplication application);

}

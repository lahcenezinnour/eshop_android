package com.lahcenezinnour.eshop.ui.fragment;


import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.lahcenezinnour.eshop.ApplicationComponent;
import com.lahcenezinnour.eshop.R;
import com.lahcenezinnour.eshop.base.BaseFragment;
import com.lahcenezinnour.eshop.data.mapper.Mapper;
import com.lahcenezinnour.eshop.injector.module.SubcategoriesModule;
import com.lahcenezinnour.eshop.interactor.interfaces.IInteractorDb;
import com.lahcenezinnour.eshop.mvp.model.Subproductcategories;
import com.lahcenezinnour.eshop.mvp.view.ISubcategoriesView;
import com.lahcenezinnour.eshop.ui.adapters.SubCategoryAdapter;
import com.lahcenezinnour.eshop.ui.adapters.model.SubCategoryItem;
import com.lahcenezinnour.eshop.ui.interfaces.OnShadowFragmentSubcategoriesInteractionListener;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class SubcategoriesFragment extends BaseFragment implements ISubcategoriesView {

    @Inject
    IInteractorDb interactorDb;
    @Inject
    OnShadowFragmentSubcategoriesInteractionListener listenerShadowSubcategories;

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    SubCategoryAdapter adapter;
    List<SubCategoryItem> subCategoryItems;
    List<Subproductcategories> subproductcategories;

    public SubcategoriesFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_subcategories, container, false);
        ButterKnife.bind(this, view);

        Intent intent = getActivity().getIntent();
        Bundle bundle = getArguments();

        if (bundle == null) {
            System.out.println( "SubcategoriesFragment bundle is null " );
        } else {
            //subCategoryItems = (List<SubCategoryItem>) bundle.getSerializable("subCategories");
            System.out.println( "idsubCategory =  " + intent.getIntExtra("idCategory", -1));
            subproductcategories = interactorDb.getSubcategoriesByCategoryFromDb(intent.getIntExtra("idCategory", -1));
        }
        subCategoryItems = new Mapper().subCategoryToSubCategoryItem(subproductcategories);

        System.out.println( "subCategoryItems =  " + subCategoryItems);
        adapter = new SubCategoryAdapter(subCategoryItems);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(adapter);
        return view;
    }

    @OnClick(R.id.shadow)
    public void clickShadow() {
        listenerShadowSubcategories.onClickShadowFragmentSubcategories();
    }

    @Override
    protected void injectDependencies(ApplicationComponent component, Context context) {
        component.plus_SubcategoriesFragment(new SubcategoriesModule(this, context)).injectSubcategoriesFragment(this);
    }

    @Override
    public void showMessage(String message) {

    }

    @Override
    public void showOfflineMessage() {

    }

    @Override
    public void showRetryMessage(Throwable throwable) {

    }

    @Override
    public void showNetworkError(Throwable throwable) {

    }

    @Override
    public void showServerError(Throwable throwable) {

    }

    @Override
    public void networkError() {

    }
}

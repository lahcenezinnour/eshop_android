package com.lahcenezinnour.eshop.ui.fragment;


import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.lahcenezinnour.eshop.ApplicationComponent;
import com.lahcenezinnour.eshop.R;
import com.lahcenezinnour.eshop.base.BaseFragment;
import com.lahcenezinnour.eshop.mvp.model.Productcategories;

import java.util.List;

import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class AllFragment extends BaseFragment {


    public AllFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_all, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    protected void injectDependencies(ApplicationComponent component, Context context) {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();

        if (bundle == null) {
            System.out.println( "bundle is null " );
        } else {
            System.out.println( "text " + bundle);
        }


        List<Productcategories> all = (List<Productcategories>) bundle.getSerializable("all");

        System.out.println("*******  all = "+ all);
    }
}

package com.lahcenezinnour.eshop.ui.activity;

import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.lahcenezinnour.eshop.ApplicationComponent;
import com.lahcenezinnour.eshop.EshopApplication;
import com.lahcenezinnour.eshop.R;
import com.lahcenezinnour.eshop.base.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CheckoutActivity extends BaseActivity {

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.codeText) EditText code;
    @BindView(R.id.codeBtn) Button codeBtn;
    @BindView(R.id.constraintLayoutCoupon) ConstraintLayout constraintLayoutCoupon;
    @BindView(R.id.close) ImageView imageViewCLose;
    @BindView(R.id.countryConstraintLayout) ConstraintLayout countryConstraintLayout;
    @BindView(R.id.nameConstraintLayout) ConstraintLayout nameConstraintLayout;
    @BindView(R.id.addressConstraintLayout1) ConstraintLayout addressConstraintLayout1;
    @BindView(R.id.addressConstraintLayout2) ConstraintLayout addressConstraintLayout2;
    @BindView(R.id.town_cityConstraintLayout) ConstraintLayout town_cityConstraintLayout;
    @BindView(R.id.zipConstraintLayout) ConstraintLayout zipConstraintLayout;
    @BindView(R.id.phoneConstraintLayout) ConstraintLayout phoneConstraintLayout;
    @BindView(R.id.emailConstraintLayout) ConstraintLayout emailConstraintLayout;
    @BindView(R.id.button2) Button nextBtn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        /*toolbar.setElevation(0);


        code.setBackground(customDrawable.setCornerView(8, ContextCompat.getColor(this, R.color.code_background)));
        codeBtn.setBackground(customDrawable.setCornerView(8, ContextCompat.getColor(this, android.R.color.white)));
        codeBtn.setClickable(true);

        countryConstraintLayout.setBackground(customDrawable.setCornerView(8, ContextCompat.getColor(this, android.R.color.white)));
        nameConstraintLayout.setBackground(customDrawable.setCornerView(8, ContextCompat.getColor(this, android.R.color.white)));
        addressConstraintLayout1.setBackground(customDrawable.setCornerView(8, ContextCompat.getColor(this, android.R.color.white)));
        addressConstraintLayout2.setBackground(customDrawable.setCornerView(8, ContextCompat.getColor(this, android.R.color.white)));
        town_cityConstraintLayout.setBackground(customDrawable.setCornerView(8, ContextCompat.getColor(this, android.R.color.white)));
        zipConstraintLayout.setBackground(customDrawable.setCornerView(8, ContextCompat.getColor(this, android.R.color.white)));
        phoneConstraintLayout.setBackground(customDrawable.setCornerView(8, ContextCompat.getColor(this, android.R.color.white)));
        emailConstraintLayout.setBackground(customDrawable.setCornerView(8, ContextCompat.getColor(this, android.R.color.white)));
        nextBtn.setBackground(customDrawable.setCornerView(8, ContextCompat.getColor(this, R.color.next_order_background)));
*/
        imageViewCLose.setOnClickListener(view -> {
            constraintLayoutCoupon.setVisibility(View.GONE);
            Snackbar.make(view, "No Coupon !!", Snackbar.LENGTH_LONG)
                    .setAction("Cancel", view1 -> constraintLayoutCoupon.setVisibility(View.VISIBLE)).show();
        });

    }

    @Override
    protected void injectDependencies(ApplicationComponent component) {

    }

    @Override
    protected void releaseSubComponents(EshopApplication application) {

    }
}

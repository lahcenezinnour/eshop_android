package com.lahcenezinnour.eshop.ui.fragment;


import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.lahcenezinnour.eshop.ApplicationComponent;
import com.lahcenezinnour.eshop.R;
import com.lahcenezinnour.eshop.base.BaseFragment;
import com.lahcenezinnour.eshop.injector.module.ProductsModule;
import com.lahcenezinnour.eshop.mvp.model.Products;
import com.lahcenezinnour.eshop.mvp.presenter.interfaces.IProductsPresenter;
import com.lahcenezinnour.eshop.mvp.view.IProductsView;
import com.lahcenezinnour.eshop.ui.interfaces.OnFragmentFiltersInteractionListener;
import com.lahcenezinnour.eshop.ui.interfaces.OnFragmentShopInteractionListener;
import com.lahcenezinnour.eshop.ui.interfaces.OnFragmentSubcategoriesInteractionListener;
import com.lahcenezinnour.eshop.util.Animations;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProductsFragment extends BaseFragment implements IProductsView {

    @Inject
    Context context;
    @Inject
    Resources resources;
    @Inject
    Animations animation;
    @Inject
    IProductsPresenter productsPresenter;
    @Inject
    @Named("products")
    OnFragmentShopInteractionListener listenerShop;
    @Inject
    OnFragmentFiltersInteractionListener listenerFilters;
    @Inject
    OnFragmentSubcategoriesInteractionListener listenerSubcategories;

    public ProductsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_products, container, false);
    }

    @Override
    protected void injectDependencies(ApplicationComponent component, Context context) {
        component.plus_ProductsFragment(new ProductsModule(this, context)).injectProductsFragment(this);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listenerShop = null;
        listenerFilters = null;
        listenerSubcategories = null;

        productsPresenter.unbind();
        productsPresenter = null;
    }

    public OnFragmentShopInteractionListener getListenerShop() {
        return listenerShop;
    }

    public OnFragmentFiltersInteractionListener getListenerFilters() {
        return listenerFilters;
    }

    public OnFragmentSubcategoriesInteractionListener getListenerSubcategories() {
        return listenerSubcategories;
    }

    @Override
    public void showProductsByRange(List<Products> products) {

    }

    @Override
    public void saveProductByRangeInDB(List<Products> character) {

    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void showMessage(String message) {

    }

    @Override
    public void showOfflineMessage() {

    }

    @Override
    public void showRetryMessage(Throwable throwable) {

    }

    @Override
    public void showNetworkError(Throwable throwable) {

    }

    @Override
    public void showServerError(Throwable throwable) {

    }

    @Override
    public void showError(Throwable throwable) {

    }

    @Override
    public void showServiceError() {

    }

    @Override
    public void onLoadMore(List<Products> products) {

    }

    @Override
    public void networkError() {

    }
}

package com.lahcenezinnour.eshop.ui.fragment;


import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;

import com.lahcenezinnour.eshop.ApplicationComponent;
import com.lahcenezinnour.eshop.R;
import com.lahcenezinnour.eshop.base.BaseFragment;
import com.lahcenezinnour.eshop.injector.module.FiltersModule;
import com.lahcenezinnour.eshop.mvp.view.IFiltersView;
import com.lahcenezinnour.eshop.ui.interfaces.OnMenuItemFragmentFiltersInteractionListener;
import com.lahcenezinnour.eshop.ui.interfaces.OnShadowFragmentFiltersInteractionListener;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class FiltersFragment extends BaseFragment implements IFiltersView {
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.gridViewColor)
    GridView gridViewColor;
    @BindView(R.id.menu)
    ImageView imageViewMenu;
    @BindView(R.id.shadow)
    View viewShadow;

    @Inject
    OnMenuItemFragmentFiltersInteractionListener listenerMenuItemFilters;
    @Inject
    OnShadowFragmentFiltersInteractionListener listenerShadowFilters;

    public FiltersFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_filters, container, false);
        ButterKnife.bind(this, view);
        mToolbar.setTitle("Filters");

        imageViewMenu.setOnClickListener(view1 -> listenerMenuItemFilters.onClickMenuItemFragmentFilters());

        return view;
    }

    @OnClick(R.id.shadow)
    public void clickShadow(View view) {
        listenerShadowFilters.onClickShadowFragmentFilters();
    }

    @Override
    protected void injectDependencies(ApplicationComponent component, Context context) {
        component.plus_FiltersFragment(new FiltersModule(this, context)).injectFiltersFragment(this);
    }

    @Override
    public void showMessage(String message) {

    }

    @Override
    public void showOfflineMessage() {

    }

    @Override
    public void showRetryMessage(Throwable throwable) {

    }

    @Override
    public void showNetworkError(Throwable throwable) {

    }

    @Override
    public void showServerError(Throwable throwable) {

    }

    @Override
    public void networkError() {

    }
}

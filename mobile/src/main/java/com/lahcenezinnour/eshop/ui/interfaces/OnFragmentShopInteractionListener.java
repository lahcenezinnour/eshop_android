package com.lahcenezinnour.eshop.ui.interfaces;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

/**
 * Created by lahcene zinnour on 2/21/17.
 */

public interface OnFragmentShopInteractionListener {

    void showFragmentShop(Toolbar toolbar, Button button, FrameLayout frameLayout, TextView textView);

    void hideFragmentShop(Toolbar toolbar, Button button, FrameLayout frameLayout, TextView textView, Fragment fragment, FragmentManager fragmentManager);

}

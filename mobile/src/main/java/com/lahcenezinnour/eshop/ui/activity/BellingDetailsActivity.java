package com.lahcenezinnour.eshop.ui.activity;

import android.os.Bundle;

import com.lahcenezinnour.eshop.ApplicationComponent;
import com.lahcenezinnour.eshop.EshopApplication;
import com.lahcenezinnour.eshop.R;
import com.lahcenezinnour.eshop.base.BaseActivity;

import butterknife.ButterKnife;

public class BellingDetailsActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_belling_details);
        ButterKnife.bind(this);



    }

    @Override
    protected void injectDependencies(ApplicationComponent component) {

    }

    @Override
    protected void releaseSubComponents(EshopApplication application) {

    }


}

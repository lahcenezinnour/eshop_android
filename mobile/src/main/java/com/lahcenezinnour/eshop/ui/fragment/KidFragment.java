package com.lahcenezinnour.eshop.ui.fragment;


import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.lahcenezinnour.eshop.ApplicationComponent;
import com.lahcenezinnour.eshop.R;
import com.lahcenezinnour.eshop.base.BaseFragment;

import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class KidFragment extends BaseFragment {


    public KidFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_kid, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    protected void injectDependencies(ApplicationComponent component, Context context) {

    }

}

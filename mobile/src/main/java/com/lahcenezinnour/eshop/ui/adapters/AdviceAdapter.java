package com.lahcenezinnour.eshop.ui.adapters;

import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.lahcenezinnour.eshop.R;
import com.lahcenezinnour.eshop.mvp.model.Productcategories;

import java.util.List;

/**
 * Created by lahcene zinnour on 2/22/17.
 */

public class AdviceAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements View.OnClickListener {

    private GridLayoutManager mLayoutManager;
    private List<Productcategories> productcategoriesList;
    private NetworkImageView mNetworkImageView;
    private ImageLoader mImageLoader;

    public AdviceAdapter(List<Productcategories> productcategories) {
        this.productcategoriesList = productcategories;
    }

    public class CategoryHolder extends RecyclerView.ViewHolder {
        public NetworkImageView image;
        public TextView title;

        public CategoryHolder(View view) {
            super(view);
            image = (NetworkImageView) view.findViewById(R.id.image_category);
            title = (TextView) view.findViewById(R.id.title_category);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new AdviceAdapter.CategoryHolder(LayoutInflater.from(parent.getContext()).inflate(
                R.layout.layout_category, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Productcategories productcategories = productcategoriesList.get(position);
        //mImageLoader.get(productcategories.getProductImage(), ImageLoader.getImageListener(((ProductsAdapter2.ProductsHolder) holder).image, android.R.color.white, android.R.drawable.ic_dialog_alert));
        //((ProductsAdapter2.ProductsHolder) holder).image.setImageUrl(product.getProductImage(), mImageLoader);
        ((AdviceAdapter.CategoryHolder) holder).title.setText(productcategories.getCategoryName());

    }

    @Override
    public int getItemCount() {
        System.out.println("++++++++  size = "+productcategoriesList.size());
        return productcategoriesList.size();
    }

    @Override
    public void onClick(View view) {

    }

}

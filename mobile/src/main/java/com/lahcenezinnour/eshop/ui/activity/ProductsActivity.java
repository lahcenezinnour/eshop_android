package com.lahcenezinnour.eshop.ui.activity;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jakewharton.rxbinding.view.RxView;
import com.lahcenezinnour.eshop.ApplicationComponent;
import com.lahcenezinnour.eshop.EshopApplication;
import com.lahcenezinnour.eshop.R;
import com.lahcenezinnour.eshop.base.BaseActivity;
import com.lahcenezinnour.eshop.ui.adapters.ExpandableListAdapter;
import com.lahcenezinnour.eshop.ui.adapters.model.ExpandedMenuModel;
import com.lahcenezinnour.eshop.ui.fragment.FiltersFragment;
import com.lahcenezinnour.eshop.ui.fragment.ProductsFragment;
import com.lahcenezinnour.eshop.ui.fragment.ShopFragment;
import com.lahcenezinnour.eshop.ui.fragment.SubcategoriesFragment;
import com.lahcenezinnour.eshop.ui.interfaces.OnFragmentFiltersInteractionListener;
import com.lahcenezinnour.eshop.ui.interfaces.OnFragmentShopInteractionListener;
import com.lahcenezinnour.eshop.ui.interfaces.OnFragmentSubcategoriesInteractionListener;
import com.lahcenezinnour.eshop.ui.interfaces.OnMenuItemFragmentFiltersInteractionListener;
import com.lahcenezinnour.eshop.ui.interfaces.OnShadowFragmentFiltersInteractionListener;
import com.lahcenezinnour.eshop.ui.interfaces.OnShadowFragmentShopInteractionListener;
import com.lahcenezinnour.eshop.ui.interfaces.OnShadowFragmentSubcategoriesInteractionListener;
import com.lahcenezinnour.eshop.util.Animations;
import com.lahcenezinnour.eshop.util.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProductsActivity extends BaseActivity implements OnFragmentShopInteractionListener, OnFragmentFiltersInteractionListener , OnFragmentSubcategoriesInteractionListener, OnMenuItemFragmentFiltersInteractionListener, OnShadowFragmentShopInteractionListener, OnShadowFragmentFiltersInteractionListener, OnShadowFragmentSubcategoriesInteractionListener {

    public static final String PRODUCTS_FRAGMENT = "PRODUCTS_FRAGMENT";
    public static final String SHOP_FRAGMENT = "SHOP_FRAGMENT";
    public static final String FILTERS_FRAGMENT = "FILTERS_FRAGMENT";
    public static final String SUBCATEGORIES_FRAGMENT = "SUBCATEGORIES_FRAGMENT";

    static boolean FRAGMENT_SHOP_IN = true;
    static boolean FRAGMENT_FILTERS_IN = true;
    static boolean FRAGMENT_SUBCATEGORIES_IN = true;

    @Inject
    Resources resources;
    @Inject
    Animations animation;
    @Inject
    Utils utils;

    protected ProductsFragment productsFragment;
    protected ShopFragment shopFragment;
    protected FiltersFragment filtersFragment;
    protected SubcategoriesFragment subcategoriesFragment;

    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.nav_view)
    NavigationView mNavigationView;
    @BindView(R.id.expandableListView)
    ExpandableListView mExpandableListView;
    @BindView(R.id.layoutSubcategories)
    ConstraintLayout  mConstraintLayoutSubcategories;
    @BindView(R.id.arrow)
    ImageView mImageViewArrow;

    protected FrameLayout mFrameLayoutProducts, mFrameLayoutShop, mFrameLayoutFilters, mFrameLayoutSubcategories;

    TextView mTextViewNotification;
    Button mBtnShop, mBtnFilters;
    ConstraintLayout mConstraintLayoutCount;
    RelativeLayout mRelativeLayoutShop, mRelativeLayoutFilters;
    Menu menu;
    ExpandableListAdapter mMenuAdapter;
    List<ExpandedMenuModel> listDataHeader;
    HashMap<ExpandedMenuModel, List<String>> listDataChild;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_products);
        ButterKnife.bind(this);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        prepareListData();
        mMenuAdapter = new ExpandableListAdapter(this, listDataHeader, listDataChild);
        mExpandableListView.setAdapter(mMenuAdapter);
        mExpandableListView.setOnChildClickListener((expandableListView, view, i, i1, l) -> {
            Log.d("DEBUG", "submenu item clicked" + i);

            return false;
        });
        mExpandableListView.setOnGroupClickListener((expandableListView, view, i, l) -> {
            Log.d("DEBUG", "heading clicked" + i);


            return false;
        });

        mExpandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            int previousItem = -1;

            @Override
            public void onGroupExpand(int groupPosition) {
                if (groupPosition != previousItem)
                    mExpandableListView.collapseGroup(previousItem);
                previousItem = groupPosition;
            }
        });
        if (mNavigationView != null) {
            setupDrawerContent(mNavigationView, drawer);
        }

        mImageViewArrow.setBackgroundResource(R.drawable.arrow_right);
        productsFragment = (ProductsFragment) getSupportFragmentManager().findFragmentByTag(PRODUCTS_FRAGMENT);
        shopFragment = (ShopFragment) getSupportFragmentManager().findFragmentByTag(SHOP_FRAGMENT);
        filtersFragment = (FiltersFragment) getSupportFragmentManager().findFragmentByTag(FILTERS_FRAGMENT);
        subcategoriesFragment = (SubcategoriesFragment) getSupportFragmentManager().findFragmentByTag(SUBCATEGORIES_FRAGMENT);

        if (productsFragment == null) {
            productsFragment = new ProductsFragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_products, productsFragment, PRODUCTS_FRAGMENT).commit();
        }
        if (shopFragment == null) {
            shopFragment = new ShopFragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_shop, shopFragment, SHOP_FRAGMENT).commit();
        }
        if (filtersFragment == null) {
            filtersFragment = new FiltersFragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_filters, filtersFragment, FILTERS_FRAGMENT).commit();
        }
        if (subcategoriesFragment == null) {
            Bundle bundleSubcategoryFragment = new Bundle();
            bundleSubcategoryFragment.putInt("idCategory", getIntent().getIntExtra("idCategory", -1));
            subcategoriesFragment = new SubcategoriesFragment();
            subcategoriesFragment.setArguments(bundleSubcategoryFragment);
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_subcategories, subcategoriesFragment, SUBCATEGORIES_FRAGMENT).commit();
        }

        if (FRAGMENT_SHOP_IN) {
            getSupportFragmentManager().beginTransaction().hide(shopFragment).commit();
        } else {
            mToolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimary2));
            getSupportFragmentManager().beginTransaction().show(shopFragment).commit();
        }
        if (FRAGMENT_FILTERS_IN) {
            getSupportFragmentManager().beginTransaction().hide(filtersFragment).commit();
        } else {
            getSupportFragmentManager().beginTransaction().show(filtersFragment).commit();
        }
        if (FRAGMENT_SUBCATEGORIES_IN) {
            getSupportFragmentManager().beginTransaction().hide(subcategoriesFragment).commit();
        }else {
            mConstraintLayoutSubcategories.setBackgroundColor(getResources().getColor(R.color.layout_fragment_products));
            mToolbar.setBackgroundColor(getResources().getColor(R.color.layout_fragment_products));
            getSupportFragmentManager().beginTransaction().show(subcategoriesFragment).commit();
        }

        menu = mToolbar.getMenu();
        getMenuInflater().inflate(R.menu.main, menu);
        MenuItem item1 = menu.findItem(R.id.menu);
        MenuItemCompat.setActionView(item1, R.layout.notification_update_count_layout);

        mConstraintLayoutCount = (ConstraintLayout) MenuItemCompat.getActionView(item1);

        mTextViewNotification = (TextView) mConstraintLayoutCount.findViewById(R.id.badge_notification_1);

        mRelativeLayoutShop = (RelativeLayout) mConstraintLayoutCount.findViewById(R.id.relative_layout_item_count1);
        mRelativeLayoutFilters = (RelativeLayout) mConstraintLayoutCount.findViewById(R.id.relative_layout_item_count2);

        mBtnShop = (Button) mConstraintLayoutCount.findViewById(R.id.button1);
        mBtnFilters = (Button) mConstraintLayoutCount.findViewById(R.id.button2);
        mBtnFilters.setBackgroundResource(R.drawable.filters);

        mFrameLayoutShop = (FrameLayout) findViewById(R.id.fragment_shop);
        mFrameLayoutFilters = (FrameLayout) findViewById(R.id.fragment_filters);
        mFrameLayoutSubcategories = (FrameLayout) findViewById(R.id.fragment_subcategories);

        animation.translateOutAnim2(mFrameLayoutShop, null, null);
        animation.translateOutAnim2(mFrameLayoutFilters, null, null);

        //mFrameLayoutShop.setVisibility(View.GONE);
        //mFrameLayoutLabel.setVisibility(View.GONE);
        
        RxView.clicks(mRelativeLayoutShop)
                .subscribe(aVoid ->
                        {
                            if (FRAGMENT_SHOP_IN) {
                                if (!FRAGMENT_FILTERS_IN) {
                                    productsFragment.getListenerFilters().hideFragmentFilters(mFrameLayoutFilters, filtersFragment, getSupportFragmentManager());
                                }
                                if (!FRAGMENT_SUBCATEGORIES_IN) {
                                    productsFragment.getListenerSubcategories().hideFragmentSubcategories(mFrameLayoutSubcategories, mConstraintLayoutSubcategories, mToolbar, mImageViewArrow, subcategoriesFragment, getSupportFragmentManager());
                                }
                                getSupportFragmentManager().beginTransaction().show(shopFragment).commit();
                                productsFragment.getListenerShop().showFragmentShop(mToolbar, mBtnShop, mFrameLayoutShop, mTextViewNotification);

                            } else {
                                productsFragment.getListenerShop().hideFragmentShop(mToolbar, mBtnShop, mFrameLayoutShop, mTextViewNotification, shopFragment, getSupportFragmentManager());
                            }
                        }
                );

        RxView.clicks(mRelativeLayoutFilters)
                .subscribe(aVoid ->
                        {
                            if (FRAGMENT_FILTERS_IN) {
                                if (!FRAGMENT_SHOP_IN) {
                                    productsFragment.getListenerShop().hideFragmentShop(mToolbar, mBtnShop, mFrameLayoutShop, mTextViewNotification, shopFragment, getSupportFragmentManager());
                                }
                                if (!FRAGMENT_SUBCATEGORIES_IN) {
                                    productsFragment.getListenerSubcategories().hideFragmentSubcategories(mFrameLayoutSubcategories, mConstraintLayoutSubcategories, mToolbar, mImageViewArrow, subcategoriesFragment, getSupportFragmentManager());
                                }
                                getSupportFragmentManager().beginTransaction().show(filtersFragment).commit();
                                productsFragment.getListenerFilters().showFragmentFilters(mFrameLayoutFilters);
                            }/* else {
                                getSupportFragmentManager().beginTransaction().hide(filtersFragment).commit();
                                productsFragment.getListenerFilters().hideFragmentFilters(mFrameLayoutFilters, mViewShadowFilters);
                            }*/
                        }
                );

        RxView.clicks(mConstraintLayoutSubcategories)
                .subscribe(aVoid ->
                        {
                            if (FRAGMENT_SUBCATEGORIES_IN) {
                                if (!FRAGMENT_SHOP_IN) {
                                    productsFragment.getListenerShop().hideFragmentShop(mToolbar, mBtnShop, mFrameLayoutShop, mTextViewNotification, shopFragment, getSupportFragmentManager());
                                }
                                if (!FRAGMENT_FILTERS_IN) {
                                    productsFragment.getListenerFilters().hideFragmentFilters(mFrameLayoutFilters, filtersFragment, getSupportFragmentManager());
                                }
                                getSupportFragmentManager().beginTransaction().show(subcategoriesFragment).commit();
                                productsFragment.getListenerSubcategories().showFragmentSubcategories(mFrameLayoutSubcategories, mConstraintLayoutSubcategories, mToolbar, mImageViewArrow);
                            } else {
                                productsFragment.getListenerSubcategories().hideFragmentSubcategories(mFrameLayoutSubcategories, mConstraintLayoutSubcategories, mToolbar, mImageViewArrow, subcategoriesFragment, getSupportFragmentManager());
                            }
                        }
                );
    }


    @Override
    public void onBackPressed() {
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void injectDependencies(ApplicationComponent component) {
        component.injectProductsActivity(this);
    }

    @Override
    protected void releaseSubComponents(EshopApplication application) {

    }

    private void setupDrawerContent(NavigationView navigationView, DrawerLayout mDrawerLayout) {
        //revision: this don't works, use setOnChildClickListener() and setOnGroupClickListener() above instead
        navigationView.setNavigationItemSelectedListener(
                menuItem -> {
                    menuItem.setChecked(true);
                    mDrawerLayout.closeDrawers();
                    return true;
                });

    }


    private void prepareListData() {
        listDataHeader = new ArrayList<>();
        listDataChild = new HashMap<>();

        ExpandedMenuModel item1 = new ExpandedMenuModel();
        item1.setIcon(R.drawable.clothings);
        item1.setTitle("CLOTHINGS");
        item1.setIndicator(R.drawable.arrow_down);
        // Adding data header
        listDataHeader.add(item1);

        ExpandedMenuModel item2 = new ExpandedMenuModel();
        item2.setIcon(R.drawable.shoes);
        item2.setTitle("SHOES");
        item2.setIndicator(R.drawable.arrow_down);
        listDataHeader.add(item2);

        ExpandedMenuModel item3 = new ExpandedMenuModel();
        item3.setIcon(R.drawable.sports);
        item3.setTitle("SPORTS");
        item3.setIndicator(R.drawable.arrow_down);
        listDataHeader.add(item3);

        ExpandedMenuModel item4 = new ExpandedMenuModel();
        item4.setIcon(R.drawable.bags);
        item4.setTitle("BAGS & ACCESSORY");
        item4.setIndicator(R.drawable.arrow_down);
        listDataHeader.add(item4);

        ExpandedMenuModel item5 = new ExpandedMenuModel();
        item5.setIcon(R.drawable.account);
        item5.setTitle("ACCOUNT");
        item5.setIndicator(R.drawable.arrow_down);
        listDataHeader.add(item5);

        ExpandedMenuModel item6 = new ExpandedMenuModel();
        item6.setIcon(R.drawable.settings);
        item6.setTitle("SETTINGS");
        item6.setIndicator(R.drawable.arrow_down);
        listDataHeader.add(item6);

        // Adding child data
        List<String> heading1 = new ArrayList<>();
        heading1.add("New");

        List<String> heading2 = new ArrayList<>();
        heading2.add("Sneakers");
        heading2.add("Sandals");
        heading2.add("Boots");
        heading2.add("Slippers");

        listDataChild.put(listDataHeader.get(0), heading1);// Header, Child data
        listDataChild.put(listDataHeader.get(1), heading2);

    }

    @Override
    public void showFragmentShop(Toolbar toolbar, Button button, FrameLayout frameLayout, TextView textView) {
        FRAGMENT_SHOP_IN = false;
        toolbar.setBackgroundColor(resources.getColor(R.color.colorPrimary2));
        animation.alphaIn(button);
        button.setBackgroundResource(R.drawable.close);
        animation.alphaOut(button);
        animation.translateInAnim(frameLayout);
        textView.setVisibility(View.GONE);
        utils.changeStatusBarColor(this, ContextCompat.getColor(this, R.color.colorPrimaryDark2));
    }

    @Override
    public void hideFragmentShop(Toolbar toolbar, Button button, FrameLayout frameLayout, TextView textView, Fragment fragment, FragmentManager fragmentManager) {
        FRAGMENT_SHOP_IN = true;
        toolbar.setBackgroundColor(resources.getColor(R.color.colorPrimary));
        animation.alphaIn(button);
        button.setBackgroundResource(R.drawable.shop);
        animation.alphaOut(button);
        animation.translateOutAnim(frameLayout, fragment, fragmentManager);
        textView.setVisibility(View.VISIBLE);
        utils.changeStatusBarColor(this, ContextCompat.getColor(this, R.color.colorPrimaryDark));
    }

    @Override
    public void showFragmentFilters(FrameLayout frameLayout) {
        FRAGMENT_FILTERS_IN = false;
        animation.translateInHorAnim(frameLayout);
        utils.changeStatusBarColor(this, ContextCompat.getColor(this, R.color.elevation_background2));
    }

    @Override
    public void hideFragmentFilters(FrameLayout frameLayout, Fragment fragment, FragmentManager fragmentManager) {
        FRAGMENT_FILTERS_IN = true;
        animation.translateOutHorAnim(frameLayout, fragment, fragmentManager);
        utils.changeStatusBarColor(this, ContextCompat.getColor(this, R.color.colorPrimaryDark));
    }

    @Override
    public void showFragmentSubcategories(FrameLayout frameLayout, ConstraintLayout constraintLayout, Toolbar toolbar, ImageView imageView) {
        FRAGMENT_SUBCATEGORIES_IN = false;
        constraintLayout.setBackgroundColor(getResources().getColor(R.color.layout_fragment_products));
        toolbar.setBackgroundColor(getResources().getColor(R.color.layout_fragment_products));
        animation.alphaIn(imageView);
        imageView.setBackgroundResource(R.drawable.arrow_down);
        animation.alphaOut(imageView);
        animation.translateInAnim(frameLayout);
        utils.changeStatusBarColor(this, ContextCompat.getColor(this, R.color.elevation_background2));
    }

    @Override
    public void hideFragmentSubcategories(FrameLayout frameLayout, ConstraintLayout constraintLayout, Toolbar toolbar, ImageView imageView, Fragment fragment, FragmentManager fragmentManager) {
        FRAGMENT_SUBCATEGORIES_IN = true;
        constraintLayout.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
        animation.alphaIn(imageView);
        imageView.setBackgroundResource(R.drawable.arrow_right);
        animation.alphaOut(imageView);
        animation.translateOutAnim(frameLayout, fragment, fragmentManager);
        utils.changeStatusBarColor(this, ContextCompat.getColor(this, R.color.colorPrimaryDark));
    }

    public static boolean isFragmentShopIn() {
        return FRAGMENT_SHOP_IN;
    }

    public static boolean isFragmentSubcategoriesIn() {
        return FRAGMENT_SUBCATEGORIES_IN;
    }

    public static boolean isFragmentFiltersIn() {
        return FRAGMENT_FILTERS_IN;
    }

    public ProductsFragment getProductsFragment() {
        return productsFragment;
    }

    @Override
    public void onClickMenuItemFragmentFilters() {
        productsFragment.getListenerFilters().hideFragmentFilters(mFrameLayoutFilters, filtersFragment, getSupportFragmentManager());
    }

    @Override
    public void onClickShadowFragmentShop() {
        productsFragment.getListenerShop().hideFragmentShop(mToolbar, mBtnShop, mFrameLayoutShop, mTextViewNotification, shopFragment, getSupportFragmentManager());
    }

    @Override
    public void onClickShadowFragmentFilters() {
        productsFragment.getListenerFilters().hideFragmentFilters(mFrameLayoutFilters, filtersFragment, getSupportFragmentManager());
    }

    @Override
    public void onClickShadowFragmentSubcategories() {
        productsFragment.getListenerSubcategories().hideFragmentSubcategories(mFrameLayoutSubcategories, mConstraintLayoutSubcategories, mToolbar, mImageViewArrow, subcategoriesFragment, getSupportFragmentManager());
    }
}

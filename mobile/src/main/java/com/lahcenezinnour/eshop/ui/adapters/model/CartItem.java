package com.lahcenezinnour.eshop.ui.adapters.model;

/**
 * Created by lahcene zinnour on 12/4/16.
 */

public class CartItem {

    private String productName;
    private Float productPrice;
    private String productImage;
    private int productQuantity;
    private String productSize;

    public CartItem() {
    }

    public CartItem(String productName, Float productPrice, String productImage, int productQuantity, String productSize) {
        this.productName = productName;
        this.productPrice = productPrice;
        this.productImage = productImage;
        this.productQuantity = productQuantity;
        this.productSize = productSize;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Float getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(Float productPrice) {
        this.productPrice = productPrice;
    }

    public String getProductImage() {
        return productImage;
    }

    public void setProductImage(String productImage) {
        this.productImage = productImage;
    }

    public int getProductQuantity() {
        return productQuantity;
    }

    public void setProductQuantity(int productQuantity) {
        this.productQuantity = productQuantity;
    }

    public String getProductSize() {
        return productSize;
    }

    public void setProductSize(String productSize) {
        this.productSize = productSize;
    }
}

package com.lahcenezinnour.eshop.ui.fragment;


import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.lahcenezinnour.eshop.ApplicationComponent;
import com.lahcenezinnour.eshop.R;
import com.lahcenezinnour.eshop.base.BaseFragment;
import com.lahcenezinnour.eshop.mvp.model.Productcategories;
import com.lahcenezinnour.eshop.ui.adapters.AdviceAdapter;
import com.lahcenezinnour.eshop.ui.custom.AutofitRecyclerView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class ManFragment extends BaseFragment {

    @BindView(R.id.recycler_view)
    AutofitRecyclerView recyclerView;

    GridLayoutManager mLayoutManager;
    AdviceAdapter mAdapter;
    List<Productcategories> productcategoriesMen;

    public ManFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_man, container, false);
        ButterKnife.bind(this, view);

        Bundle bundle = getArguments();
        productcategoriesMen = (List<Productcategories>) bundle.getSerializable("men");
        recyclerView.setNestedScrollingEnabled(false);
        mAdapter = new AdviceAdapter(productcategoriesMen);
        recyclerView.setAdapter(mAdapter);
        recyclerView.addOnItemTouchListener(
                new AutofitRecyclerView.RecyclerItemClickListener(getActivity(), (view1, i) -> System.out.println(productcategoriesMen.get(i).getCategoryID())));
        return view;
    }

    @Override
    protected void injectDependencies(ApplicationComponent component, Context context) {

    }


}

package com.lahcenezinnour.eshop.ui.interfaces;

/**
 * Created by lahcene zinnour on 2/26/17.
 */

public interface OnMenuItemFragmentFiltersInteractionListener {
    void onClickMenuItemFragmentFilters();
}

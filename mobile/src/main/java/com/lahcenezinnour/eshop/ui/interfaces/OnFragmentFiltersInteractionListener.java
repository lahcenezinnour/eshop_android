package com.lahcenezinnour.eshop.ui.interfaces;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.widget.FrameLayout;

/**
 * Created by lahcene zinnour on 2/23/17.
 */

public interface OnFragmentFiltersInteractionListener {
    void showFragmentFilters(FrameLayout frameLayout);

    void hideFragmentFilters(FrameLayout frameLayout, Fragment fragment, FragmentManager fragmentManager);

}
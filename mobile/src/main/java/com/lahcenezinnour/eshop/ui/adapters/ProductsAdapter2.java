package com.lahcenezinnour.eshop.ui.adapters;

import android.content.Context;
import android.os.Handler;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.lahcenezinnour.eshop.R;
import com.lahcenezinnour.eshop.mvp.model.Products;
import com.lahcenezinnour.eshop.mvp.presenter.interfaces.IMainPresenter;
import com.lahcenezinnour.eshop.mvp.view.IMainView;
import com.lahcenezinnour.eshop.util.CustomVolleyRequest;
import com.lahcenezinnour.eshop.util.EndlessParentScrollListener;
import com.mirhoseini.utils.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lahcene zinnour on 2/11/17.
 */

public class ProductsAdapter2 extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;

    private List<Products> products;
    private IMainView onLoadMoreListener;
    private Context context;
    private GridLayoutManager manager;

    private boolean isMoreLoading = false;
    private int visibleThreshold = 1;
    int firstVisibleItem, visibleItemCount, totalItemCount;
    static int i = 0;
    private NetworkImageView mNetworkImageView;
    private ImageLoader mImageLoader;
    IMainPresenter mainPresenter;

    public ProductsAdapter2(IMainView onLoadMoreListener, IMainPresenter mainPresenter, Context context) {
        this.mainPresenter = mainPresenter;
        this.onLoadMoreListener = onLoadMoreListener;
        this.context = context;
        products = new ArrayList<>();

    }

    public void setLinearLayoutManager(GridLayoutManager manager) {
        this.manager = manager;
    }

    /*public void setRecyclerView(RecyclerView mView){
        this.mLinearLayoutManager= (GridLayoutManager) mView.getLayoutManager();
        mView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                i++;
                visibleItemCount = recyclerView.getChildCount();
                totalItemCount = mLinearLayoutManager.getItemCount();
                firstVisibleItem = mLinearLayoutManager.findFirstVisibleItemPosition();
                System.out.println("visibleItemCount = "+visibleItemCount);
                System.out.println("totalItemCount = "+totalItemCount);
                System.out.println("firstVisibleItem = "+firstVisibleItem);
                if (!isMoreLoading && (totalItemCount - visibleItemCount)<= (firstVisibleItem + visibleThreshold)) {
                    if (onLoadMoreListener != null) {
                        System.out.println("------------- onScrolled");
                        mainPresenter.test(2, 2);
                    }
                    isMoreLoading = true;
                }
            }
        });
    }*/

    public void setScrollView(NestedScrollView scrollView) {
        scrollView.setOnScrollChangeListener(new EndlessParentScrollListener(manager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                if (!isMoreLoading) {
                    System.out.println("------------- onScrolled");
                    if (Utils.isConnected(context))
                        mainPresenter.test(1, 5);
                    else
                        mainPresenter.test(1, 5);
                    isMoreLoading = true;
                }
            }
        });
    }

    public class ProductsHolder extends RecyclerView.ViewHolder {
        public NetworkImageView image;
        public TextView title, price;

        public ProductsHolder(View view) {
            super(view);
            image = (NetworkImageView) view.findViewById(R.id.image_product);
            title = (TextView) view.findViewById(R.id.title_product);
            price = (TextView) view.findViewById(R.id.price_product);
        }
    }

    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            progressBar = (ProgressBar) v.findViewById(R.id.progressBar);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        RecyclerView.ViewHolder vh;
        if (viewType == VIEW_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_product, parent, false);

            vh = new ProductsAdapter2.ProductsHolder(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_progressbar, parent, false);

            vh = new ProductsAdapter2.ProgressViewHolder(v);
        }
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        mImageLoader = CustomVolleyRequest.getInstance(context).getImageLoader();
        if (holder instanceof ProductsAdapter2.ProductsHolder) {

            Products product = products.get(position);
            mImageLoader.get(product.getProductImage(), ImageLoader.getImageListener(((ProductsAdapter2.ProductsHolder) holder).image, android.R.color.white, android.R.drawable.ic_dialog_alert));
            ((ProductsAdapter2.ProductsHolder) holder).image.setImageUrl(product.getProductImage(), mImageLoader);
            ((ProductsAdapter2.ProductsHolder) holder).title.setText(product.getProductName());
            ((ProductsAdapter2.ProductsHolder) holder).price.setText(String.valueOf(product.getProductPrice()));
            //imageDownload(product.getProductImage(), ((ProductsAdapter2.ProductsHolder) holder).image);

        } else {
            ((ProductsAdapter2.ProgressViewHolder) holder).progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    @Override
    public int getItemViewType(int position) {
        return products.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }

    public void addAll(List<Products> lst) {
        products.clear();
        products.addAll(lst);
        notifyDataSetChanged();
    }

    public void addItemMore(List<Products> lst) {
        products.addAll(lst);
        notifyItemRangeChanged(products.size() - lst.size(), products.size());
    }

    public void setMoreLoading(boolean isMoreLoading) {
        this.isMoreLoading = isMoreLoading;
    }

    public void setProgressMore(final boolean isProgress) {
        if (isProgress) {
            new Handler().post(() -> {
                products.add(null);
                notifyItemInserted(products.size() - 1);
            });
        } else {
            products.remove(products.size() - 1);
            notifyItemRemoved(products.size());
        }
    }

}

package com.lahcenezinnour.eshop.ui.fragment;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.lahcenezinnour.eshop.ApplicationComponent;
import com.lahcenezinnour.eshop.R;
import com.lahcenezinnour.eshop.base.BaseFragment;
import com.lahcenezinnour.eshop.ui.activity.AdviceActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class LabelFragment extends BaseFragment {

    @BindView(R.id.button5)
    Button advice;
    @BindView(R.id.button4)
    Button recent;
    @BindView(R.id.button3)
    Button popular;

    public LabelFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_label, container, false);
        ButterKnife.bind(this, view);

        advice.setOnClickListener(View -> {
            Intent adviceIntent = new Intent(getActivity(), AdviceActivity.class);
            startActivity(adviceIntent);
        });

        recent.setOnClickListener(View -> {
            Intent adviceIntent = new Intent(getActivity(), AdviceActivity.class);
            startActivity(adviceIntent);
        });

        popular.setOnClickListener(View -> {
                Intent adviceIntent = new Intent(getActivity(), AdviceActivity.class);
                startActivity(adviceIntent);
        });
        return view;
    }

    @Override
    protected void injectDependencies(ApplicationComponent component, Context context) {

    }


}

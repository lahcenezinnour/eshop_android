package com.lahcenezinnour.eshop.ui.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.lahcenezinnour.eshop.ApplicationComponent;
import com.lahcenezinnour.eshop.R;
import com.lahcenezinnour.eshop.base.BaseFragment;
import com.lahcenezinnour.eshop.mvp.model.Productcategories;
import com.lahcenezinnour.eshop.ui.activity.ProductsActivity;
import com.lahcenezinnour.eshop.ui.adapters.AdviceAdapter;
import com.lahcenezinnour.eshop.ui.custom.AutofitRecyclerView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class WomanFragment extends BaseFragment {


    @BindView(R.id.recycler_view)
    AutofitRecyclerView recyclerView;

    GridLayoutManager mLayoutManager;
    AdviceAdapter mAdapter;
    List<Productcategories> productcategoriesWomen;

    public WomanFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_woman, container, false);
        ButterKnife.bind(this, view);

        Bundle bundle = getArguments();
        productcategoriesWomen = (List<Productcategories>) bundle.getSerializable("women");
        System.out.println("women = "+ productcategoriesWomen);
        recyclerView.setNestedScrollingEnabled(false);
        mAdapter = new AdviceAdapter(productcategoriesWomen);
        recyclerView.setAdapter(mAdapter);
        recyclerView.addOnItemTouchListener(
                new AutofitRecyclerView.RecyclerItemClickListener(getActivity(), (view1, i) -> {
                    System.out.println(productcategoriesWomen.get(i).getCategoryID());
                    Intent intent = new Intent(getActivity(), ProductsActivity.class);
                    intent.putExtra("idCategory", productcategoriesWomen.get(i).getCategoryID());
                    startActivity(intent);
                }));

        return view;
    }

    @Override
    protected void injectDependencies(ApplicationComponent component, Context context) {

    }

}

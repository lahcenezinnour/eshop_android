package com.lahcenezinnour.eshop.ui.adapters.model;

/**
 * Created by lahcene zinnour on 12/4/16.
 */

public class SubCategoryItem {

    int idSubCategory = -1;
    String nameSubCategory = "azerty";

    public SubCategoryItem() {
    }

    public int getIdSubCategory() {
        return idSubCategory;
    }

    public void setIdSubCategory(int idSubCategory) {
        this.idSubCategory = idSubCategory;
    }

    public String getNameSubCategory() {
        return nameSubCategory;
    }

    public void setNameSubCategory(String nameSubCategory) {
        this.nameSubCategory = nameSubCategory;
    }
}

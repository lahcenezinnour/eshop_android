package com.lahcenezinnour.eshop.ui.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.lahcenezinnour.eshop.R;
import com.lahcenezinnour.eshop.ui.adapters.model.SubCategoryItem;

import java.util.List;

/**
 * Created by lahcene zinnour on 2/28/17.
 */

public class SubCategoryAdapter  extends RecyclerView.Adapter<SubCategoryAdapter.SubCategoryHolder> {

    List<SubCategoryItem> subCategoryItems;

    public SubCategoryAdapter(List<SubCategoryItem> subCategoryItems) {
        this.subCategoryItems = subCategoryItems;
    }

    public class SubCategoryHolder extends RecyclerView.ViewHolder {
        public Button button;

        public SubCategoryHolder(View view) {
            super(view);
            button = (Button) view.findViewById(R.id.button_subcategory);
        }
    }

    @Override
    public SubCategoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new SubCategoryHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_subcategory, parent, false));
    }

    @Override
    public void onBindViewHolder(SubCategoryHolder holder, int position) {
        SubCategoryItem subCategoryItem = subCategoryItems.get(position);
        holder.button.setText(subCategoryItem.getNameSubCategory());
    }

    @Override
    public int getItemCount() {
        return subCategoryItems.size();
    }
}

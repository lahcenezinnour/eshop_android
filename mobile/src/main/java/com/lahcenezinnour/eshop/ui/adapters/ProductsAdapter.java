package com.lahcenezinnour.eshop.ui.adapters;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.lahcenezinnour.eshop.R;
import com.lahcenezinnour.eshop.mvp.model.Products;
import com.lahcenezinnour.eshop.ui.interfaces.OnLoadMoreListener;

import java.util.List;

/**
 * Created by lahcene zinnour on 1/28/17.
 */

public class ProductsAdapter extends RecyclerView.Adapter{
    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;
    static int i = 0;

    private List<Products> products;
    private RecyclerView recyclerView;
    private int visibleThreshold = 2;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;
    private OnLoadMoreListener onLoadMoreListener;

    public class ProductsHolder extends RecyclerView.ViewHolder {
        public TextView title, price;
        public ProductsHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title_product);
            price = (TextView) view.findViewById(R.id.price_product);
        }
    }

    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            progressBar = (ProgressBar) v.findViewById(R.id.progressBar);
        }
    }

    public ProductsAdapter() {

    }


    public ProductsAdapter(List<Products> products, RecyclerView recyclerView) {
        this.products = products;
        this.recyclerView = recyclerView;
        i++;
        System.out.println("constructor i= "+i);

        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
            System.out.println("True i= "+i);
            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView
                    .getLayoutManager();

            recyclerView
                    .addOnScrollListener(new RecyclerView.OnScrollListener() {
                        @Override
                        public void onScrolled(RecyclerView recyclerView,
                                               int dx, int dy) {
                            super.onScrolled(recyclerView, dx, dy);

                            totalItemCount = linearLayoutManager.getItemCount();
                            lastVisibleItem = linearLayoutManager
                                    .findLastVisibleItemPosition();
                            if (!loading
                                    && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                                // End has been reached
                                // Do something
                                if (onLoadMoreListener != null) {
                                    onLoadMoreListener.onLoadMore();
                                }
                                loading = true;
                            }
                        }
                    });
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        RecyclerView.ViewHolder vh;
        if (viewType == VIEW_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.layout_product, parent, false);

            vh = new ProductsHolder(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.layout_progressbar, parent, false);

            vh = new ProgressViewHolder(v);
        }
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof ProductsHolder) {
            Products product = products.get(position);
            ((ProductsHolder) holder).title.setText(product.getProductName());
            ((ProductsHolder) holder).price.setText(String.valueOf(product.getProductPrice()));
        } else {
            ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    @Override
    public int getItemViewType(int position) {
        return products.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }

    public void setLoaded() {
        loading = false;
    }


}

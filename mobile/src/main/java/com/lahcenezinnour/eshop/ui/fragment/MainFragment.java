package com.lahcenezinnour.eshop.ui.fragment;


import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.GridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.lahcenezinnour.eshop.ApplicationComponent;
import com.lahcenezinnour.eshop.R;
import com.lahcenezinnour.eshop.base.BaseFragment;
import com.lahcenezinnour.eshop.injector.module.MainModule;
import com.lahcenezinnour.eshop.mvp.model.Products;
import com.lahcenezinnour.eshop.mvp.presenter.interfaces.IMainPresenter;
import com.lahcenezinnour.eshop.mvp.view.IMainView;
import com.lahcenezinnour.eshop.ui.adapters.ProductsAdapter2;
import com.lahcenezinnour.eshop.ui.custom.AutofitRecyclerView;
import com.lahcenezinnour.eshop.ui.interfaces.OnFragmentLabelInteractionListener;
import com.lahcenezinnour.eshop.ui.interfaces.OnFragmentShopInteractionListener;
import com.lahcenezinnour.eshop.util.Animations;
import com.mirhoseini.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import butterknife.BindView;
import butterknife.ButterKnife;
//AppCompatActivity activity = (AppCompatActivity) getActivity();
//activity.setSupportActionBar(mToolbar);
// activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);

/**
 * A simple {@link Fragment} subclass.
 */
public class MainFragment extends BaseFragment implements IMainView{

    @Inject
    Context context;
    @Inject
    Resources resources;
    @Inject
    Animations animation;
    @Inject
    IMainPresenter mainPresenter;
    @Inject
    @Named("main")
    OnFragmentShopInteractionListener listenerShop;
    @Inject
    @Named("main")
    OnFragmentLabelInteractionListener listenerLabel;
    @BindView(R.id.recycler_view)
    AutofitRecyclerView recyclerView;
    @BindView(R.id.scrollView)
    NestedScrollView scrollView;

    GridLayoutManager mLayoutManager;
    ProductsAdapter2 mAdapter;
    List<Products> products = new ArrayList<>();
    static int i = 0;

    public MainFragment() {
    }

    public static MainFragment newInstance() {
        MainFragment fragment = new MainFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        System.out.println("******************   onCreateView1");
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        ButterKnife.bind(this, view);



        System.out.println("******************   onCreateView2");
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recyclerView.setNestedScrollingEnabled(false);
        mAdapter = new ProductsAdapter2(this, mainPresenter, getContext());

        mLayoutManager = (GridLayoutManager) recyclerView.getLayoutManager();
        mLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                if (mAdapter.getItemViewType(position) == 1){
                    return 1;
                }else
                    return recyclerView.getSpanCount();
            }
        });
        mAdapter.setLinearLayoutManager(mLayoutManager);
        mAdapter.setScrollView(scrollView);
        recyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d("MainActivity_","onStart");
        if (Utils.isConnected(context))
            mainPresenter.getProductByRangeFromApi(1, 5);
        else
            mainPresenter.getProductByRangeFromDb(1, 5);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listenerShop = null;
        listenerLabel = null;

        mainPresenter.unbind();
        mainPresenter = null;
    }

    @Override
    public void showMessage(String message) {
        System.out.println("******************   showMessage");
    }

    @Override
    public void showOfflineMessage() {

    }

    @Override
    public void showProductByRange(List<Products> products) {
        System.out.println("******************   showProductByRange1");
        for (int i = 0; i < products.size(); i++) {
            System.out.println("i= " + i);
            System.out.println(" ->" + products.get(i).getProductName());
        }
        this.products.clear();
        this.products = new ArrayList<>(products);
        mAdapter.addAll(products);

        System.out.println("******************   showProductByRange2");
    }

    @Override
    public void saveProductByRangeInDB(List<Products> character) {

    }

    @Override
    public void showProgress() {
        System.out.println("******************  showProgress");

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void showRetryMessage(Throwable throwable) {

    }

    @Override
    public void showNetworkError(Throwable throwable) {

    }

    @Override
    public void showServerError(Throwable throwable) {

    }

    @Override
    protected void injectDependencies(ApplicationComponent component, Context context) {
        component.plus_MainFragment(new MainModule(this, context)).injectMainFragment(this);
    }

    @Override
    public void onLoadMore(List<Products> productsList) {
        Log.d("MainActivity_","_onLoadMore"+productsList.size());

        mAdapter.setProgressMore(true);
        new Handler().postDelayed(() -> {
            products.clear();
            mAdapter.setProgressMore(false);
            int start = mAdapter.getItemCount();
            int end = start + 2;
            int j = 0;
            System.out.println("start = "+start);
            System.out.println("end = "+end);
            for (int i1 = start + 1; i1 <= end; i1++) {
                products.add(productsList.get(j));
                j++;
            }
            mAdapter.addItemMore(products);
            mAdapter.setMoreLoading(false);
        },2000);
    }

    @Override
    public void networkError() {
        System.out.println("----------------   networkError");
    }

    public OnFragmentShopInteractionListener getListenerShop() {
        return listenerShop;
    }

    public OnFragmentLabelInteractionListener getListenerLabel() {
        return listenerLabel;
    }
}

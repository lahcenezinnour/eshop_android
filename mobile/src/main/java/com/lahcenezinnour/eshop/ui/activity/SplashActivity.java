package com.lahcenezinnour.eshop.ui.activity;

import android.os.Bundle;

import com.lahcenezinnour.eshop.ApplicationComponent;
import com.lahcenezinnour.eshop.EshopApplication;
import com.lahcenezinnour.eshop.R;
import com.lahcenezinnour.eshop.base.BaseActivity;


public class SplashActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
    }

    @Override
    protected void injectDependencies(ApplicationComponent component) {

    }

    @Override
    protected void releaseSubComponents(EshopApplication application) {

    }
}

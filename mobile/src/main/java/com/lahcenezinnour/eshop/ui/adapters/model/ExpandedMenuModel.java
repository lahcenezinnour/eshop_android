package com.lahcenezinnour.eshop.ui.adapters.model;

/**
 * Created by lahcene zinnour on 7/18/2016.
 */

public class ExpandedMenuModel {
    int icon = -1;
    String title = "azerty";
    int indicator = -1;

    public ExpandedMenuModel() {
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getIndicator() {
        return indicator;
    }

    public void setIndicator(int indicator) {
        this.indicator = indicator;
    }
}

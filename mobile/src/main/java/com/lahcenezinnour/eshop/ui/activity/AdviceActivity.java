package com.lahcenezinnour.eshop.ui.activity;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.jakewharton.rxbinding.view.RxView;
import com.lahcenezinnour.eshop.ApplicationComponent;
import com.lahcenezinnour.eshop.EshopApplication;
import com.lahcenezinnour.eshop.R;
import com.lahcenezinnour.eshop.base.BaseActivity;
import com.lahcenezinnour.eshop.ui.fragment.AdviceFragment;
import com.lahcenezinnour.eshop.ui.fragment.ShopFragment;
import com.lahcenezinnour.eshop.ui.interfaces.OnFragmentShopInteractionListener;
import com.lahcenezinnour.eshop.ui.interfaces.OnShadowFragmentShopInteractionListener;
import com.lahcenezinnour.eshop.util.Animations;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AdviceActivity extends BaseActivity implements OnFragmentShopInteractionListener, OnShadowFragmentShopInteractionListener {

    public static final String ADVICE_FRAGMENT = "ADVICE_FRAGMENT";
    public static final String SHOP_FRAGMENT = "SHOP_FRAGMENT";

    static boolean FRAGMENT_SHOP_IN = true;

    protected AdviceFragment adviceFragment;
    protected ShopFragment shopFragment;

    @Inject
    Resources resources;
    @Inject
    Animations animation;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.badge_layout)
    ConstraintLayout mConstraintLayoutShop;
    @BindView(R.id.fragment_shop)
    FrameLayout mFrameLayoutShop;
    @BindView(R.id.buttonBadge)
    Button mBtnShop;
    @BindView(R.id.badge_notification)
    TextView mTextViewnotification;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_advice);
        ButterKnife.bind(this);

        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("Our Advice");
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        adviceFragment = (AdviceFragment) getSupportFragmentManager().findFragmentByTag(ADVICE_FRAGMENT);
        shopFragment = (ShopFragment) getSupportFragmentManager().findFragmentByTag(SHOP_FRAGMENT);

        if (adviceFragment == null) {
            adviceFragment = new AdviceFragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_advice, adviceFragment, ADVICE_FRAGMENT).commit();
        }
        if (shopFragment == null) {
            shopFragment = new ShopFragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_shop, shopFragment, SHOP_FRAGMENT).commit();
        }

        animation.translateOutAnim2(mFrameLayoutShop, null, null);

        RxView.clicks(mConstraintLayoutShop)
                .subscribe(aVoid ->
                        {
                            if (FRAGMENT_SHOP_IN) {
                                getSupportFragmentManager().beginTransaction().show(shopFragment).commit();
                                adviceFragment.getListenerShop().showFragmentShop(mToolbar, mBtnShop, mFrameLayoutShop, mTextViewnotification);

                            } else {

                                adviceFragment.getListenerShop().hideFragmentShop(mToolbar, mBtnShop, mFrameLayoutShop, mTextViewnotification, shopFragment, getSupportFragmentManager());
                            }
                        }
                );

    }

    @Override
    protected void injectDependencies(ApplicationComponent component) {
        component.injectAdviceActivity(this);
    }

    @Override
    protected void releaseSubComponents(EshopApplication application) {

    }

    @Override
    public void showFragmentShop(Toolbar toolbar, Button button, FrameLayout frameLayout, TextView textView) {
        FRAGMENT_SHOP_IN = false;
        toolbar.setBackgroundColor(resources.getColor(R.color.colorPrimary2));
        animation.alphaIn(button);
        button.setBackgroundResource(R.drawable.close);
        animation.alphaOut(button);
        animation.translateInAnim(frameLayout);
        textView.setVisibility(View.GONE);
    }

    @Override
    public void hideFragmentShop(Toolbar toolbar, Button button, FrameLayout frameLayout, TextView textView, Fragment fragment, FragmentManager fragmentManager) {
        FRAGMENT_SHOP_IN = true;
        toolbar.setBackgroundColor(resources.getColor(R.color.colorPrimary));
        animation.alphaIn(button);
        button.setBackgroundResource(R.drawable.shop);
        animation.alphaOut(button);
        animation.translateOutAnim(frameLayout, fragment, fragmentManager);
        textView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onClickShadowFragmentShop() {
        adviceFragment.getListenerShop().hideFragmentShop(mToolbar, mBtnShop, mFrameLayoutShop, mTextViewnotification, shopFragment, getSupportFragmentManager());
    }

}

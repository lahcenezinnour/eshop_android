package com.lahcenezinnour.eshop.ui.interfaces;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.widget.FrameLayout;

/**
 * Created by lahcene zinnour on 2/21/17.
 */

public interface OnFragmentLabelInteractionListener {

    void showFragmentLabel(FrameLayout frameLayout);

    void hideFragmentLabel(FrameLayout frameLayout, Fragment fragment, FragmentManager fragmentManager);

}

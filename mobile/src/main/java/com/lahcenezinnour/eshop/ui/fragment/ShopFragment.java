package com.lahcenezinnour.eshop.ui.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.lahcenezinnour.eshop.ApplicationComponent;
import com.lahcenezinnour.eshop.R;
import com.lahcenezinnour.eshop.base.BaseFragment;
import com.lahcenezinnour.eshop.injector.module.ShopModule;
import com.lahcenezinnour.eshop.mvp.view.IShopView;
import com.lahcenezinnour.eshop.ui.interfaces.OnShadowFragmentShopInteractionListener;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class ShopFragment extends BaseFragment implements IShopView {


    @Inject
    OnShadowFragmentShopInteractionListener listenerShadowShop;

    @BindView(R.id.shadow)
    View viewShadow;

    public ShopFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_shop, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @OnClick(R.id.shadow)
    public void clickShadow(View view) {
        listenerShadowShop.onClickShadowFragmentShop();
    }

    @Override
    protected void injectDependencies(ApplicationComponent component, Context context) {
        component.plus_ShopFragment(new ShopModule(this, context)).injectShopFragment(this);
    }


    @Override
    public void showMessage(String message) {

    }

    @Override
    public void showOfflineMessage() {

    }

    @Override
    public void showRetryMessage(Throwable throwable) {

    }

    @Override
    public void showNetworkError(Throwable throwable) {

    }

    @Override
    public void showServerError(Throwable throwable) {

    }

    @Override
    public void networkError() {

    }
}

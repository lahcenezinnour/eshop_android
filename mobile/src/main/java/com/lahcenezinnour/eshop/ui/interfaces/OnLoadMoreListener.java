package com.lahcenezinnour.eshop.ui.interfaces;

/**
 * Created by lahcene zinnour on 1/29/17.
 */

public interface OnLoadMoreListener {
    void onLoadMore();
}

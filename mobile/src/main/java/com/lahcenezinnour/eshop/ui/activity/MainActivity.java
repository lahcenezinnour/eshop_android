package com.lahcenezinnour.eshop.ui.activity;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.support.v7.widget.Toolbar;

import com.jakewharton.rxbinding.view.RxView;
import com.lahcenezinnour.eshop.ApplicationComponent;
import com.lahcenezinnour.eshop.EshopApplication;
import com.lahcenezinnour.eshop.R;
import com.lahcenezinnour.eshop.base.BaseActivity;
import com.lahcenezinnour.eshop.ui.adapters.ExpandableListAdapter;
import com.lahcenezinnour.eshop.ui.adapters.model.ExpandedMenuModel;
import com.lahcenezinnour.eshop.ui.fragment.LabelFragment;
import com.lahcenezinnour.eshop.ui.fragment.MainFragment;
import com.lahcenezinnour.eshop.ui.fragment.ShopFragment;
import com.lahcenezinnour.eshop.ui.interfaces.OnFragmentLabelInteractionListener;
import com.lahcenezinnour.eshop.ui.interfaces.OnFragmentShopInteractionListener;
import com.lahcenezinnour.eshop.ui.interfaces.OnShadowFragmentShopInteractionListener;
import com.lahcenezinnour.eshop.util.Animations;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Subscription;

public class MainActivity extends BaseActivity implements OnFragmentShopInteractionListener, OnFragmentLabelInteractionListener , OnShadowFragmentShopInteractionListener {

    public static final String MAIN_FRAGMENT = "MAIN_FRAGMENT";
    public static final String SHOP_FRAGMENT = "SHOP_FRAGMENT";
    public static final String LABEL_FRAGMENT = "LABEL_FRAGMENT";

    static boolean FRAGMENT_SHOP_IN = true;
    static boolean FRAGMENT_LABEL_IN = true;

    @Inject
    Resources resources;
    @Inject
    Animations animation;

    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.nav_view)
    NavigationView mNavigationView;
    @BindView(R.id.expandableListView)
    ExpandableListView mExpandableListView;

    protected MainFragment mainFragment;
    protected ShopFragment shopFragment;
    protected LabelFragment labelFragment;

    protected FrameLayout mFrameLayoutMain, mFrameLayoutShop, mFrameLayoutLabel;
    TextView mTextViewnotification;
    Button mBtnShop;
    ConstraintLayout mConstraintLayoutCount;
    RelativeLayout mRelativeLayoutShop, mRelativeLayoutLabel;
    Subscription mSubscriptionShop, mSubscriptionSLabel;
    Menu menu;
    ExpandableListAdapter mMenuAdapter;
    List<ExpandedMenuModel> listDataHeader;
    HashMap<ExpandedMenuModel, List<String>> listDataChild;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        prepareListData();
        mMenuAdapter = new ExpandableListAdapter(this, listDataHeader, listDataChild);
        mExpandableListView.setAdapter(mMenuAdapter);
        mExpandableListView.setOnChildClickListener((expandableListView, view, i, i1, l) -> {
            Log.d("DEBUG", "submenu item clicked" + i);

            return false;
        });
        mExpandableListView.setOnGroupClickListener((expandableListView, view, i, l) -> {
            Log.d("DEBUG", "heading clicked" + i);


            return false;
        });

        mExpandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            int previousItem = -1;

            @Override
            public void onGroupExpand(int groupPosition) {
                if (groupPosition != previousItem)
                    mExpandableListView.collapseGroup(previousItem);
                previousItem = groupPosition;
            }
        });
        if (mNavigationView != null) {
            setupDrawerContent(mNavigationView, drawer);
        }

        mainFragment = (MainFragment) getSupportFragmentManager().findFragmentByTag(MAIN_FRAGMENT);
        shopFragment = (ShopFragment) getSupportFragmentManager().findFragmentByTag(SHOP_FRAGMENT);
        labelFragment = (LabelFragment) getSupportFragmentManager().findFragmentByTag(LABEL_FRAGMENT);
        if (mainFragment == null) {
            mainFragment = MainFragment.newInstance();
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_main, mainFragment, MAIN_FRAGMENT).commit();
        }
        if (shopFragment == null) {
            shopFragment = new ShopFragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_shop, shopFragment, SHOP_FRAGMENT).commit();
        }
        if (labelFragment == null) {
            labelFragment = new LabelFragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_label, labelFragment, LABEL_FRAGMENT).commit();
        }

        menu = mToolbar.getMenu();
        getMenuInflater().inflate(R.menu.main, menu);
        MenuItem item1 = menu.findItem(R.id.menu);
        MenuItemCompat.setActionView(item1, R.layout.notification_update_count_layout);

        mConstraintLayoutCount = (ConstraintLayout) MenuItemCompat.getActionView(item1);

        mTextViewnotification = (TextView) mConstraintLayoutCount.findViewById(R.id.badge_notification_1);

        mRelativeLayoutShop = (RelativeLayout) mConstraintLayoutCount.findViewById(R.id.relative_layout_item_count1);
        mRelativeLayoutLabel = (RelativeLayout) mConstraintLayoutCount.findViewById(R.id.relative_layout_item_count2);

        mBtnShop = (Button) mConstraintLayoutCount.findViewById(R.id.button1);

        mFrameLayoutMain = (FrameLayout) findViewById(R.id.fragment_main);
        mFrameLayoutShop = (FrameLayout) findViewById(R.id.fragment_shop);
        mFrameLayoutLabel = (FrameLayout) findViewById(R.id.fragment_label);

        animation.translateOutAnim2(mFrameLayoutShop, null, null);
        animation.translateOutAnim2(mFrameLayoutLabel, null, null);

        //mFrameLayoutShop.setVisibility(View.GONE);
        //mFrameLayoutLabel.setVisibility(View.GONE);


        RxView.clicks(mRelativeLayoutShop)
                .subscribe(aVoid ->
                        {
                            System.out.println("FRAGMENT_SHOP_IN 1= "+FRAGMENT_SHOP_IN);
                            if (FRAGMENT_SHOP_IN) {
                                if (!FRAGMENT_LABEL_IN) {
                                    mainFragment.getListenerLabel().hideFragmentLabel(mFrameLayoutLabel, labelFragment, getSupportFragmentManager());
                                }
                                getSupportFragmentManager().beginTransaction().show(shopFragment).commit();
                                mainFragment.getListenerShop().showFragmentShop(mToolbar, mBtnShop, mFrameLayoutShop, mTextViewnotification);

                            } else {

                                mainFragment.getListenerShop().hideFragmentShop(mToolbar, mBtnShop, mFrameLayoutShop, mTextViewnotification, shopFragment, getSupportFragmentManager());
                            }
                            System.out.println("FRAGMENT_SHOP_IN 2= "+FRAGMENT_SHOP_IN);
                        }
                );

        RxView.clicks(mRelativeLayoutLabel)
                .subscribe(aVoid ->
                        {
                            if (FRAGMENT_LABEL_IN) {
                                if (!FRAGMENT_SHOP_IN) {
                                    mainFragment.getListenerShop().hideFragmentShop(mToolbar, mBtnShop, mFrameLayoutShop, mTextViewnotification, shopFragment, getSupportFragmentManager());
                                }
                                getSupportFragmentManager().beginTransaction().show(labelFragment).commit();
                                mainFragment.getListenerLabel().showFragmentLabel(mFrameLayoutLabel);
                            } else {

                                mainFragment.getListenerLabel().hideFragmentLabel(mFrameLayoutLabel, labelFragment, getSupportFragmentManager());
                            }
                        }
                );

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void injectDependencies(ApplicationComponent component) {
        component.injectMainActivity(this);
    }

    @Override
    protected void releaseSubComponents(EshopApplication application) {
        application.releaseAdviceSubComponent();
        application.releaseFiltersSubComponent();
        application.releaseMainSubComponent();
        application.releaseProductsSubComponent();
        application.releaseShopSubComponent();
        application.releaseSubcategoriesSubComponent();
    }

    private void setupDrawerContent(NavigationView navigationView, DrawerLayout mDrawerLayout) {
        //revision: this don't works, use setOnChildClickListener() and setOnGroupClickListener() above instead
        navigationView.setNavigationItemSelectedListener(
                menuItem -> {
                    menuItem.setChecked(true);
                    mDrawerLayout.closeDrawers();
                    return true;
                });

    }

    private void prepareListData() {
        listDataHeader = new ArrayList<>();
        listDataChild = new HashMap<>();

        ExpandedMenuModel item1 = new ExpandedMenuModel();
        item1.setIcon(R.drawable.clothings);
        item1.setTitle("CLOTHINGS");
        item1.setIndicator(R.drawable.arrow_down);
        // Adding data header
        listDataHeader.add(item1);

        ExpandedMenuModel item2 = new ExpandedMenuModel();
        item2.setIcon(R.drawable.shoes);
        item2.setTitle("SHOES");
        item2.setIndicator(R.drawable.arrow_down);
        listDataHeader.add(item2);

        ExpandedMenuModel item3 = new ExpandedMenuModel();
        item3.setIcon(R.drawable.sports);
        item3.setTitle("SPORTS");
        item3.setIndicator(R.drawable.arrow_down);
        listDataHeader.add(item3);

        ExpandedMenuModel item4 = new ExpandedMenuModel();
        item4.setIcon(R.drawable.bags);
        item4.setTitle("BAGS & ACCESSORY");
        item4.setIndicator(R.drawable.arrow_down);
        listDataHeader.add(item4);

        ExpandedMenuModel item5 = new ExpandedMenuModel();
        item5.setIcon(R.drawable.account);
        item5.setTitle("ACCOUNT");
        item5.setIndicator(R.drawable.arrow_down);
        listDataHeader.add(item5);

        ExpandedMenuModel item6 = new ExpandedMenuModel();
        item6.setIcon(R.drawable.settings);
        item6.setTitle("SETTINGS");
        item6.setIndicator(R.drawable.arrow_down);
        listDataHeader.add(item6);

        // Adding child data
        List<String> heading1 = new ArrayList<>();
        heading1.add("New");

        List<String> heading2 = new ArrayList<>();
        heading2.add("Sneakers");
        heading2.add("Sandals");
        heading2.add("Boots");
        heading2.add("Slippers");

        listDataChild.put(listDataHeader.get(0), heading1);// Header, Child data
        listDataChild.put(listDataHeader.get(1), heading2);

    }

    @Override
    public void showFragmentLabel(FrameLayout frameLayout) {
        FRAGMENT_LABEL_IN = false;
        animation.translateInAnim(frameLayout);
    }

    @Override
    public void hideFragmentLabel(FrameLayout frameLayout, Fragment fragment, FragmentManager fragmentManager) {
        FRAGMENT_LABEL_IN = true;
        animation.translateOutAnim(frameLayout, fragment, fragmentManager);
    }

    @Override
    public void showFragmentShop(Toolbar toolbar, Button button, FrameLayout frameLayout, TextView textView) {
        FRAGMENT_SHOP_IN = false;
        toolbar.setBackgroundColor(resources.getColor(R.color.colorPrimary2));
        animation.alphaIn(button);
        button.setBackgroundResource(R.drawable.close);
        animation.alphaOut(button);
        animation.translateInAnim(frameLayout);
        textView.setVisibility(View.GONE);
    }

    @Override
    public void hideFragmentShop(Toolbar toolbar, Button button, FrameLayout frameLayout, TextView textView, Fragment fragment, FragmentManager fragmentManager) {
        FRAGMENT_SHOP_IN = true;
        toolbar.setBackgroundColor(resources.getColor(R.color.colorPrimary));
        animation.alphaIn(button);
        button.setBackgroundResource(R.drawable.shop);
        animation.alphaOut(button);
        animation.translateOutAnim(frameLayout, fragment, fragmentManager);
        textView.setVisibility(View.VISIBLE);
    }

    public static void setFragmentShopIn(boolean fragmentShopIn) {
        FRAGMENT_SHOP_IN = fragmentShopIn;
    }

    public static void setFragmentLabelIn(boolean fragmentLabelIn) {
        FRAGMENT_LABEL_IN = fragmentLabelIn;
    }

    public static boolean isFragmentShopIn() {
        return FRAGMENT_SHOP_IN;
    }

    public static boolean isFragmentLabelIn() {
        return FRAGMENT_LABEL_IN;
    }

    @Override
    public void onClickShadowFragmentShop() {
        mainFragment.getListenerShop().hideFragmentShop(mToolbar, mBtnShop, mFrameLayoutShop, mTextViewnotification, shopFragment, getSupportFragmentManager());
    }
}

package com.lahcenezinnour.eshop.ui.interfaces;

/**
 * Created by lahcene zinnour on 3/3/17.
 */

public interface OnShadowFragmentSubcategoriesInteractionListener {
    void onClickShadowFragmentSubcategories();
}

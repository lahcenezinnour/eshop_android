package com.lahcenezinnour.eshop.ui.interfaces;

import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.Toolbar;
import android.widget.FrameLayout;
import android.widget.ImageView;

/**
 * Created by lahcene zinnour on 2/23/17.
 */

public interface OnFragmentSubcategoriesInteractionListener {

    void showFragmentSubcategories(FrameLayout frameLayout, ConstraintLayout constraintLayout, Toolbar toolbar, ImageView imageView);

    void hideFragmentSubcategories(FrameLayout frameLayout, ConstraintLayout constraintLayout, Toolbar toolbar, ImageView imageView, Fragment fragment, FragmentManager fragmentManager);

}

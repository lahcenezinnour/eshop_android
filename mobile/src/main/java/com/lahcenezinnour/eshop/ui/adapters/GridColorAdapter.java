package com.lahcenezinnour.eshop.ui.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;

import com.lahcenezinnour.eshop.R;
import com.lahcenezinnour.eshop.util.CustomDrawable;

/**
 * Created by lahcene zinnour on 11/22/16.
 */

public class GridColorAdapter extends BaseAdapter {
    private Context context;
    private int[] colors;
    CustomDrawable customDrawable = new CustomDrawable();

    public GridColorAdapter(Context context, int[] colors){
        this.context = context;
        this.colors = colors;

    }

    @Override
    public int getCount() {
        return colors.length;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View colorView;

        if (view == null) {
            colorView = new View(context);
            colorView = inflater.inflate(R.layout.product_details_color, null);
            RelativeLayout layoutColor = (RelativeLayout) colorView
                    .findViewById(R.id.layoutColor);
            layoutColor.setBackground(customDrawable.setCornerView(8, ContextCompat.getColor(this.context, colors[i])));
        } else {
            colorView = view;
        }

        return colorView;
    }
}

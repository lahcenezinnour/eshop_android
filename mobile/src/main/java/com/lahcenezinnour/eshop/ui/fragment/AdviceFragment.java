package com.lahcenezinnour.eshop.ui.fragment;


import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.lahcenezinnour.eshop.ApplicationComponent;
import com.lahcenezinnour.eshop.R;
import com.lahcenezinnour.eshop.base.BaseFragment;
import com.lahcenezinnour.eshop.injector.module.AdviceModule;
import com.lahcenezinnour.eshop.mvp.model.Productcategories;
import com.lahcenezinnour.eshop.mvp.presenter.interfaces.IAdvicePresenter;
import com.lahcenezinnour.eshop.mvp.view.IAdviceView;
import com.lahcenezinnour.eshop.ui.adapters.ViewPagerAdapter;
import com.lahcenezinnour.eshop.ui.interfaces.OnFragmentShopInteractionListener;
import com.lahcenezinnour.eshop.util.Animations;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * A simple {@link Fragment} subclass.
 */
public class AdviceFragment extends BaseFragment implements IAdviceView {


    @Inject
    Context context;
    @Inject
    Resources resources;
    @Inject
    Animations animation;
    @Inject
    IAdvicePresenter advicePresenter;
    @Inject
    @Named("Advice")
    OnFragmentShopInteractionListener listenerShop;

    @BindView(R.id.viewpager)
    ViewPager viewPager;
    @BindView(R.id.tablayout)
    TabLayout tabLayout;
    AllFragment allFragment;
    ManFragment manFragment;
    WomanFragment womanFragment;
    KidFragment kidFragment;

    public AdviceFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_advice, container, false);
        ButterKnife.bind(this, view);


        tabLayout.setupWithViewPager(viewPager);

        advicePresenter.getAllCategoriesFromDb();

        setupViewPager(viewPager, allFragment, manFragment, womanFragment, kidFragment);
        return view;
    }

    @Override
    protected void injectDependencies(ApplicationComponent component, Context context) {
        component.plus_AdviceFragment(new AdviceModule(this, context)).injectAdviceFragment(this);
    }

    public void setupViewPager(ViewPager viewPager, AllFragment allFragment, ManFragment manFragment, WomanFragment womanFragment, KidFragment kidFragment){
        ViewPagerAdapter adapter = new ViewPagerAdapter(getActivity().getSupportFragmentManager());
        adapter.addFragment(allFragment, "ALL");
        adapter.addFragment(manFragment, "MAN");
        adapter.addFragment(womanFragment, "WOMAN");
        adapter.addFragment(kidFragment, "KID");
        viewPager.setAdapter(adapter);
    }

    @Override
    public void showCategories(List<Productcategories> all, List<Productcategories> men, List<Productcategories> women, List<Productcategories> kids) {

        System.out.println("+++++  all = "+ all);
        System.out.println("+++++  men = "+ men);
        System.out.println("+++++  women = "+ women);
        System.out.println("+++++  kids = "+ kids);

        Bundle bundleAllFragment = new Bundle();
        Bundle bundleManFragment = new Bundle();
        Bundle bundleWomanFragment = new Bundle();
        Bundle bundleKidFragment = new Bundle();

        bundleAllFragment.putSerializable("all", (Serializable) all);
        allFragment = new AllFragment();
        allFragment.setArguments(bundleAllFragment);

        bundleManFragment.putSerializable("men", (Serializable) men);
        manFragment = new ManFragment();
        manFragment.setArguments(bundleManFragment);

        bundleWomanFragment.putSerializable("women", (Serializable) women);
        womanFragment = new WomanFragment();
        womanFragment.setArguments(bundleWomanFragment);

        bundleKidFragment.putSerializable("kids", (Serializable) kids);
        kidFragment = new KidFragment();
        kidFragment.setArguments(bundleKidFragment);


    }

    @Override
    public void showMessage(String message) {

    }

    @Override
    public void showOfflineMessage() {

    }

    @Override
    public void showRetryMessage(Throwable throwable) {

    }

    @Override
    public void showNetworkError(Throwable throwable) {

    }

    @Override
    public void showServerError(Throwable throwable) {

    }

    @Override
    public void networkError() {

    }

    @Override
    public void onDetach() {
        super.onDetach();
        listenerShop = null;

        advicePresenter.unbind();
        advicePresenter = null;
    }

    public OnFragmentShopInteractionListener getListenerShop() {
        return listenerShop;
    }
}

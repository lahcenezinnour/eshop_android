package com.lahcenezinnour.eshop;

import android.app.Application;
import android.content.Context;

import com.lahcenezinnour.eshop.injector.component.AdviceSubComponent;
import com.lahcenezinnour.eshop.injector.component.FiltersSubComponent;
import com.lahcenezinnour.eshop.injector.component.MainSubComponent;
import com.lahcenezinnour.eshop.injector.component.ProductsSubComponent;
import com.lahcenezinnour.eshop.injector.component.ShopSubComponent;
import com.lahcenezinnour.eshop.injector.component.SubcategoriesSubComponent;
import com.lahcenezinnour.eshop.injector.module.AdviceModule;
import com.lahcenezinnour.eshop.injector.module.FiltersModule;
import com.lahcenezinnour.eshop.injector.module.MainModule;
import com.lahcenezinnour.eshop.injector.module.ProductsModule;
import com.lahcenezinnour.eshop.injector.module.ShopModule;
import com.lahcenezinnour.eshop.injector.module.SubcategoriesModule;
import com.lahcenezinnour.eshop.ui.fragment.AdviceFragment;
import com.lahcenezinnour.eshop.ui.fragment.FiltersFragment;
import com.lahcenezinnour.eshop.ui.fragment.MainFragment;
import com.lahcenezinnour.eshop.ui.fragment.ProductsFragment;
import com.lahcenezinnour.eshop.ui.fragment.ShopFragment;
import com.lahcenezinnour.eshop.ui.fragment.SubcategoriesFragment;


public class EshopApplication extends Application {

    private static ApplicationComponent component;
    private MainSubComponent mainSubComponent;
    private AdviceSubComponent adviceSubComponent;
    private ProductsSubComponent productsSubComponent;
    private FiltersSubComponent filtersSubComponent;
    private ShopSubComponent shopSubComponent;
    private SubcategoriesSubComponent subcategoriesSubComponent;

    public static ApplicationComponent getComponent() {
        return component;
    }

    public static EshopApplication get(Context context) {
        return (EshopApplication) context.getApplicationContext();
    }

    public MainSubComponent getMainSubComponent(MainFragment fragment, Context context) {
        if (null == mainSubComponent)
            createMainSubComponent(fragment, context);

        return mainSubComponent;
    }

    public MainSubComponent createMainSubComponent(MainFragment fragment, Context context) {
        mainSubComponent = component.plus_MainFragment(new MainModule(fragment, context));
        return mainSubComponent;
    }

    public void releaseMainSubComponent() {
        mainSubComponent = null;
    }//------------------------------------------------------

    public AdviceSubComponent getAdviceSubComponent(AdviceFragment fragment, Context context) {
        if (null == adviceSubComponent)
            createAdviceSubComponent(fragment, context);

        return adviceSubComponent;
    }

    public AdviceSubComponent createAdviceSubComponent(AdviceFragment fragment, Context context) {
        adviceSubComponent = component.plus_AdviceFragment(new AdviceModule(fragment, context));
        return adviceSubComponent;
    }

    public void releaseAdviceSubComponent() {
        adviceSubComponent = null;
    }//-----------------------------------------------------------

    public ProductsSubComponent getProductsSubComponent(ProductsFragment fragment, Context context) {
        if (null == productsSubComponent)
            createProductsSubComponent(fragment, context);

        return productsSubComponent;
    }

    public ProductsSubComponent createProductsSubComponent(ProductsFragment fragment, Context context) {
        productsSubComponent = component.plus_ProductsFragment(new ProductsModule(fragment, context));
        return productsSubComponent;
    }

    public void releaseProductsSubComponent() {
        productsSubComponent = null;
    }//-----------------------------------------------------------

    public FiltersSubComponent getFiltersSubComponent(FiltersFragment fragment, Context context) {
        if (null == filtersSubComponent)
            createFiltersSubComponent(fragment, context);

        return filtersSubComponent;
    }

    public FiltersSubComponent createFiltersSubComponent(FiltersFragment fragment, Context context) {
        filtersSubComponent = component.plus_FiltersFragment(new FiltersModule(fragment, context));
        return filtersSubComponent;
    }

    public void releaseFiltersSubComponent() {
        filtersSubComponent = null;
    }//-----------------------------------------------------------

    public ShopSubComponent getShopSubComponent(ShopFragment fragment, Context context) {
        if (null == shopSubComponent)
            createShopSubComponent(fragment, context);

        return shopSubComponent;
    }

    public ShopSubComponent createShopSubComponent(ShopFragment fragment, Context context) {
        shopSubComponent = component.plus_ShopFragment(new ShopModule(fragment, context));
        return shopSubComponent;
    }

    public void releaseShopSubComponent() {
        shopSubComponent = null;
    }//-----------------------------------------------------------

    public SubcategoriesSubComponent getSubcategoriesSubComponent(SubcategoriesFragment fragment, Context context) {
        if (null == subcategoriesSubComponent)
            createSubcategoriesSubComponent(fragment, context);

        return subcategoriesSubComponent;
    }

    public SubcategoriesSubComponent createSubcategoriesSubComponent(SubcategoriesFragment fragment, Context context) {
        subcategoriesSubComponent = component.plus_SubcategoriesFragment(new SubcategoriesModule(fragment, context));
        return subcategoriesSubComponent;
    }

    public void releaseSubcategoriesSubComponent() {
        subcategoriesSubComponent = null;
    }//-----------------------------------------------------------

    @Override
    public void onCreate() {
        super.onCreate();

        component = createComponent();
    }

    public ApplicationComponent createComponent() {
        return DaggerApplicationComponent.builder()
                .androidModule(new AndroidModule(this))
                .dataModule(new DataModule())
                .build();
    }

}

package com.lahcenezinnour.eshop.injector.module;

import com.lahcenezinnour.eshop.base.BaseView;

import dagger.Module;
import dagger.Provides;

/**
 * Created by lahcene zinnour on 7/16/17.
 */
@Module
class BaseModuleCore  {
    BaseView view;

    BaseModuleCore(BaseView view) {
        this.view = view;
    }

    @Provides
    public BaseView provideView() {
        return view;
    }


}

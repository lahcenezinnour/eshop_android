package com.lahcenezinnour.eshop.injector.module;

import com.lahcenezinnour.eshop.interactor.InteractorApiImpl;
import com.lahcenezinnour.eshop.interactor.interfaces.IInteractorApi;
import com.lahcenezinnour.eshop.mvp.view.ISubcategoriesView;

import dagger.Module;
import dagger.Provides;

/**
 * Created by lahcene zinnour on 5/28/17.
 */

@Module
class SubcategoriesModuleCore {

    ISubcategoriesView view;

    SubcategoriesModuleCore(ISubcategoriesView view) {
        this.view = view;
    }

    @Provides
    public ISubcategoriesView provideView() {
        return view;
    }

    @Provides
    public IInteractorApi provideInteractor(InteractorApiImpl interactorApi) {
        return interactorApi;
    }
}
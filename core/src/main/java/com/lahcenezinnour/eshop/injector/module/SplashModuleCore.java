package com.lahcenezinnour.eshop.injector.module;

import com.lahcenezinnour.eshop.interactor.InteractorApiImpl;
import com.lahcenezinnour.eshop.interactor.interfaces.IInteractorApi;
import com.lahcenezinnour.eshop.mvp.view.IMainView;
import com.lahcenezinnour.eshop.mvp.view.ISplashView;

import dagger.Module;
import dagger.Provides;

/**
 * Created by lahcene zinnour on 1/15/17.
 */
@Module
class SplashModuleCore {

    ISplashView view;

    SplashModuleCore(ISplashView view) {
        this.view = view;
    }

    @Provides
    public ISplashView provideView() {
        return view;
    }

    @Provides
    public IInteractorApi provideInteractor(InteractorApiImpl interactorApi) {
        return interactorApi;
    }

    /*@Provides
    public IMainPresenter providePresenter(MainPresenterImpl presenter) {
        presenter.bind(view);
        return presenter;
    }*/
}

package com.lahcenezinnour.eshop.injector.module;

import com.lahcenezinnour.eshop.interactor.InteractorApiImpl;
import com.lahcenezinnour.eshop.interactor.interfaces.IInteractorApi;
import com.lahcenezinnour.eshop.mvp.view.IAdviceView;

import dagger.Module;
import dagger.Provides;

/**
 * Created by lahcene zinnour on 2/21/17.
 */
@Module
class AdviceModuleCore {

    IAdviceView view;

    AdviceModuleCore(IAdviceView view) {
        this.view = view;
    }

    @Provides
    public IAdviceView provideView() {
        return view;
    }

    @Provides
    public IInteractorApi provideInteractor(InteractorApiImpl interactorApi) {
        return interactorApi;
    }
}

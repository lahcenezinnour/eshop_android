package com.lahcenezinnour.eshop.util;

import rx.Scheduler;

/**
 * Created by lahcene zinnour on 20/10/2016.
 */

public interface ISchedulerProvider {

    Scheduler mainThread();

    Scheduler backgroundThread();

}

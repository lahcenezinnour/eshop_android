package com.lahcenezinnour.eshop.util;

/**
 * Created by lahcene zinnour on 20/10/2016.
 */

public abstract class Constants {

    public static final String API_BASE_URL =
            "http://192.168.1.192:8080/eshop/rest";

    public static final String API_URL_GET_PRODUCT = API_BASE_URL + "/product";

    public static final String API_URL_GET_PRODUCT_LIST = API_URL_GET_PRODUCT + "/all";

    public static final String API_URL_GET_PRODUCT_LIST_BY_RANGE = API_URL_GET_PRODUCT + "/range";

    public static final String API_URL_GET_PRODUCT_BY_PAGE_ITEMS = API_URL_GET_PRODUCT_LIST + "/{page}/{items}";


    public static final String CONTENT_TYPE_LABEL = "Content-Type";
    public static final String CONTENT_TYPE_VALUE_JSON = "application/json; charset=utf-8";

    public static final int NETWORK_CONNECTION_TIMEOUT = 30; // 30 sec
    public static final long CACHE_SIZE = 10 * 1024 * 1024; // 10 MB
    public static final int CACHE_MAX_AGE = 2; // 2 min
    public static final int CACHE_MAX_STALE = 7; // 7 day

    public static final int CODE_OK = 200;


}

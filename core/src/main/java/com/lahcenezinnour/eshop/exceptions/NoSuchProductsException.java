package com.lahcenezinnour.eshop.exceptions;

import java.util.NoSuchElementException;

/**
 * Created by lahcene zinnour on 6/26/17.
 */

public class NoSuchProductsException extends NoSuchElementException {
}

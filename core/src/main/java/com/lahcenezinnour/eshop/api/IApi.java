package com.lahcenezinnour.eshop.api;

import com.lahcenezinnour.eshop.domain.entity.Products;
import com.lahcenezinnour.eshop.util.Constants;

import java.util.List;

import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by lahcene zinnour on 1/1/17.
 */

public interface IApi {

    @GET(Constants.API_URL_GET_PRODUCT_LIST+"/all")
    Observable<List<Products>> getAllProducts();

    @GET(Constants.API_URL_GET_PRODUCT_BY_PAGE_ITEMS)
    Observable<List<Products>> getProductsByRange(@Path("page") int page, @Path("items") int items);

    @GET(Constants.API_URL_GET_PRODUCT_LIST_BY_RANGE)
    Observable<List<Products>> getProductsByRange(@Query("page") int page, @Query("items") int items, @Query("activity") String activity);

    @GET(Constants.API_URL_GET_PRODUCT+"/all/{page}/{items}")
    Observable<List<Products>> getProductsByCategory(@Path("idCategory") int id, @Path("nb") int page, @Path("items") int items);

    @GET(Constants.API_URL_GET_PRODUCT+"/all/{page}/{items}")
    Observable<List<Products>> getProductsBySubcategory(@Path("idSubcategory") int id, @Path("nb") int page, @Path("items") int items);

}

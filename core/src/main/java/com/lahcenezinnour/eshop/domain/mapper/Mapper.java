package com.lahcenezinnour.eshop.domain.mapper;

import com.lahcenezinnour.eshop.mvp.model.Productcategories;
import com.lahcenezinnour.eshop.mvp.model.Products;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lahcene zinnour on 1/2/17.
 */

public class Mapper {

    public static List<Products> ProductEntityToProductModelDataMapper(List<com.lahcenezinnour.eshop.domain.entity.Products> productsEntity) {
        List<Products> productsModel = new ArrayList<>();

        for (int i = 0; i < productsEntity.size() ; i++) {
            Products productModel = new Products();

            productModel.setProductID(productsEntity.get(i).getProductID());
            productModel.setProductCartDesc(productsEntity.get(i).getProductCartDesc());
            productModel.setProductImage(productsEntity.get(i).getProductImage());
            productModel.setProductLocation(productsEntity.get(i).getProductLocation());
            productModel.setProductLongDesc(productsEntity.get(i).getProductLongDesc());
            productModel.setProductName(productsEntity.get(i).getProductName());
            productModel.setProductPrice(productsEntity.get(i).getProductPrice());
            productModel.setProductShortDesc(productsEntity.get(i).getProductShortDesc());
            productModel.setProductSKU(productsEntity.get(i).getProductSKU());
            productModel.setProductStock(productsEntity.get(i).getProductStock());
            productModel.setProductThumb(productsEntity.get(i).getProductThumb());
            productModel.setProductUnlimited(productsEntity.get(i).getProductUnlimited());
            productModel.setProductUpdateDate(productsEntity.get(i).getProductUpdateDate());
            productModel.setProductWeight(productsEntity.get(i).getProductWeight());
            productModel.setProductActivityDisplay(productsEntity.get(i).getProductActivityDisplay());
            productModel.setProductActivityName(productsEntity.get(i).getProductActivityName());
            productModel.setCategory(CategoryEntityToCategoryModelDataMapper(productsEntity.get(i).getCategory()));

            productsModel.add(productModel);
        }

        return productsModel;
    }

public static Productcategories CategoryEntityToCategoryModelDataMapper(com.lahcenezinnour.eshop.domain.entity.Productcategories category){
    return new Productcategories(category.getCategoryID(), category.getCategoryName(), category.getCategoryGender());
}
}

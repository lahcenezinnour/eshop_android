/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lahcenezinnour.eshop.domain.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author lahcen
 */

public class Options{

    @SerializedName("optionID") private Integer optionID;
    @SerializedName("optionName") private String optionName;
    @SerializedName("optionGroups") private Optiongroups optionGroups;
    @Expose private List<Productoptions> productoptions = new ArrayList<>();

    public Options() {
    }

    public Options(Integer optionID) {
        this.optionID = optionID;
    }

    public Integer getOptionID() {
        return optionID;
    }

    public void setOptionID(Integer optionID) {
        this.optionID = optionID;
    }

    public String getOptionName() {
        return optionName;
    }

    public void setOptionName(String optionName) {
        this.optionName = optionName;
    }

    public Optiongroups getOptionGroups() {
        return optionGroups;
    }

    public void setOptionGroups(Optiongroups optionGroups) {
        this.optionGroups = optionGroups;
    }

    @Override
    public String toString() {
        return "Options{" +
                "optionID=" + optionID +
                ", optionName='" + optionName + '\'' +
                '}';
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lahcenezinnour.eshop.domain.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * @author lahcen
 */

public class Productcategories {


    @SerializedName("categoryID") private Integer categoryID;
    @SerializedName("categoryName") private String categoryName;
    @SerializedName("categoryGender") private String categoryGender;
    @Expose private List<Subproductcategories> subCategory = new ArrayList<>();
    @Expose private List<Products> products = new ArrayList<>();

    public Productcategories() {
    }

    public Productcategories(Integer categoryID) {
        this.categoryID = categoryID;
    }

    public Productcategories(String categoryName, String categoryGender) {
        this.categoryName = categoryName;
        this.categoryGender = categoryGender;
    }


    public Integer getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(Integer categoryID) {
        this.categoryID = categoryID;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryGender() {
        return categoryGender;
    }

    public void setCategoryGender(String categoryGender) {
        this.categoryGender = categoryGender;
    }

    public List<Subproductcategories> getSubCategory() {
        return subCategory;
    }

    public void setSubCategory(List<Subproductcategories> subCategory) {
        this.subCategory = subCategory;
    }

    public List<Products> getProducts() {
        return products;
    }

    public void setProducts(List<Products> products) {
        this.products = products;
    }

    @Override
    public String toString() {
        return "Productcategories{" +
                "categoryID=" + categoryID +
                ", categoryName='" + categoryName + '\'' +
                ", categoryGender='" + categoryGender + '\'' +
                '}';
    }
}

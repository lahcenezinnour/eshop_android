/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lahcenezinnour.eshop.domain.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author lahcen
 */

public class Optiongroups{

    @SerializedName("optionGroupID") private Integer optionGroupID;
    @SerializedName("optionGroupName") private String optionGroupName;
    @Expose private List<Options> optionsSet = new ArrayList<>();

    public Optiongroups() {
    }

    public Optiongroups(Integer optionGroupID) {
        this.optionGroupID = optionGroupID;
    }

    public Integer getOptionGroupID() {
        return optionGroupID;
    }

    public void setOptionGroupID(Integer optionGroupID) {
        this.optionGroupID = optionGroupID;
    }

    public String getOptionGroupName() {
        return optionGroupName;
    }

    public void setOptionGroupName(String optionGroupName) {
        this.optionGroupName = optionGroupName;
    }

    @Override
    public String toString() {
        return "Optiongroups{" +
                "optionGroupID=" + optionGroupID +
                ", optionGroupName='" + optionGroupName + '\'' +
                '}';
    }
}

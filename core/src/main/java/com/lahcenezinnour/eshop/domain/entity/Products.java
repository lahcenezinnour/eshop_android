package com.lahcenezinnour.eshop.domain.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Products {

    @SerializedName("productID") private Integer productID;
    @SerializedName("productCartDesc") private String productCartDesc;
    @SerializedName("productImage") private String productImage;
    @SerializedName("productLive") private Boolean productLive;
    @SerializedName("productLocation") private String productLocation;
    @SerializedName("productLongDesc") private String productLongDesc;
    @SerializedName("productName") private String productName;
    @SerializedName("productPrice") private float productPrice;
    @SerializedName("productShortDesc") private String productShortDesc;
    @SerializedName("productSKU") private String productSKU;
    @SerializedName("productStock") private Float productStock;
    @SerializedName("productThumb") private String productThumb;
    @SerializedName("productUnlimited") private Boolean productUnlimited;
    @SerializedName("productUpdateDate") private Long productUpdateDate;
    @SerializedName("productWeight") private Float productWeight;
    @SerializedName("productActivityDisplay") private String productActivityDisplay;
    @SerializedName("productActivityName") private String productActivityName;
    @SerializedName("category") private Productcategories category;
    @Expose private List<Productoptions> productoptions = new ArrayList<>();

    public Products() {
    }

    public Products(Integer productID, String productCartDesc, String productImage, Boolean productLive, String productLocation, String productLongDesc, String productName, float productPrice, String productShortDesc, String productSKU, Float productStock, String productThumb, Boolean productUnlimited, Long productUpdateDate, Float productWeight, String productActivityDisplay, String productActivityName) {
        this.productID = productID;
        this.productCartDesc = productCartDesc;
        this.productImage = productImage;
        this.productLive = productLive;
        this.productLocation = productLocation;
        this.productLongDesc = productLongDesc;
        this.productName = productName;
        this.productPrice = productPrice;
        this.productShortDesc = productShortDesc;
        this.productSKU = productSKU;
        this.productStock = productStock;
        this.productThumb = productThumb;
        this.productUnlimited = productUnlimited;
        this.productUpdateDate = productUpdateDate;
        this.productWeight = productWeight;
        this.productActivityDisplay = productActivityDisplay;
        this.productActivityName = productActivityName;
    }

    public Integer getProductID() {
        return productID;
    }

    public void setProductID(Integer productID) {
        this.productID = productID;
    }

    public String getProductCartDesc() {
        return productCartDesc;
    }

    public void setProductCartDesc(String productCartDesc) {
        this.productCartDesc = productCartDesc;
    }

    public String getProductImage() {
        return productImage;
    }

    public void setProductImage(String productImage) {
        this.productImage = productImage;
    }

    public Boolean getProductLive() {
        return productLive;
    }

    public void setProductLive(Boolean productLive) {
        this.productLive = productLive;
    }

    public String getProductLocation() {
        return productLocation;
    }

    public void setProductLocation(String productLocation) {
        this.productLocation = productLocation;
    }

    public String getProductLongDesc() {
        return productLongDesc;
    }

    public void setProductLongDesc(String productLongDesc) {
        this.productLongDesc = productLongDesc;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public float getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(float productPrice) {
        this.productPrice = productPrice;
    }

    public String getProductShortDesc() {
        return productShortDesc;
    }

    public void setProductShortDesc(String productShortDesc) {
        this.productShortDesc = productShortDesc;
    }

    public String getProductSKU() {
        return productSKU;
    }

    public void setProductSKU(String productSKU) {
        this.productSKU = productSKU;
    }

    public Float getProductStock() {
        return productStock;
    }

    public void setProductStock(Float productStock) {
        this.productStock = productStock;
    }

    public String getProductThumb() {
        return productThumb;
    }

    public void setProductThumb(String productThumb) {
        this.productThumb = productThumb;
    }

    public Boolean getProductUnlimited() {
        return productUnlimited;
    }

    public void setProductUnlimited(Boolean productUnlimited) {
        this.productUnlimited = productUnlimited;
    }

    public Long getProductUpdateDate() {
        return productUpdateDate;
    }

    public void setProductUpdateDate(Long productUpdateDate) {
        this.productUpdateDate = productUpdateDate;
    }

    public Float getProductWeight() {
        return productWeight;
    }

    public void setProductWeight(Float productWeight) {
        this.productWeight = productWeight;
    }

    public String getProductActivityDisplay() {
        return productActivityDisplay;
    }

    public void setProductActivityDisplay(String productActivityDisplay) {
        this.productActivityDisplay = productActivityDisplay;
    }

    public String getProductActivityName() {
        return productActivityName;
    }

    public void setProductActivityName(String productActivityName) {
        this.productActivityName = productActivityName;
    }

    public Productcategories getCategory() {
        return category;
    }

    public void setCategory(Productcategories category) {
        this.category = category;
    }

    public List<Productoptions> getProductoptions() {
        return productoptions;
    }

    public void setProductoptions(List<Productoptions> productoptions) {
        this.productoptions = productoptions;
    }

    @Override
    public String toString() {
        return "Products{" +
                "productID=" + productID +
                ", productCartDesc='" + productCartDesc + '\'' +
                ", productImage='" + productImage + '\'' +
                ", productLive=" + productLive +
                ", productLocation='" + productLocation + '\'' +
                ", productLongDesc='" + productLongDesc + '\'' +
                ", productName='" + productName + '\'' +
                ", productPrice=" + productPrice +
                ", productShortDesc='" + productShortDesc + '\'' +
                ", productSKU='" + productSKU + '\'' +
                ", productStock=" + productStock +
                ", productThumb='" + productThumb + '\'' +
                ", productUnlimited=" + productUnlimited +
                ", productUpdateDate=" + productUpdateDate +
                ", productWeight=" + productWeight +
                ", productActivityDisplay='" + productActivityDisplay + '\'' +
                ", productActivityName='" + productActivityName + '\'' +
                '}';
    }

}



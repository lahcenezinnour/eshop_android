/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lahcenezinnour.eshop.domain.entity;

import com.google.gson.annotations.SerializedName;

/**
 * @author lahcen
 */
public class Subproductcategories {

    @SerializedName("subCategoryID") private Integer subCategoryID;
    @SerializedName("subCategoryName") private String subCategoryName;
    @SerializedName("categoryID") private Productcategories categoryID;

    public Subproductcategories() {
    }

    public Subproductcategories(Integer subCategoryID) {
        this.subCategoryID = subCategoryID;
    }

    public Subproductcategories(Integer subCategoryID, String subCategoryName) {
        this.subCategoryID = subCategoryID;
        this.subCategoryName = subCategoryName;
    }

    public Integer getSubCategoryID() {
        return subCategoryID;
    }

    public void setSubCategoryID(Integer subCategoryID) {
        this.subCategoryID = subCategoryID;
    }

    public String getSubCategoryName() {
        return subCategoryName;
    }

    public void setSubCategoryName(String subCategoryName) {
        this.subCategoryName = subCategoryName;
    }

    public void setCategoryID(Productcategories categoryID) {
        this.categoryID = categoryID;
    }

    public Productcategories getCategoryID() {
        return categoryID;
    }

    @Override
    public String toString() {
        return "Subproductcategories{" +
                "subCategoryID=" + subCategoryID +
                ", subCategoryName='" + subCategoryName + '\'' +
                '}';
    }
}

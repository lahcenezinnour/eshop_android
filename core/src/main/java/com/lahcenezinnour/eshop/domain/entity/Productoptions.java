/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lahcenezinnour.eshop.domain.entity;

import com.google.gson.annotations.SerializedName;

/**
 *
 * @author lahcen
 */

public class Productoptions{

    @SerializedName("productOptionID") private Integer productOptionID;
    @SerializedName("optionPriceIncrement") private Double optionPriceIncrement;
    @SerializedName("optionGroupID") private int optionGroupID;
    @SerializedName("productID") private Products productID;
    @SerializedName("optionID") private Options optionID;

    public Productoptions() {
    }

    public Productoptions(int optionGroupID, Products productID, Options optionID) {
        this.optionGroupID = optionGroupID;
        this.productID = productID;
        this.optionID = optionID;
    }

    public Integer getProductOptionID() {
        return productOptionID;
    }

    public void setProductOptionID(Integer productOptionID) {
        this.productOptionID = productOptionID;
    }

    public Double getOptionPriceIncrement() {
        return optionPriceIncrement;
    }

    public void setOptionPriceIncrement(Double optionPriceIncrement) {
        this.optionPriceIncrement = optionPriceIncrement;
    }

    public Options getOptionID() {
        return optionID;
    }

    public void setOptionID(Options optionID) {
        this.optionID = optionID;
    }

    public int getOptionGroupID() {
        return optionGroupID;
    }

    public void setOptionGroupID(int optionGroupID) {
        this.optionGroupID = optionGroupID;
    }

    @Override
    public String toString() {
        return "Productoptions{" +
                "productOptionID=" + productOptionID +
                ", optionPriceIncrement=" + optionPriceIncrement +
                ", optionGroupID=" + optionGroupID +
                '}';
    }
}

package com.lahcenezinnour.eshop.interactor.interfaces;


import com.lahcenezinnour.eshop.mvp.model.Productcategories;
import com.lahcenezinnour.eshop.mvp.model.Products;
import com.lahcenezinnour.eshop.mvp.model.Subproductcategories;

import java.util.List;

/**
 * Created by lahcene zinnour on 1/3/17.
 */

public interface IInteractorDb {

    List<Products> getProductsByRange_DB(int page, int items);

    int insertProductsByRange_DB(List<Products> products);

    int insertProducts_DB(List<Products> products);

    List<Productcategories> getAllCategories_DB();

    List<Productcategories> getCategoriesByGender_DB( String gender);

    List<Subproductcategories> getSubcategoriesByCategory_DB(int idCategory);

    List<Products> getProductsByCategory_DB(int id, int page, int items);

    List<Products> getProductsByRange_DB(int page, int items, String activity);

    List<Products> getProductsByActivityDisplay_DB(int page, int items, String activity);
}

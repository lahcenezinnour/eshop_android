package com.lahcenezinnour.eshop.interactor.interfaces;

import com.lahcenezinnour.eshop.base.BaseInteractor;
import com.lahcenezinnour.eshop.domain.entity.Products;

import java.util.List;

import rx.Observable;

/**
 * Created by lahcene zinnour on 1/3/17.
 */

public interface IInteractorApi extends BaseInteractor {

    Observable<List<Products>> getProducts_API();

    Observable<List<Products>> getProductsByRange_API(int page, int items);

    Observable<List<Products>> getProductsByRange_API(int page, int items, String activity);

    Observable<List<Products>> getProductsByCategory_API(int id, int page, int items);

    Observable<List<Products>> getProductsBySubcategory_API(int id, int page, int items);


}

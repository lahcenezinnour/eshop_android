package com.lahcenezinnour.eshop.interactor;

import com.lahcenezinnour.eshop.api.IApi;
import com.lahcenezinnour.eshop.domain.entity.Products;
import com.lahcenezinnour.eshop.interactor.interfaces.IInteractorApi;
import com.lahcenezinnour.eshop.util.ISchedulerProvider;

import java.util.List;

import javax.inject.Inject;

import rx.Observable;
import rx.Subscription;
import rx.subjects.ReplaySubject;

/**
 * Created by lahcene zinnour on 1/3/17.
 */

public class InteractorApiImpl implements IInteractorApi {

    private IApi api;
    private ISchedulerProvider scheduler;

    private ReplaySubject<List<Products>> productSubject;
    private Subscription productSubscription;

    @Inject
    public InteractorApiImpl(IApi api, ISchedulerProvider scheduler) {
        this.api = api;
        this.scheduler = scheduler;
    }

    @Override
    public Observable<List<Products>> getProducts_API() {
        if (productSubscription == null || productSubscription.isUnsubscribed()) {
            productSubject = ReplaySubject.create();

            productSubscription = api.getAllProducts()
                    .subscribeOn(scheduler.backgroundThread())
                    .subscribe(productSubject);
        }

        return productSubject.asObservable();
    }

    @Override
    public Observable<List<Products>> getProductsByRange_API(int page, int items) {
        if (productSubscription == null || productSubscription.isUnsubscribed()) {
            productSubject = ReplaySubject.create();

            productSubscription = api.getProductsByRange(page, items)
                    .subscribeOn(scheduler.backgroundThread())
                    .subscribe(productSubject);
        }

        return productSubject.asObservable();
    }

    @Override
    public Observable<List<Products>> getProductsByRange_API(int page, int items, String activity) {
        if (productSubscription == null || productSubscription.isUnsubscribed()) {
            productSubject = ReplaySubject.create();

            productSubscription = api.getProductsByRange(page, items, activity)
                    .subscribeOn(scheduler.backgroundThread())
                    .subscribe(productSubject);
        }

        return productSubject.asObservable();
    }

    @Override
    public Observable<List<Products>> getProductsByCategory_API(int id, int page, int items) {
        return null;
    }

    @Override
    public Observable<List<Products>> getProductsBySubcategory_API(int id, int page, int items) {
        return null;
    }

    @Override
    public void unbind() {
        if (productSubscription != null && !productSubscription.isUnsubscribed())
            productSubscription.unsubscribe();
    }
}

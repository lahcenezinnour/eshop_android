package com.lahcenezinnour.eshop.base;

/**
 * Created by lahcene zinnour on 20/10/2016.
 */

public interface BaseInteractor {

    void unbind();

}

package com.lahcenezinnour.eshop.base;

/**
 * Created by lahcene zinnour on 20/10/2016.
 */

public interface BasePresenter<T> {

    void bind(T view);

    void unbind();

}

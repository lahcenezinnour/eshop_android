package com.lahcenezinnour.eshop.base;

/**
 * Created by lahcene zinnour on 20/10/2016.
 */

public interface BaseView {

    void showNetworkError();
    void showOfflineMessage();
    void showRetryMessage();
    void showServerError();
    void showProgress();
    void hideProgress();


    void showQueryNoResult();
}

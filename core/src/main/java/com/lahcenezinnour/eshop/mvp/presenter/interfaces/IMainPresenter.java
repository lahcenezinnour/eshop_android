package com.lahcenezinnour.eshop.mvp.presenter.interfaces;

import com.lahcenezinnour.eshop.base.BasePresenter;
import com.lahcenezinnour.eshop.mvp.view.IMainView;


/**
 * Created by lahcene zinnour on 1/5/17.
 */

public interface IMainPresenter extends BasePresenter<IMainView> {

    void getProductByRange_API(int page, int items, String activity, boolean isConnected);

    void getProductByRange_DB(int page, int items, String activity);



}

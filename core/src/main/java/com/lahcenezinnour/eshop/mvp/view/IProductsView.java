package com.lahcenezinnour.eshop.mvp.view;

import com.lahcenezinnour.eshop.base.BaseView;
import com.lahcenezinnour.eshop.mvp.model.Products;

import java.util.List;

/**
 * Created by lahcene zinnour on 2/24/17.
 */
public interface IProductsView extends BaseView {

    void showProductsByRange(List<Products> products);

    void saveProductByRangeInDB(List<Products> character);

    void showProgress();

    void hideProgress();

    void showRetryMessage(Throwable throwable);

    void showError(Throwable throwable);

    void showServiceError();
    void onLoadMore(List<Products> products);

    void networkError();
}

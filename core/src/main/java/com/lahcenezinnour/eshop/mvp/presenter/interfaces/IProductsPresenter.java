package com.lahcenezinnour.eshop.mvp.presenter.interfaces;

import com.lahcenezinnour.eshop.base.BasePresenter;
import com.lahcenezinnour.eshop.mvp.view.IProductsView;


/**
 * Created by lahcene zinnour on 1/5/17.
 */

public interface IProductsPresenter extends BasePresenter<IProductsView> {

    void getProductsByCategoryFromApi(int nb, int i);

    void getProductsByCategoryFromDb(int nb, int i);



}

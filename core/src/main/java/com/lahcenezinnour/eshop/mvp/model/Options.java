/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lahcenezinnour.eshop.mvp.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author lahcen
 */

public class Options implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer optionID;
    private String optionName;
    private Optiongroups optionGroups;
    private List<Productoptions> productoptions = new ArrayList<>();

    public Options() {
    }

    public Options(Integer optionID) {
        this.optionID = optionID;
    }

    public Integer getOptionID() {
        return optionID;
    }

    public void setOptionID(Integer optionID) {
        this.optionID = optionID;
    }

    public String getOptionName() {
        return optionName;
    }

    public void setOptionName(String optionName) {
        this.optionName = optionName;
    }

    public Optiongroups getOptionGroups() {
        return optionGroups;
    }

    public void setOptionGroups(Optiongroups optionGroups) {
        this.optionGroups = optionGroups;
    }

    @Override
    public String toString() {
        return "Options{" +
                "optionID=" + optionID +
                ", optionName='" + optionName + '\'' +
                '}';
    }
}

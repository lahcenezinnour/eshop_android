package com.lahcenezinnour.eshop.mvp.presenter.interfaces;

import com.lahcenezinnour.eshop.base.BasePresenter;
import com.lahcenezinnour.eshop.mvp.view.IFiltersView;


/**
 * Created by lahcene zinnour on 1/5/17.
 */

public interface IFiltersPresenter extends BasePresenter<IFiltersView> {


}

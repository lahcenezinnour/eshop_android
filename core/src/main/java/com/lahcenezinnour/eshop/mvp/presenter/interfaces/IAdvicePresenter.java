package com.lahcenezinnour.eshop.mvp.presenter.interfaces;

import com.lahcenezinnour.eshop.base.BasePresenter;
import com.lahcenezinnour.eshop.mvp.view.IAdviceView;


/**
 * Created by lahcene zinnour on 1/5/17.
 */

public interface IAdvicePresenter extends BasePresenter<IAdviceView> {

    void getAllCategoriesFromDb();

    void getCategoriesByGenderFromDb(String gender);

}

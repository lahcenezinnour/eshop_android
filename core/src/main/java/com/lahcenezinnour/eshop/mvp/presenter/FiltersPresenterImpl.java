package com.lahcenezinnour.eshop.mvp.presenter;

import com.lahcenezinnour.eshop.interactor.interfaces.IInteractorApi;
import com.lahcenezinnour.eshop.interactor.interfaces.IInteractorDb;
import com.lahcenezinnour.eshop.mvp.model.Products;
import com.lahcenezinnour.eshop.mvp.presenter.interfaces.IFiltersPresenter;
import com.lahcenezinnour.eshop.mvp.view.IFiltersView;
import com.lahcenezinnour.eshop.util.ISchedulerProvider;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import rx.Subscription;
import rx.subscriptions.Subscriptions;

/**
 * Created by lahcene zinnour on 1/5/17.
 */

public class FiltersPresenterImpl implements IFiltersPresenter {

    @Inject
    IInteractorApi interactorApi;
    @Inject
    IInteractorDb interactorDb;

    IFiltersView view;
    List<Products> products = new ArrayList<>();

    private Subscription subscription = Subscriptions.empty();
    private ISchedulerProvider scheduler;

    @Inject
    public FiltersPresenterImpl(ISchedulerProvider scheduler) {
        this.scheduler = scheduler;
    }

    @Override
    public void bind(IFiltersView view) {
        this.view = view;
    }

    @Override
    public void unbind() {
        if (subscription != null && !subscription.isUnsubscribed())
            subscription.unsubscribe();

        interactorApi.unbind();

        view = null;
        interactorApi = null;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lahcenezinnour.eshop.mvp.model;

import java.io.Serializable;

/**
 * @author lahcen
 */
public class Subproductcategories implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer subCategoryID;
    private String subCategoryName;
    private Productcategories categoryID;

    public Subproductcategories() {
    }

    public Subproductcategories(Integer subCategoryID) {
        this.subCategoryID = subCategoryID;
    }

    public Subproductcategories(Integer subCategoryID, String subCategoryName) {
        this.subCategoryID = subCategoryID;
        this.subCategoryName = subCategoryName;
    }

    public Integer getSubCategoryID() {
        return subCategoryID;
    }

    public void setSubCategoryID(Integer subCategoryID) {
        this.subCategoryID = subCategoryID;
    }

    public String getSubCategoryName() {
        return subCategoryName;
    }

    public void setSubCategoryName(String subCategoryName) {
        this.subCategoryName = subCategoryName;
    }

    public void setCategoryID(Productcategories categoryID) {
        this.categoryID = categoryID;
    }

    public Productcategories getCategoryID() {
        return categoryID;
    }

    @Override
    public String toString() {
        return "Subproductcategories{" +
                "subCategoryID=" + subCategoryID +
                ", subCategoryName='" + subCategoryName + '\'' +
                '}';
    }
}

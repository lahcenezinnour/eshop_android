package com.lahcenezinnour.eshop.mvp.presenter;

import com.lahcenezinnour.eshop.domain.mapper.Mapper;
import com.lahcenezinnour.eshop.exceptions.NoSuchProductsException;
import com.lahcenezinnour.eshop.interactor.interfaces.IInteractorApi;
import com.lahcenezinnour.eshop.interactor.interfaces.IInteractorDb;
import com.lahcenezinnour.eshop.mvp.model.Products;
import com.lahcenezinnour.eshop.mvp.presenter.interfaces.IMainPresenter;
import com.lahcenezinnour.eshop.mvp.presenter.interfaces.ISplashPresenter;
import com.lahcenezinnour.eshop.mvp.view.IMainView;
import com.lahcenezinnour.eshop.util.ISchedulerProvider;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import rx.Observer;
import rx.Subscription;
import rx.exceptions.Exceptions;
import rx.functions.Func1;
import rx.subscriptions.Subscriptions;

/**
 * Created by lahcene zinnour on 1/5/17.
 */

public class SplashPresenterImpl implements ISplashPresenter {

    @Inject
    IInteractorApi interactorApi;
    @Inject
    IInteractorDb interactorDb;

    IMainView view;
    List<Products> products = new ArrayList<>();

    private Subscription subscription = Subscriptions.empty();
    private ISchedulerProvider scheduler;

    @Inject
    public SplashPresenterImpl(ISchedulerProvider scheduler) {
        this.scheduler = scheduler;
    }

    @Override
    public void bind(IMainView view) {
        this.view = view;
    }

    @Override
    public void unbind() {
        if (subscription != null && !subscription.isUnsubscribed())
            subscription.unsubscribe();

        interactorApi.unbind();

        view = null;
        interactorApi = null;
    }

    /*Observable<java.util.List<Products>>*/
    @Override
    public void getProductByRange_API(int page, int items, String activity, boolean isConnected) {
        if (view != null) {
            view.showProgress();
        }

        subscription = interactorApi.getProductsByRange_API(page, items, activity)
                .map(products -> {
                    if (products.size() > 0 || products != null)
                        return products;
                    else
                        throw Exceptions.propagate(new NoSuchProductsException());
                })
                .map(Mapper::ProductEntityToProductModelDataMapper)
                .map(products -> {
                    interactorDb.insertProductsByRange_DB(products);

                    return products;
                })
                .observeOn(scheduler.mainThread())
                .subscribe(products -> {
                            if (null != view) {
                                view.hideProgress();

                                if (!isConnected){
                                    getProductByRange_DB(page, items, activity);
                                }
                                else
                                    view.onLoadMore(products);
                            }
                        },
                        // handle exceptions
                        throwable -> {
                            if (null != view) {
                                view.hideProgress();
                            }

                            if (isConnected) {
                                if (null != view) {
                                    if (throwable instanceof NoSuchProductsException)
                                        view.showQueryNoResult();
                                    else
                                        view.showRetryMessage();
                                }
                            } else {
                                if (null != view) {
                                    view.showNetworkError();
                                    view.showRetryMessage();
                                }
                            }
                        });

    }

    @Override
    public void getProductByRange_DB(int page, int items, String activity) {
        if (interactorDb.getAllCategories_DB().size() > 0)
            view.onLoadMore(interactorDb.getProductsByRange_DB(page, items, activity));
        else
            throw Exceptions.propagate(new NoSuchProductsException());

    }

}

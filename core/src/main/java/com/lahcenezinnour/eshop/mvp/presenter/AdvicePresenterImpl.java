package com.lahcenezinnour.eshop.mvp.presenter;

import com.lahcenezinnour.eshop.interactor.interfaces.IInteractorApi;
import com.lahcenezinnour.eshop.interactor.interfaces.IInteractorDb;
import com.lahcenezinnour.eshop.mvp.model.Products;
import com.lahcenezinnour.eshop.mvp.presenter.interfaces.IAdvicePresenter;
import com.lahcenezinnour.eshop.mvp.view.IAdviceView;
import com.lahcenezinnour.eshop.util.ISchedulerProvider;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import rx.Subscription;
import rx.subscriptions.Subscriptions;

/**
 * Created by lahcene zinnour on 1/5/17.
 */

public class AdvicePresenterImpl implements IAdvicePresenter {

    @Inject
    IInteractorApi interactorApi;
    @Inject
    IInteractorDb interactorDb;

    IAdviceView view;
    List<Products> products = new ArrayList<>();

    private Subscription subscription = Subscriptions.empty();
    private ISchedulerProvider scheduler;

    @Inject
    public AdvicePresenterImpl(ISchedulerProvider scheduler) {
        this.scheduler = scheduler;
    }

    @Override
    public void bind(IAdviceView view) {
        this.view = view;
    }

    @Override
    public void unbind() {
        if (subscription != null && !subscription.isUnsubscribed())
            subscription.unsubscribe();

        interactorApi.unbind();

        view = null;
        interactorApi = null;
    }

    @Override
    public void getAllCategoriesFromDb() {
        if (view != null){

        }
        view.showCategories(interactorDb.getAllCategories_DB(),
                interactorDb.getCategoriesByGender_DB("MEN"),
                interactorDb.getCategoriesByGender_DB("WOMEN"),
                interactorDb.getCategoriesByGender_DB("KIDS"));
    }

    @Override
    public void getCategoriesByGenderFromDb(String gender) {

    }
}

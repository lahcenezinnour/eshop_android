package com.lahcenezinnour.eshop.mvp.view;

import com.lahcenezinnour.eshop.base.BaseView;
import com.lahcenezinnour.eshop.mvp.model.Products;

import java.util.List;

/**
 * Created by lahcene zinnour on 1/5/17.
 */

public interface IMainView extends BaseView {

    void showProductByRange(List<Products> products);

    void saveProductByRangeInDB(List<Products> character);

    void showProgress();

    void hideProgress();

    void showRetryMessage(Throwable throwable);

    void onLoadMore(List<Products> products);

}

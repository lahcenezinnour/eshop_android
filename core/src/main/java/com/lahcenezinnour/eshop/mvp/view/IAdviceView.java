package com.lahcenezinnour.eshop.mvp.view;

import com.lahcenezinnour.eshop.base.BaseView;
import com.lahcenezinnour.eshop.mvp.model.Productcategories;

import java.util.List;

/**
 * Created by lahcene zinnour on 1/5/17.
 */

public interface IAdviceView extends BaseView {

    void showCategories(List<Productcategories> all,
                        List<Productcategories> men,
                        List<Productcategories> women,
                        List<Productcategories> kids);


}
